package com.pingan.brands.api.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonUtils {

    private static Logger log = LoggerFactory.getLogger(CommonUtils.class);

    private CommonUtils() {
        throw new IllegalStateException("Utility class");
    }
}
