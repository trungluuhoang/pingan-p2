# PingAn Customer Project

This project provides a foundation to build page templates and components.

It provides default modules to different purpose: 
* pingan-api: contains api/common classes like constants, utils or services...
* pingan-content: contains sample bootstrap (xml) for pages and content apps
* pingan-templates: contains templating definition for pages
* pingan-ui: contains app definitions and UI classes like actions, commands, rest,..
* pingan-webapp: contains magnolia dx-core bundle with these modules
