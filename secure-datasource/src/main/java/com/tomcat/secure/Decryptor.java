package com.tomcat.secure;

public interface Decryptor {

    public void configure() throws DecryptionException;

    public byte[] decrypt(String content) throws DecryptionException;

}