package com.tomcat.secure;

public class DecryptionException extends Exception {

    DecryptionException(Throwable t) {
        super(t);
    }

    DecryptionException(String message) {
        super(message);
    }
}