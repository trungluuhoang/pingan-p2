package com.tomcat.secure;

import java.nio.charset.StandardCharsets;
import java.util.Properties;

import javax.naming.Context;
import javax.sql.DataSource;

import org.apache.tomcat.jdbc.pool.DataSourceFactory;

/**
 * A {@link DataSourceFactory} that supports an encrypted password.
 * The datasource assumes that the <code>password</code> property contains an encrypted password and
 * decrypts it using the configured {@link ConfigurableDecryptor}.
 */
public class SecureDataSourceFactory extends DataSourceFactory {

    private Decryptor decryptor = new ConfigurableDecryptor();

    @Override
    public DataSource createDataSource(Properties properties, Context context, boolean XA) throws Exception {
        validate(properties);
        replacePassword(properties);
        return super.createDataSource(properties, context, XA);
    }

    private void replacePassword(Properties properties) throws DecryptionException {
        if (PasswordHolder.getPassword() == null) {
            PasswordHolder.setPassword(decryptPassword(properties));
        }
        properties.setProperty(PROP_PASSWORD, PasswordHolder.getPassword());
    }

    private String decryptPassword(Properties properties) throws DecryptionException {
        decryptor.configure();
        return new String(decryptor.decrypt(properties.getProperty(PROP_PASSWORD)), StandardCharsets.UTF_8);
    }

    private void validate(Properties properties) throws DecryptionException {
        if (!properties.containsKey(PROP_PASSWORD)) {
            throw new DecryptionException("Property '" + PROP_PASSWORD + "' not specified");
        }
    }

}
