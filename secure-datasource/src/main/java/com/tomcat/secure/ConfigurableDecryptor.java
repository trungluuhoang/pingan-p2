package com.tomcat.secure;

import static javax.crypto.Cipher.DECRYPT_MODE;
import static javax.xml.bind.DatatypeConverter.parseBase64Binary;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Properties;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * A configurable {@link Decryptor} that allows a given cipher text to be decrypted using a specified algorithm and key file.
 * The decryptor can be configured through the properties provided in the constructor.   The following properties are available
 *
 * <dl>
 * <dt>algorithm</dt>
 * <dt>mode</dt>
 * <dt>padding</dt>
 * <dt>secretKey</dt>
 * </dl>
 *
 * The file specified by keyFilename must exist and be readable by the Tomcat user.
 */
public class ConfigurableDecryptor implements Decryptor {

    private static final String PROP_KEY_ALGORITHM = "keyAlgorithm";
    private static final String PROP_CIPHER_ALGORITHM = "cipherAlgorithm";
    private static final String PROP_SECRETKEY = "secretKey";

    private static final String DEFAULT_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";
    private static final String DEFAULT_KEY_ALGORITHM = "AES";

    private String keyAlgorithm;
    private String cipherAlgorithm;
    private byte[] encryptionKeyBytes;

    public void configure() throws DecryptionException {
        try (InputStream input = getClass().getClassLoader().getResourceAsStream("config.properties")) {

            Properties properties = new Properties();
            if (input == null) {
                throw new DecryptionException("Sorry, unable to find config.properties");
            }
            //load a properties file from class path, inside static method
            properties.load(input);
            keyAlgorithm = properties.getProperty(PROP_KEY_ALGORITHM, DEFAULT_KEY_ALGORITHM);
            cipherAlgorithm = properties.getProperty(PROP_CIPHER_ALGORITHM, DEFAULT_CIPHER_ALGORITHM);

            encryptionKeyBytes = properties.getProperty(PROP_SECRETKEY, "").getBytes();

        } catch (IOException e) {
            throw new DecryptionException(e);
        }

    }

    public byte[] decrypt(String content) throws DecryptionException {
        try {
            System.out.println("decrypt:" + content);
            SecretKey secretKey = new SecretKeySpec(encryptionKeyBytes, keyAlgorithm);
            Cipher cipher = Cipher.getInstance(cipherAlgorithm);
            cipher.init(DECRYPT_MODE, secretKey);
            return cipher.doFinal(parseBase64Binary(content));
        } catch (GeneralSecurityException e) {
            throw new DecryptionException(e);
        }
    }
}
