package com.tomcat.secure;

import static javax.xml.bind.DatatypeConverter.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Main {
    private static final String KEY_ALGORITHM = "AES";
    private static final String DEFAULT_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";

    public static String encrypt(String content, String priKey) {
        try {
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
            SecretKeySpec secretKey = new SecretKeySpec(priKey.getBytes(), KEY_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] result = cipher.doFinal(content.getBytes());
            return printBase64Binary(result);
        } catch (Exception ex) {
            System.out.println("ex:" + ex);
        }
        return null;
    }

    /**
     * * @param content * @param priKey * @return
     */
    public static String decrypt(String content, String priKey) {
        try {
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
            SecretKeySpec secretKey = new SecretKeySpec(priKey.getBytes(), KEY_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] result = cipher.doFinal(parseBase64Binary(content));
            return new String(result);
        } catch (Exception ex) {
            System.out.println("ex:" + ex);
        }
        return null;
    }

    public static void main(String[] args) {
        Queue<String> arguments = new LinkedList<>(Arrays.asList(args));
        try {
            String command = arguments.remove();
            String password = arguments.remove();
            String secretKey = arguments.remove();
            if (command.equals("encrypt")) System.out.println("Encrypted password: " + encrypt(password, secretKey));
            else if (command.equals("decrypt")) System.out.println("Decrypted password " + decrypt(password, secretKey));
            else printUsage();
        } catch (NoSuchElementException e) {
            printUsage();
        }
    }

    private static void printUsage() {
        System.out.println("Usage: secure-datasource <command> <options>");
        System.out.println("Commands:");
        System.out.println("    encrypt <password> <secretKey>");
        System.out.println("    decrypt <password> <secretKey>");
    }
}
