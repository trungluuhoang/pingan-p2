package com.tomcat.secure;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class ConfigurableDecryptorTest {

    private Decryptor decryptor;

    @Before
    public void setUp() throws DecryptionException {
        decryptor = new ConfigurableDecryptor();
        decryptor.configure();
    }

    @Test
    public void configure() {
    }

    @Test
    public void decrypt() throws DecryptionException {
        String expectedPassword = "123456";
        byte[] clearBytes = decryptor.decrypt("Wr3GIx3IqoOclRXrpRMRsQ==");
        assertThat(expectedPassword, is(equalTo(new String(clearBytes))));
    }
}