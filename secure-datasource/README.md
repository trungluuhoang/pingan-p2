# Secure Datasource Module

This module will be used to decrypt password of JNDI datasource
###### Ref: https://docs.oracle.com/javase/8/docs/api/javax/crypto/Cipher.html
###### Ref: https://docs.oracle.com/javase/8/docs/technotes/guides/security/StandardNames.html#Cipher



#
#### Config.properties

```
keyAlgorithm=[optional]
cipherAlgorithm=[optional]
secretKey=[required]
```

+ KEY_ALGORITHM = "AES";
+ DEFAULT_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";

Sample: */secure-datasource/src/test/resources/config.properties*



#
#### Tomcat Configuration 

**${catalina.base}** is the **root** tomcat folder

1. Combine `secure-datasource-1.0-SNAPSHOT.jar`

2. Add the jar `secure-datasource-1.0-SNAPSHOT.jar` to **${catalina.base}/lib**

3. Add **config.properties** to **${catalina.base}/shared** folder

4. Add `"${catalina.base}/shared"` to **common.loader** of **${catalina.base}/conf/catalina.properties**

    `common.loader="${catalina.base}/lib","${catalina.base}/lib/*.jar","${catalina.home}/lib","${catalina.home}/lib/*.jar","${catalina.base}/shared"`

5. Add a Resource to **${catalina.base}/conf/context.xml** with `factory="com.tomcat.secure.SecureDataSourceFactory"` and **encrypted password**

```
    <Resource name="jdbc/MagnoliaAuthor"
              auth="Container"
              factory="com.tomcat.secure.SecureDataSourceFactory"
              type="javax.sql.DataSource"
              driverClassName="org.postgresql.Driver"
              explicitUrl="true"
              url="jdbc:postgresql://localhost:5432/magnoliaAuthor"
              username="magnoliaAuthor"
              password="5gJI/aQn26XEL6g/0uZxtg=="
              maxActive="200"
              initialSize="0"
              minIdle="0"
              maxIdle="8"
              maxWait="10000"
              timeBetweenEvictionRunsMillis="30000"
              minEvictableIdleTimeMillis="60000"
              testWhileIdle="true"
              validationQuery="SELECT 1"
              maxAge="600000"
              rollbackOnReturn="true"
    />
```   

Sample: 

1. */secure-datasource/tomcat-config/context.xml*

2. */secure-datasource/tomcat-config/secure-datasource-1.0-SNAPSHOT.jar* 



#
#### Magnolia Configuration

Change Datasource to use tomcat JNDI Resource
```
    <DataSource name="magnolia">
      <param name="driver" value="javax.naming.InitialContext"/>
      <param name="url" value="java:comp/env/jdbc/MagnoliaAuthor"/>
      <param name="databaseType" value="postgresql"/>
    </DataSource>
```

Sample: 

*/secure-datasource/magnolia-config/jackrabbit-bundle-postgres-search-secure.xml*


#
#### Encrypt/Decrypt manually

Commands:

```
encrypt <password> <secretKey>
    
decrypt <password> <secretKey> 
```

Encrypt
`com.tomcat.secure.Main encrypt 123456 Paic123412341234`

Decrypt
`com.tomcat.secure.Main decrypt Wr3GIx3IqoOclRXrpRMRsQ== Paic123412341234`

