package com.pingan.brands.ui.setup;

import info.magnolia.cms.security.Permission;
import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.AddPermissionTask;
import info.magnolia.module.delta.AddURIPermissionTask;
import info.magnolia.module.delta.BootstrapSingleResource;
import info.magnolia.module.delta.BootstrapSingleResourceAndOrderAfter;
import info.magnolia.module.delta.DeltaBuilder;
import info.magnolia.module.delta.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is optional and lets you manage the versions of your module,
 * by registering "deltas" to maintain the module's configuration, or other type of content.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 *
 * @see info.magnolia.module.DefaultModuleVersionHandler
 * @see info.magnolia.module.ModuleVersionHandler
 * @see info.magnolia.module.delta.Task
 */
public class PingAnUIVersionHandler extends DefaultModuleVersionHandler {

    public PingAnUIVersionHandler() {
        register(DeltaBuilder.update("1.0.0", "").addTasks(updateTo_1_0_0()));
    }

    @Override
    protected List<Task> getExtraInstallTasks(InstallContext installContext) {
        List<Task> tasks = new ArrayList<>();
        tasks.addAll(updateTo_1_0_0());
        return tasks;
    }

    private List<Task> updateTo_1_0_0() {
        List<Task> tasks = new ArrayList<>();

//        tasks.add(new BootstrapSingleResource("Bootstrap appLauncherLayout configuration", "", "/mgnl-bootstrap/pingan-ui/config/config.modules.ui-admincentral.config.appLauncherLayout.yaml", "edit"));

        tasks.add(new AddPermissionTask("Give anonymous role permissions to read pa-carousel workspace", "Gives anonymous role permissions to read pa-carousel workspace.", "anonymous", "pa-carousel", "/", Permission.READ, true));

        tasks.add(new AddPermissionTask("Give anonymous role permissions to read pa-history workspace", "Gives anonymous role permissions to read pa-history workspace.", "anonymous", "pa-history", "/", Permission.READ, true));

        tasks.add(new AddPermissionTask("Give anonymous role permissions to read pa-investor-relations workspace", "Gives anonymous role permissions to read pa-investor-relations workspace.", "anonymous", "pa-investor-relations", "/", Permission.READ, true));

        tasks.add(new AddPermissionTask("Give anonymous role permissions to read pa-ir-contacts workspace", "Gives anonymous role permissions to read pa-ir-contacts workspace.", "anonymous", "pa-ir-contacts", "/", Permission.READ, true));

        tasks.add(new AddPermissionTask("Give anonymous role permissions to read pa-ir-library workspace", "Gives anonymous role permissions to read pa-ir-library workspace.", "anonymous", "pa-ir-library", "/", Permission.READ, true));

        tasks.add(new AddPermissionTask("Give anonymous role permissions to read pa-navigation workspace", "Gives anonymous role permissions to read pa-navigation workspace.", "anonymous", "pa-navigation", "/", Permission.READ, true));

        tasks.add(new AddPermissionTask("Give anonymous role permissions to read pa-profiles workspace", "Gives anonymous role permissions to read pa-profiles workspace.", "anonymous", "pa-profiles", "/", Permission.READ, true));

        tasks.add(new AddPermissionTask("Give anonymous role permissions to read pa-share-capital workspace", "Gives anonymous role permissions to read pa-share-capital workspace.", "anonymous", "pa-share-capital", "/", Permission.READ, true));

        tasks.add(new AddPermissionTask("Give anonymous role permissions to read pa-share workspace", "Gives anonymous role permissions to read pa-share workspace.", "anonymous", "pa-share", "/", Permission.READ, true));

        tasks.add(new AddPermissionTask("Give anonymous role permissions to read/write pa-subscription workspace", "Gives anonymous role permissions to read/write pa-subscription workspace.", "anonymous", "pa-subscription", "/", Permission.ALL, true));

        tasks.add(new AddURIPermissionTask("Add Get&Post permissions", "rest-anonymous", "/.rest/pingan/v1/form/subscribe", AddURIPermissionTask.GET_POST));

        tasks.add(new AddURIPermissionTask("Add Get&Post permissions", "rest-anonymous", "/.rest/pingan/v1/query/*", AddURIPermissionTask.GET_POST));

        tasks.add(new AddPermissionTask("Give anonymous role permissions to read pa-sustainability-reports workspace", "Gives anonymous role permissions to read pa-sustainability-reports workspace.", "anonymous", "pa-sustainability-reports", "/", Permission.READ, true));

        return tasks;
    }

}
