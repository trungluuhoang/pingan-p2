package com.pingan.brands.ui;

import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;

/**
 * This class is optional and represents the configuration for the pingan-ui module.
 * By exposing simple getter/setter/adder methods, this bean can be configured via content2bean
 * using the properties and node from <tt>config:/modules/pingan-ui</tt>.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 * See https://documentation.magnolia-cms.com/display/DOCS/Module+configuration for information about module configuration.
 */
public class PingAnUI implements ModuleLifecycle {

    @Override
    public void start(ModuleLifecycleContext moduleLifecycleContext) {
        // do nothing
    }

    @Override
    public void stop(ModuleLifecycleContext moduleLifecycleContext) {
        // do nothing
    }
}
