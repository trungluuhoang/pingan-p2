package com.pingan.brands.content.setup;

import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.BootstrapSingleResource;
import info.magnolia.module.delta.DeltaBuilder;
import info.magnolia.module.delta.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is optional and lets you manage the versions of your module,
 * by registering "deltas" to maintain the module's configuration, or other type of content.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 *
 * @see info.magnolia.module.DefaultModuleVersionHandler
 * @see info.magnolia.module.ModuleVersionHandler
 * @see info.magnolia.module.delta.Task
 */
public class PingAnContentVersionHandler extends DefaultModuleVersionHandler {

    public PingAnContentVersionHandler() {
        register(DeltaBuilder.update("1.0.0", "").addTasks(updateTo_1_0_0()));
    }

    @Override
    protected List<Task> getExtraInstallTasks(InstallContext installContext) {
        List<Task> tasks = new ArrayList<>();
        tasks.addAll(updateTo_1_0_0());
        return tasks;
    }

    private List<Task> updateTo_1_0_0() {
        List<Task> tasks = new ArrayList<>();

//        tasks.add(new BootstrapSingleResource("Add navigation sample.", "", "/mgnl-bootstrap-samples/pingan-content/navigation/pa-navigation.pingan.yaml"));

//        tasks.add(new BootstrapSingleResource("Add dam sample.", "", "/mgnl-bootstrap-samples/pingan-content/dam/dam.pingan.xml"));
//        tasks.add(new BootstrapSingleResource("Add dam banner sample.", "", "/mgnl-bootstrap-samples/pingan-content/dam/dam.pingan.banner.xml"));
//        tasks.add(new BootstrapSingleResource("Add dam videos.", "", "/mgnl-bootstrap-samples/pingan-content/dam/dam.pingan.videos.xml"));
//        tasks.add(new BootstrapSingleResource("Add dam verticalSlide.", "", "/mgnl-bootstrap-samples/pingan-content/dam/dam.pingan.verticalSlide.xml"));
//        tasks.add(new BootstrapSingleResource("Add dam news.", "", "/mgnl-bootstrap-samples/pingan-content/dam/dam.pingan.news.xml"));
//        tasks.add(new BootstrapSingleResource("Add dam others.", "", "/mgnl-bootstrap-samples/pingan-content/dam/dam.pingan.others.xml"));

//        tasks.add(new BootstrapSingleResource("Add carousel sample.", "", "/mgnl-bootstrap-samples/pingan-content/carousel/pa-carousel.pingan.homepage-carousel.yaml"));

//        tasks.add(new BootstrapSingleResource("Add news sample.", "", "/mgnl-bootstrap-samples/pingan-content/news/pa-news.pingan.yaml"));

//        tasks.add(new BootstrapSingleResource("Add investor-relations sample.", "", "/mgnl-bootstrap-samples/pingan-content/investor-relations/pa-investor-relations.pingan.yaml"));

//        tasks.add(new BootstrapSingleResource("Add website sample.", "", "/mgnl-bootstrap-samples/pingan-content/website/website.pingan.yaml"));

//        tasks.add(new BootstrapSingleResource("Add profile sample.", "", "/mgnl-bootstrap-samples/pingan-content/profiles/pa-profiles.pingan.Our-Leadership.yaml"));

//        tasks.add(new BootstrapSingleResource("Add profile sample.", "", "/mgnl-bootstrap-samples/pingan-content/profiles/pa-profiles.pingan.Corporate-Governance.yaml"));
        return tasks;
    }

}
