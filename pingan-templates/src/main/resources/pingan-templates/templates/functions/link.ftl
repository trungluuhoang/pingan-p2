[#function getAssetLink node linkProperty]
    [#assign assetlink = ""]
    [#if node.hasProperty(linkProperty)]
        [#assign uuid = node.getProperty(linkProperty).getString()!]
        [#if uuid?has_content && !uuid?starts_with("jcr:")]
            [#assign uuid = "jcr:"+ uuid!]
        [/#if]
        [#assign imagingMimeTypes = ['image/bmp', 'image/jpeg', 'image/gif', 'image/tiff', 'image/png']]
        [#assign asset = damfn.getAsset(uuid)!]
        [#if asset?has_content]
            [#if imagingMimeTypes?seq_contains(asset.mimeType)]
                [#assign assetlink = damfn.getAssetLink(uuid)!]                
            [#else]
                [#assign assetlink = ctx.contextPath + "/resource" + asset.path]
            [/#if]
        [/#if]
    [/#if]
    [#return assetlink]
[/#function]

[#function getLinkByReference contentMap linkProperty workspace]
    [#assign link = ""]
    [#if contentMap?has_content && cmsfn.asJCRNode(contentMap).hasProperty(linkProperty)]
        [#assign target = cmsfn.contentById(cmsfn.asJCRNode(contentMap).getProperty(linkProperty).getString()!, workspace)!]
        [#if target?has_content && !cmsfn.asJCRNode(target).hasProperty("mgnl:deleted")]
            [#assign link = cmsfn.link(target)!]
        [/#if]
    [/#if]
    [#return link]
[/#function]

[#function getLink node urlNode]
    [#assign link = ""]
    [#if node.hasNode(urlNode)!]
        [#assign subNode = node.getNode(urlNode)!]
        [#if subNode?has_content && subNode.hasProperty("field")]
            [#assign contentNode = cmsfn.asContentMap(subNode)!]
            [#assign linkType = contentNode.field!]
            [#-------------- ASSIGNMENTS FOR EACH TYPE --------------]
            [#if linkType=="page"]
                [#assign target = cmsfn.contentById(contentNode.page, "website")!]
                [#--Ensure target node exists and it's not deleted--]
                [#if target?has_content && !cmsfn.asJCRNode(target).hasProperty("mgnl:deleted")]
                    [#assign link = cmsfn.link(target)!]
                [/#if]
            [#elseif linkType=="external"]
                [#assign tempLink = (cmsfn.externalLink(subNode, "external")!)]
                [#if tempLink?has_content]
                    [#assign link = tempLink + "\" target=\"_blank"]
                [/#if]
            [#elseif linkType=="dam"]
                    [#assign link = getAssetLink(cmsfn.asJCRNode(contentNode), "dam")!]
            [/#if]
        [/#if]
    [/#if]
    [#return link]
[/#function]


[#function getProfileTitle node propertyName]
    [#assign link = ""]
    [#if node.hasNode(propertyName)!]
        [#assign subNode = node.getNode(propertyName)!]
        [#list cmsfn.children(subNode) as item]
            [#if item?has_content && item.hasProperty("field")]
                [#assign link += item.getProperty("field").getString() + "</br>"]
            [/#if]
        [/#list]
    [/#if]
    [#return link]
[/#function]

[#function setImageAlt node imgUrl propertyName]
    [#assign imageAlt = ""]
    [#if !node?? ]
        [#return imageAlt]
    [/#if]
    [#if !imgUrl?? || imgUrl==""]
        [#return imageAlt]
    [/#if]
    [#assign jcrNode = cmsfn.asJCRNode(node)]
    [#if jcrNode.hasProperty(propertyName)]
        [#assign imageAlt = jcrNode.getProperty(propertyName).getString()!'Ping An Group']
    [#else]
        [#assign imageAlt = 'Ping An Group']                
    [/#if]     
    [#return imageAlt]

[/#function]