[#-------------- ASSIGNMENTS --------------]
[#assign siteRoot = cmsfn.root(content, "mgnl:page")! /]
[#assign queryStr = ctx.getParameter('queryStr')!?html]
[#assign page = (ctx.getParameter('page')!1)?number]
[#assign limit = 10]
[#assign offset = (page-1)*limit]
[#assign searchStr = queryStr?replace('[\\^\\-+!()]','%','r')?replace('~{2,}','~','r')?replace('^\\u3000+|\\u3000+$','%','r')?trim]
[#-------------- RENDERING --------------]
[#if searchStr == "" || searchStr == "\\"]
  <div class="container">
    <div class="border-bottom border-primary page-title-container">
      <h1>${content.headline!"Search for"} "${queryStr!}"</h1>
      <div class="text-grey">0 results</div>
    </div>
    <h5 class="page-desc font-weight-normal mb-8 mb-sm-10"></h5>
  </div>
  <div class="container">
    <div class="details-page-content">
      <section><div class="border-bottom mb-7"></div></section>
    </div>
  </div>
[#elseif queryStr?has_content]
  [#assign totalSearchResults = searchfn.searchPages(searchStr, siteRoot.@path) /]
  [#assign totalRecordsFound = totalSearchResults?size!0 /]
  [#assign pageNums = (totalRecordsFound/limit)?string['0;;roundingMode=up']?number]
  [#assign prevPage = (page > 1)?then(page - 1, page)]
  [#assign nextPage = (page < pageNums)?then(page +1, page)]
  [#assign searchResults = searchfn.searchPages(searchStr, siteRoot.@path, limit, offset) /]
  [#assign recordsFound = searchResults?size /]
  <div class="container">
    <div class="border-bottom border-primary page-title-container">
      <h1>${content.headline!"Search for"} "${queryStr!}"</h1>
      <div class="text-grey">${totalRecordsFound!} results</div>
    </div>
    <h5 class="page-desc font-weight-normal mb-8 mb-sm-10"></h5>
  </div>
  <div class="container">
    <div class="details-page-content">
      <section>
        <div class="border-bottom mb-7">
          [#if searchResults?has_content]
            [#list searchResults as item]
              <a class="d-block mb-7 w-fit-content search-result" href="${cmsfn.link(item)}">
                <h5 class="mb-2 title">${item.title!}</h5>
                [#--<div class="mb-1 desc">${cmsfn.decode(item).excerpt!}</div>--]
                <div class="text-primary link">${cmsfn.link(item)}</div>
              </a>
            [/#list]
          [/#if]
        </div>
        [#if pageNums > 0]
          <nav class="pagination" aria-label="pagination"><a class="page-link" href="${ctx.contextPath}/search.html?queryStr=${queryStr}&page=${prevPage}" aria-label="Previous" data-page="prev">
            <svg xmlns="http://www.w3.org/2000/svg" width="7.94" height="13.053" viewBox="0 0 7.94 13.053">
                <path d="M12061.466,311.514l-5.112,5.112,5.112,5.112" transform="translate(-12054.939 -310.1)" fill="none" stroke="#919493" stroke-linecap="round" stroke-width="2"></path>
            </svg><span class="sr-only">Previous</span></a>

            [#list 1..pageNums as x]
              <a class="page-link [#if (x==page)]active[/#if]" href="${ctx.contextPath}/search.html?queryStr=${queryStr}&page=${x}" data-page="${x}">${x}</a>
            [/#list]

            <a class="page-link" href="${ctx.contextPath}/search.html?queryStr=${queryStr}&page=${nextPage}" aria-label="Next" data-page="next">
              <svg xmlns="http://www.w3.org/2000/svg" width="7.94" height="13.053" viewBox="0 0 7.94 13.053">
                  <path d="M12061.466,311.514l-5.112,5.112,5.112,5.112" transform="translate(12062.88 323.153) rotate(180)" fill="none" stroke="#919493" stroke-linecap="round" stroke-width="2"></path>
              </svg><span class="sr-only">Next</span></a>
          </nav>
        [/#if]
      </section>
    </div>
  </div>
[#else]
  <div class="container">
    [#if cmsfn.isEditMode()]
      ${i18n['search.editMode']}
    [/#if]
  </div>
[/#if]
