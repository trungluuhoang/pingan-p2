[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign profileListUUID = content.profileList!]
[#assign profileListParent = cmsfn.contentById(profileListUUID, "pa-profiles")!]

[#if profileListParent?has_content]
<div class="container">
    <div class="details-page-content">
        <section>
            <div class="row row-m-4 row-sm-m-6 row-md-m-3 page-vtab-content">
                <div class="col-md-3 col-navs">
                    <div class="nav nav--tab" role="tablist">
                        [#list cmsfn.children(profileListParent, "mgnl:folder") as group]
                            <a class="nav-link h4-responsive col-6 col-md-12 nav-link [#if group?index==0]active[/#if]" data-toggle="tab" href="#${group.@name!}" role="tab" aria-selected="true">${group.jcrName!}</a>
                        [/#list]
                    </div>
                </div>
                <div class="col-md-9 col-content">
                    <div class="tab-content">
                        [#list cmsfn.children(profileListParent, "mgnl:folder") as group]
                            <div class="tab-pane [#if group?index==0]show active[/#if]" role="tabpanel" id="${group.@name!}">
                                <div class="grid--people">
                                    <div class="row row-md-m-4 row-m-2">
                                        [#list cmsfn.children(group, "mgnl:paProfile") as profile]
                                            [#assign wrapForI18n = cmsfn.wrapForI18n(profile)]
                                            [#if wrapForI18n.display?has_content && wrapForI18n.display]
                                            [#assign imageUrl=getAssetLink(cmsfn.asJCRNode(wrapForI18n), "profileImage")!]
                                            [#assign imageAlt=setImageAlt(wrapForI18n, imageUrl, "profileImageAlt")!]
                                                <div class="col-md-4 col-6 mb-6">
                                                    <div class="tile tile-people">
                                                        <div class="thumbnail mb-4 mb-md-3">
                                                            <img class="w-100 rounded object-fit-cover hover-elevation cursor-pointer"
                                                             src="${imageUrl!}" alt="${imageAlt!}">
                                                        </div>
                                                        <h5 class="mb-2 name">${wrapForI18n.profileName!}</h5>
                                                        <div class="desc">${getProfileTitle(cmsfn.asJCRNode(wrapForI18n), "title")}</div>
                                                        <div class="longdesc">${wrapForI18n.description!}</div>
                                                    </div>
                                                </div>
                                            [/#if]
                                        [/#list]
                                    </div>
                                </div>
                            </div>
                        [/#list]
                    </div>
                </div>
            </div>
        </section>
        <div class="modal fade modal-popup modal-popup--people" tabindex="-1" role="dialog" aria-hidden="true" id="modal_popup_people">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <svg width="16.828" height="16.828" viewBox="0 0 16.828 16.828">
                                <g transform="translate(-1316.257 -229.257)">
                                    <line x2="19.799" transform="translate(1317.672 230.672) rotate(45)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
                                    <line y2="19.799" transform="translate(1331.671 230.672) rotate(45)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
                                </g>
                            </svg>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h5 class="text-primary title h3-responsive">Name</h5>
                        <div class="longdesc"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
[/#if]