[#include "/pingan-templates/templates/functions/link.ftl"/]
[#assign desktopImage = getAssetLink(cmsfn.asJCRNode(content), "desktopImage")!""]
[#assign mobileImage = getAssetLink(cmsfn.asJCRNode(content), "mobileImage")!""]
[#assign desktopImageAlt = setImageAlt(content, desktopImage, "desktopImageAlt")!]
[#assign mobileImageAlt = setImageAlt(content, mobileImage, "mobileImageAlt")!]
[#assign url = getLink(cmsfn.asJCRNode(content), "url")!""]
<section>
	<div class="banner banner-video mx-0 mb-8">
		[#if desktopImage?has_content]
			<img class="w-100 rounded-lg img-bg d-desktop" src="${desktopImage!}" alt="${desktopImageAlt!}">
		[/#if]

		[#if mobileImage?has_content]
		<img class="w-100 rounded-lg img-bg d-responsive" src="${mobileImage!}" alt="${mobileImageAlt!}">
		[/#if]

		[#if url?has_content]
		<div class="banner-desc-container">
			<div class="banner-desc">
				<a class="btn btn-outline-white btn-icon glightbox" href="${url!}">
					<svg class="img-icon" xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16">
						<path d="M7.072,2.321a1,1,0,0,1,1.857,0l6.523,16.307A1,1,0,0,1,14.523,20H1.477a1,1,0,0,1-.928-1.371Z" transform="translate(20) rotate(90)"></path>
					</svg>Play</a>
			</div>
		</div>
		[/#if]
	</div>
</section>