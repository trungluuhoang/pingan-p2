[#assign siteRoot = cmsfn.root(content, "mgnl:page")! /]
[#assign pageUrl = ctx.aggregationState.originalBrowserURL!""]
[#assign pageContent = cmsfn.page(content)!content]
[#assign parentPage = cmsfn.parent(pageContent, "mgnl:page")!]
[#if pageContent.categories?has_content]
    [#assign categoriesList = cmsfn.children(pageContent.categories)!]
[/#if]

<div class="container">
    <div class="details-page-content">
        <div class="row article row-m-4 row-md-m-10">

            <div class="col-md-4 col-lg-3 mb-6 mb-md-0 col-article-author">
                <div class="row row-m-2">
                    <!--  <div class="col-auto">
                        <svg xmlns="http://www.w3.org/2000/svg" width="50.578" height="50.578" viewBox="0 0 50.578 50.578">
                            <g id="contacts" transform="translate(0 0.001)">
                                <path id="path_border" d="M43.168,7.411a25.283,25.283,0,1,0,0,35.756,25.269,25.269,0,0,0,0-35.756ZM25.289,46.419a21.13,21.13,0,1,1,21.13-21.13,21.154,21.154,0,0,1-21.13,21.13Z" transform="translate(0 0)" fill="#d8d9da"></path>
                                <path id="path_ppl" d="M165.119,159.216a6.295,6.295,0,1,0-10.424,0,10.522,10.522,0,0,0-5.3,9.13,2.077,2.077,0,0,0,4.153,0,6.362,6.362,0,0,1,12.724,0,2.077,2.077,0,0,0,4.153,0A10.523,10.523,0,0,0,165.119,159.216Zm-5.212-5.67a2.143,2.143,0,1,1-2.143,2.143A2.145,2.145,0,0,1,159.907,153.545Z" transform="translate(-134.618 -134.619)" fill="#bfc1c4"></path>
                            </g>
                        </svg>
                    </div>  -->
                    <div class="col">
                        <div class="mb-1">${pageContent.author!}</div>
                        <div class="small text-grey">${pageContent.date?string("dd MMM yyyy")}</div>
                    </div>
                    <div class="col-md-12 col-auto pt-md-4 pb-0 pb-md-4 mt-md-5 border-md-top border-md-bottom flex-shrink-0">
                        <div class="row row-m-2">
                            <div class="col-auto col-md-3 d-none d-md-block">
                                <div class="small text-grey">Share</div>
                            </div>
                            <div class="col">
                                <div class="row row-m-1">
                                    <div class="col-auto">
                                        <div class="cursor-pointer share-link" data-share-link-template="https://www.facebook.com/sharer/sharer.php?u=${pageUrl!}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35">
                                                <g transform="translate(-422 -869)">
                                                    <circle cx="17.5" cy="17.5" r="17.5" transform="translate(422 869)" fill="#bfc1c4"></circle>
                                                    <path d="M133.629,6.192V3.94a1.126,1.126,0,0,1,1.126-1.126h1.126V0h-2.252a3.377,3.377,0,0,0-3.377,3.377V6.192H128V9.006h2.252v9.006h3.377V9.006h2.252l1.126-2.815Z" transform="translate(308 877.482)" fill="#fff"></path>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="cursor-pointer share-link" data-share-link-template="https://twitter.com/intent/tweet?url=${pageUrl!}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35">
                                                <g transform="translate(-468 -869)">
                                                    <circle cx="17.5" cy="17.5" r="17.5" transform="translate(468 869)" fill="#bfc1c4"></circle>
                                                    <path d="M18.243,49.755a7.8,7.8,0,0,1-2.155.591,3.719,3.719,0,0,0,1.645-2.067,7.474,7.474,0,0,1-2.372.905,3.74,3.74,0,0,0-6.47,2.558,3.851,3.851,0,0,0,.087.853A10.586,10.586,0,0,1,1.27,48.682a3.741,3.741,0,0,0,1.149,5A3.694,3.694,0,0,1,.73,53.22v.041a3.757,3.757,0,0,0,3,3.675,3.733,3.733,0,0,1-.981.123A3.307,3.307,0,0,1,2.038,57,3.776,3.776,0,0,0,5.532,59.6,7.515,7.515,0,0,1,.9,61.2a7.006,7.006,0,0,1-.9-.051,10.529,10.529,0,0,0,5.738,1.678A10.572,10.572,0,0,0,16.383,52.18c0-.165-.006-.325-.014-.483A7.461,7.461,0,0,0,18.243,49.755Z" transform="translate(477.401 831.385)" fill="#fff"></path>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="col-auto">
                                        <div class="cursor-pointer share-link" data-share-link-template="https://www.linkedin.com/sharing/share-offsite/?url=${pageUrl!}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35">
                                                <g transform="translate(-514 -869)">
                                                    <circle cx="17.5" cy="17.5" r="17.5" transform="translate(514 869)" fill="#bfc1c4"></circle>
                                                    <path d="M16.755,19.374v6.194H13.164v-5.78c0-1.452-.519-2.443-1.819-2.443A1.965,1.965,0,0,0,9.5,18.66a2.457,2.457,0,0,0-.119.876v6.033H5.79s.048-9.789,0-10.8H9.382V16.3l-.024.035h.024V16.3a3.567,3.567,0,0,1,3.237-1.785C14.983,14.512,16.755,16.056,16.755,19.374ZM2.033,9.558a1.872,1.872,0,1,0-.048,3.734h.024a1.873,1.873,0,1,0,.024-3.734ZM.213,25.568H3.8v-10.8H.213Z" transform="translate(523 868.442)" fill="#fff"></path>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-article-content">
                <div class="pb-6 border-bottom">
                    [@cms.area name="detail"/]
                </div>
                [#assign tags = tagfn.getTags(cmsfn.page(content))! /]

                [#if tags?has_content]
                    <div class="row row-m-2 mt-5 article-tags">
                        <div class="col-auto title">Tags</div>
                        <div class="col-auto separator">|</div>
                        <div class="col">
                            <div class="row row-m-2">
                                [#list tags as tag ]
                                    <div class="col-auto"><a href="${ctx.contextPath}/search.html?queryStr=${tag}">${tag?replace("-", " ")!}</a></div>
                                [/#list]
                            </div>
                        </div>
                    </div>
                [/#if]
                [#if categoriesList?has_content]
                    <div class="row row-m-2 mt-5 article-tags">
                        <div class="col-auto title">Categories</div>
                        <div class="col-auto separator">|</div>
                        <div class="col">
                            <div class="row row-m-2">
                                [#list categoriesList as catLink ]
                                    [#assign cat = cmsfn.contentById(catLink.field, "category")!]
                                    <div class="col-auto"><a href="${ctx.contextPath}${parentPage.@path?replace('^/en','','r')}.html?category=${cat.@id!}">${cat.name!}</a></div>
                                [/#list]
                            </div>
                        </div>
                    </div>
                [/#if]
            </div>
        </div>
    </div>
</div>
