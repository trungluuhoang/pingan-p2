[#include "/pingan-templates/templates/functions/link.ftl"/]
<section>
    [#if content.displayType?has_content && content.displayType == 'textHighlight']
        <q>${cmsfn.decode(content).desc!""}</q>
    [#elseif content.displayType?has_content && content.displayType == 'textBorder']
        [#assign desktopImage = getAssetLink(cmsfn.asJCRNode(content), "image")!""]
        [#assign desktopImageAlt = setImageAlt(content, desktopImage, "imageAlt")!]
        <div class="row row-m-0 border-top border-bottom border-primary py-8">
            <div class="col-md-3 mb-5 mb-md-0 pr-0 pr-md-8">
                <img class="w-100 rounded" src="${desktopImage!}" alt="${desktopImageAlt!}"></div>
            <div class="col-md-9 ">
                <div class="font-weight-bold">${cmsfn.decode(content).desc!}</div>
                <div class="cursor-pointer" data-toggle="modal" data-target="#${content.@path?replace("/","")}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="2" viewBox="0 0 20 2">
                        <line x2="20" transform="translate(0 1)" fill="none" stroke="#F05A23" stroke-width="2"></line>
                    </svg>
                    <div class="d-inline ml-2 text-grey">Read More</div>
                </div>
            </div>
        </div>
        <div class="modal fade modal-popup modal-popup--more" tabindex="-1" role="dialog" aria-hidden="true"
             id="${content.@path?replace("/","")}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <svg width="16.828" height="16.828" viewBox="0 0 16.828 16.828">
                                <g transform="translate(-1316.257 -229.257)">
                                    <line x2="19.799" transform="translate(1317.672 230.672) rotate(45)" fill="none"
                                          stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
                                    <line y2="19.799" transform="translate(1331.671 230.672) rotate(45)" fill="none"
                                          stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
                                </g>
                            </svg>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h5 class="text-primary title h3-responsive">${cmsfn.decode(content).title!}</h5>
                        ${cmsfn.decode(content).longDesc!}
                    </div>
                </div>
            </div>
        </div>
    [#elseif content.displayType?has_content && content.displayType == 'textPopup']
        <div class="row row-m-4 row-md-m-8">
            <div class="col-md-7 mb-5 mb-md-0">
                <h5 class="text-primary mb-3">${cmsfn.decode(content).title!}</h5>
                ${cmsfn.decode(content).desc!}
            </div>
            <div class="col-md-5">
                <div class="px-8 py-6 mb-8 px-sm-10 py-sm-8 mb-sm-0 bg-orange-gradient text-white rounded tile-more">
                    <h5 class="font-weight-normal text-capitalize mb-4">More About ${cmsfn.decode(content).popupTitle!}</h5>
                    <button class="d-block btn btn-outline-white rounded-pill w-fit-content btn-icon mb-2"
                            data-toggle="modal" data-target="#${content.@path?replace("/","")}">
                        <svg class="img-icon img-icon-sm" xmlns="http://www.w3.org/2000/svg" width="10" height="10"
                             viewBox="0 0 10 10">
                            <g transform="translate(4.241 -8.875)">
                                <line x2="8" transform="translate(-3.241 13.875)" fill="none" stroke="#fff"
                                      stroke-linecap="round" stroke-width="2"></line>
                                <line y2="8" transform="translate(0.759 9.875)" fill="none" stroke="#fff"
                                      stroke-linecap="round" stroke-width="2"></line>
                            </g>
                        </svg>
                        <span class="title">Read More</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="modal fade modal-popup modal-popup--more" tabindex="-1" role="dialog" aria-hidden="true"
             id="${content.@path?replace("/","")}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <svg width="16.828" height="16.828" viewBox="0 0 16.828 16.828">
                                <g transform="translate(-1316.257 -229.257)">
                                    <line x2="19.799" transform="translate(1317.672 230.672) rotate(45)" fill="none"
                                          stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
                                    <line y2="19.799" transform="translate(1331.671 230.672) rotate(45)" fill="none"
                                          stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
                                </g>
                            </svg>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h5 class="text-primary title h3-responsive">${cmsfn.decode(content).popupTitle!}</h5>
                        ${cmsfn.decode(content).popupDesc!}
                    </div>
                </div>
            </div>
        </div>
    [#else]
        ${cmsfn.decode(content).desc!""}
    [/#if]
</section>

