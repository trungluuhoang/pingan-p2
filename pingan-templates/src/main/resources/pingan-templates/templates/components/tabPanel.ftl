[#if cmsfn.isEditMode()]
    <h4 class="text-primary mb-4">Tab: ${content.title!}</h4>
[/#if]
[@cms.area name="detail"/]
