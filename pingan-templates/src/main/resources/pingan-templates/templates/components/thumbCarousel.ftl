<div class="container">
    <div class="details-page-content">
        <div class="swiper swiper--${content.displayType!}">
            <h5 class="text-primary mb-5 mb-md-4">${content.title!}</h5>
            [#if cmsfn.isEditMode()]
                <div>
            [#else]
                <div class="swiper-container">
            [/#if]
                <div class="swiper-wrapper">
                    [@cms.area name="items"/]
                </div>
                [#if !cmsfn.isEditMode()]
                    <div class="swiper-button-prev">
                        <svg xmlns="http://www.w3.org/2000/svg" width="15.125" height="28.966" viewBox="0 0 15.125 28.966">
                            <path d="M7849.8-493.4l-11,12.365,11,12.365" transform="translate(-7836.789 495.518)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"></path>
                        </svg>
                    </div>
                    <div class="swiper-button-next">
                        <svg xmlns="http://www.w3.org/2000/svg" width="15.125" height="28.966" viewBox="0 0 15.125 28.966">
                            <path d="M7838.8-493.4l11,12.365-11,12.365" transform="translate(-7836.679 495.518)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"></path>
                        </svg>
                    </div>
                [/#if]
            </div>
        </div>
    </div>
</div>