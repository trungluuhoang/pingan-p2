[#include "/pingan-templates/templates/functions/link.ftl"/]

<div class="container">
  <div class="details-page-content">
    <section>
      <h5 class="text-primary mb-4">${content.title}</h5>
      <div class="row row-m-4 row-sm-m-5">
      [#list cmsfn.children(content.dataList) as group]
        [#assign imgUrl=getAssetLink(cmsfn.asJCRNode(group), "image")!]
          <div class="col-sm-4 mb-10">
            <div class="h-100 border-bottom pb-3">
              [#assign imageAlt=setImageAlt(group, imgUrl, "imageAlt")!]
              <img class="w-100 rounded mb-4" src="${imgUrl!''}" alt="${imageAlt!}">
              <h5 class="mb-3">${group.title}</h5>
              <ul>
                [#list cmsfn.children(group.textList) as item]
                  <li>${item.field}</li>
                [/#list]
              </ul>
            </div>
          </div>
      [/#list]
      </div>
    </section>
  </div>
</div>