[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign dataListUUID = content.dataList!]
[#assign dataListParent = cmsfn.contentById(dataListUUID, "pa-share")!]

<div class="container">
  <div class="details-page-content">
    <section>
      [#list cmsfn.children(dataListParent, "mgnl:paShare")]
        <div class="grey-table grey-table--responsive">
          <div class="container-fluid rounded grey-table-header d-desktop">
            <div class="pt-3 pb-2">
              [#if dataListParent == 'Major-Shareholders']
                <div class="row row-m-2">
                  <div class="col-3">Shareholder<br>Name</div>
                  <div class="col-2">Shareholder<br>Percentage</div>
                  <div class="col-2">Shares Held<sup>(1)</sup></div>
                  <div class="col-2">Type of Shares</div>
                  <div class="col-3">Change during the Reporting period</div>
                </div>
              [#else]
                <div class="row row-m-2">
                  <div class="col-2">Dividend Period</div>
                  <div class="col-2">A H Shares<br/>Ex-dividend Date</div>
                  <div class="col-4">Dividend Policy<br/>(inclusive of applicable tax)</div>
                  <div class="col-4">Total Amount of Cash Dividend</div>
                </div>
              [/#if]
            </div>
          </div>
          <div class="container-fluid grey-table-body container-p-2">
            [#if dataListParent == 'Major-Shareholders']
              [#items as item]
                <div class="row row-m-2 py-sm-4 pb-4">
                  <div class="col-sm-3 col-title">
                    <h6 class="line-height-1-5 val">${item.dataType.name}</h6>
                  </div>
                  <div class="col-sm-2 col-6">
                    <div class="mb-1 d-responsive label">Shareholder Percentage</div>
                    <div class="val">${item.dataType.percentage}</div>
                  </div>
                  <div class="col-sm-2 col-6">
                    <div class="mb-1 d-responsive label">Shares Held<sup>(1)</sup></div>
                    <div class="val">${item.dataType.held}</div>
                  </div>
                  <div class="col-sm-2 col-6">
                    <div class="mb-1 d-responsive label">Type of Shares</div>
                    <div class="val">${item.dataType.type}</div>
                  </div>
                  <div class="col-sm col-6">
                    <div class="mb-1 d-responsive label">Shares Change</div>
                    <div class="val">${item.dataType.change}</div>
                  </div>
                  <div class="col-sm-auto">
                    <button class="btn btn-sm btn-outline-grey rounded-pill border-border-grey btn-icon mt-1 mt-sm-0 btn-remarks" type="button" aria-expanded="false" data-toggle="modal" data-target="#modal_popup_remarks_${item_index}">
                      <svg class="img-icon" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10">
                        <g transform="translate(-15.5 -11.5)">
                          <line x2="8" transform="translate(16.5 16.5)" fill="none" stroke="#f05a23" stroke-linecap="round" stroke-width="2"></line>
                          <line y2="8" transform="translate(20.5 12.5)" fill="none" stroke="#f05a23" stroke-linecap="round" stroke-width="2"></line>
                        </g>
                      </svg>Remarks
                    </button>
                  </div>
                </div>
              [/#items]
            [#else]
              [#items as item]
                <div class="row row-m-2 py-sm-4 pb-4">
                  <div class="col-sm-2 col-title">
                    <h6 class="line-height-1-5 val">${item.dataType.period}</h6>
                  </div>
                  <div class="col-sm-2 col-6">
                    <div class="mb-1 d-responsive title">A H Shares Ex-dividend Date</div>
                    <div class="line-height-1-5 val">${item.dataType.date}</div>
                  </div>
                  <div class="col-sm-4 col-6">
                    <div class="mb-1 d-responsive label">Dividend Policy (inclusive of applicable tax)</div>
                    <div class="val">${cmsfn.decode(item.dataType).policy!}</div>
                  </div>
                  <div class="col-sm-4 col-6">
                    <div class="mb-1 d-responsive label">Total Amount of Cash Dividend</div>
                    <div class="val">${item.dataType.cash}</div>
                  </div>
                </div>
              [/#items]
            [/#if]
          </div>
        </div>
      [/#list]
      [#if dataListParent == 'Major-Shareholders']
        [#list cmsfn.children(dataListParent, "mgnl:paShare") as item]
          <div class="modal fade modal-popup" tabindex="-1" role="dialog" id="modal_popup_remarks_${item_index}" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <svg width="16.828" height="16.828" viewBox="0 0 16.828 16.828">
                      <g transform="translate(-1316.257 -229.257)">
                        <line x2="19.799" transform="translate(1317.672 230.672) rotate(45)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
                        <line y2="19.799" transform="translate(1331.671 230.672) rotate(45)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
                      </g>
                    </svg>
                  </button>
                </div>
                <div class="modal-body">
                  <h5 class="text-primary title h3-responsive"></h5>
                  <h5 class="text-primary mb-8 line-height-1-5 title title--remarks">${item.dataType.name!""}</h5>
                  <div class="content border-bottom">
                    <div class="row mb-1">
                      <div class="col-12">
                        <div class="p-2 bg-grey text-white rounded">Nature of shareholder</div>
                      </div>
                      <div class="col-12">
                        <div class="p-2 pb-4">${item.dataType.nature!""}</div>
                      </div>
                    </div>
                    <div class="row mb-1">
                      <div class="col-12">
                        <div class="p-2 bg-grey text-white rounded">Number of selling-restricted shares held</div>
                      </div>
                      <div class="col-12">
                        <div class="p-2 pb-4">${item.dataType.sellingRestricted!""}</div>
                      </div>
                    </div>
                    <div class="row mb-1">
                      <div class="col-12">
                        <div class="p-2 bg-grey text-white rounded">Number Of Pledged Or Frozen Shares</div>
                      </div>
                      <div class="col-12">
                        <div class="p-2 pb-4">${item.dataType.pledged!""}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        [/#list]
      [/#if]
    </section>
  </div>
</div>
