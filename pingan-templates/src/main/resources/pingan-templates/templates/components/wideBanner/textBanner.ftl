[#include "/pingan-templates/templates/functions/link.ftl"/]
[#assign url = getLink(cmsfn.asJCRNode(content), "url")!""]

[#if cmsfn.isEditMode()]
    <div class="col-sm-4">
[#else]
    <div class="col-sm-6">
[/#if]

    <div class="h-100 d-flex">
        <div class="m-auto content">
            <h1 class="text-primary mb-3 mb-sm-5">${content.title!}</h1>
            <h5 class="font-weight-normal mb-6">${content.description!}</h5>
            [#if url != ""]
                <a href="${url}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="2" viewBox="0 0 20 2">
                        <line x2="20" transform="translate(0 1)" fill="none" stroke="#F05A23" stroke-width="2"></line>
                    </svg>
                    <div class="d-inline ml-2">${content.urlText!""}</div>
                </a>
            [/#if]
        </div>
    </div>
</div>