<section class="section--stock">
    <div class="container">
        <div class="section-content">
            <div class="row">
                <div class="col-md-4">
                    <div class="mb-6 mb-md-0 px-0 px-md-8 d-flex flex-column h-100" id="stock-info-inner-desk">
                        [@cms.area name="leftColumn"/]
                    </div>
                </div>
                <div class="col-md-8">
                    [@cms.area name="rightColumn"/]
                </div>
            </div><a class="d-responsive mt-7 text-grey" href="#" id="stock-info-inner-responsive">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="2" viewBox="0 0 24 2">
                    <line x2="24" transform="translate(0 1)" fill="none" stroke="#F05A23" stroke-width="2"></line>
                </svg><span class="ml-2"></span></a>
                <script>
                    window.addEventListener('load', function() {
                        var wrap = document.getElementById('stock-info-inner-desk');
                        var targ = document.getElementById('stock-info-inner-responsive');
                        targ.href = wrap.querySelector('a').href;
                        targ.querySelector('span').innerHTML = wrap.querySelector('a span').innerHTML;
                    });
                </script>
        </div>
    </div>
</section>
