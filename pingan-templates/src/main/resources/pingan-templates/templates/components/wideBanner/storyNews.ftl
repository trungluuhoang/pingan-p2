[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign siteRoot = cmsfn.root(content, "mgnl:page")!"" /]
[#assign sitePage = cmsfn.page(content)!"" /]
[#assign numOfNews = content.numOfNews?has_content?then(content.numOfNews?number, 3)]
[#assign newsDetailsTemplate = 'pingan-templates:pages/perspectivesDetails']
[#assign jcrQuery = "SELECT * from [nt:base] AS p WHERE [mgnl:template]='${newsDetailsTemplate}' AND ISDESCENDANTNODE('/${siteRoot!}/${sitePage!}') order by [date] desc"]
[#assign latestNews = adsearchfn.search("website",jcrQuery!, "JCR-SQL2", "mgnl:page", numOfNews, 0)!]

<div class="col-lg-6 mb-9 mb-lg-0">
	<div class="h-100 d-flex">
		<div class="m-auto content">
			[#if latestNews?size > 0]
				<div class="home-item-list">
					<h4 class="font-weight-normal h3-responsive list-title">${content.lastestNewTitle!"Ping An News"}</h4>
					<div class="list-items mb-5 mb-sm-6">
						[#list latestNews as item]
							[#assign wrapForI18nNode = cmsfn.wrapForI18n(item)]
							[#assign urlNewsItem = cmsfn.link(item)!""]
							[#assign itemMap = cmsfn.asContentMap(wrapForI18nNode)]
							<a class="d-block py-3 pt-sm-6 pb-sm-5 border-bottom list-item" href="${urlNewsItem!}">

								<small class="text-grey mb-2">[#if itemMap.date?has_content]${itemMap.date?string("dd MMM yyyy")}[/#if]</small>
								<div class="title">${itemMap.title!}</div>
							</a>
						[/#list]
					</div>
					<a class="link-all" href="${getLinkByReference(content, "allNewsPage", "website")}">
						<svg class="mr-2" xmlns="http://www.w3.org/2000/svg" width="24" height="2" viewBox="0 0 24 2">
							<line x2="24" fill="none" stroke="#F05A23" stroke-linecap="round" stroke-width="2"></line>
						</svg>
						${content.allNewsTitle!"All News"}
					</a>
				</div>
			[/#if]
		</div>
	</div>
</div>
