[#include "/pingan-templates/templates/functions/link.ftl"/]
[#--[#include "/pingan-templates/templates/function/functions.ftl"/]--]

[#assign siteRoot = cmsfn.root(content, "mgnl:page")!"" /]
[#assign verticalSlidersUUID = content.verticalSliders!]
[#assign verticalSlidersUUIDParent = cmsfn.contentById(verticalSlidersUUID, "pa-carousel")!]
[#assign numOfNews = content.numOfNews?has_content?then(content.numOfNews?number, 3)]
[#assign numOfIR = content.numOfIR?has_content?then(content.numOfIR?number, 3)]
[#assign newsDetailsTemplate = 'pingan-templates:pages/newsDetails']


[#if cmsfn.isEditMode()]
    <div class="col-md-2">
[#else]
    <div class="col-md-3">
[/#if]
        <div class="tiles d-flex d-md-block">
            [#list cmsfn.children(verticalSlidersUUIDParent, "mgnl:paCarousel") as item]
                [#assign wrapForI18nContent = cmsfn.wrapForI18n(item)]
                [#assign urlVsItem = getLink(cmsfn.asJCRNode(wrapForI18nContent), "url")!""]
                [#assign desktopImage = getAssetLink(cmsfn.asJCRNode(wrapForI18nContent), "desktopImage")!]
                [#assign desktopImageAlt = setImageAlt(wrapForI18nContent, desktopImage, "desktopImageAlt")!]
                <a class="d-block position-relative mb-6 pr-md-0 tile-top pr-6" href="${urlVsItem!""}">
                    <div class="mb-5 mb-md-0 big-title">${((item?index)+1)?string["00"]!}</div>
                    <h5 class="font-weight-normal text-dark-grey mb-4 d-responsive">${wrapForI18nContent.mainTitle!}</h5>
                    <div class="rounded-lg position-relative hover-elevation">
                        <img src="${desktopImage!}" alt="${desktopImageAlt!}">
                        <div class="position-absolute-0000 blur-overlay"></div>
                        <div class="position-absolute-0000 text-white d-flex">
                            <div class="mt-auto p-6 d-desktop">${wrapForI18nContent.mainTitle!}</div>
                        </div>
                    </div>
                </a>
            [/#list]
        </div>
    </div>

[#if cmsfn.isEditMode()]
    <div class="col-md-2">
[#else]
    <div class="col-md-3">
[/#if]
    <div class="lists">
        [#assign jcrQuery = "SELECT * from [nt:base] AS p WHERE [mgnl:template]='${newsDetailsTemplate}' AND ISDESCENDANTNODE('/${siteRoot!}') order by [date] desc"]
        [#assign latestNews = adsearchfn.search("website",jcrQuery!, "JCR-SQL2", "mgnl:page", numOfNews, 0)!]
        [#if latestNews?size > 0]
            <div class="home-item-list mb-10">
                <h4 class="font-weight-normal h3-responsive">${content.lastestNewTitle!"Latest News"}</h4>
                <div class="list-items mb-5 mb-sm-6">
                    [#list latestNews as item]
                        [#assign wrapForI18nNode = cmsfn.wrapForI18n(item)]
                        [#assign contentMapNode = cmsfn.asContentMap(item)]
                        [#assign urlNewsItem = cmsfn.link(item)!""]
                        [#assign itemMap = cmsfn.asContentMap(wrapForI18nNode)]
                        [#assign desktopImage = getAssetLink(item, "desktopImage")!]
                        [#assign desktopImageAlt = setImageAlt(contentMapNode, desktopImage, "desktopImageAlt")!]
                        <a class="d-block py-3 pt-sm-6 pb-sm-5 border-bottom list-item" href="${urlNewsItem!}">
                            <small class="text-grey mb-2">[#if itemMap.date?has_content]${itemMap.date?string("dd MMM yyyy")}[/#if]</small>
                            <div class="title">${itemMap.title!}</div>
                            <img class="mt-5 d-desktop" src="${desktopImage!}" alt="${desktopImageAlt!}"></a>
                    [/#list]
                </div>
                <a class="link-all" href="${getLinkByReference(content, "allNewsPage", "website")}">
                    <svg class="mr-2" xmlns="http://www.w3.org/2000/svg" width="24" height="2" viewBox="0 0 24 2">
                        <line x2="24" fill="none" stroke="#F05A23" stroke-linecap="round" stroke-width="2"></line>
                    </svg>
                    ${content.allNewsTitle!"All News"}
                </a>
            </div>
        [/#if]

        [#assign investorRelations = model.getLatestData(numOfIR, "pa-investor-relations", "mgnl:paInvestorRelations", "")!]
        [#if investorRelations?size > 0]
            <div class="home-item-list">
                <h4 class="font-weight-normal h3-responsive">${content.iRTitle!"Investor Relations"}</h4>
                <div class="list-items mb-5 mb-sm-6">
                    [#list investorRelations as item]
                        [#assign wrapForI18nNode = cmsfn.wrapForI18n(item)]
                        [#assign urlIrItem = getLink(wrapForI18nNode, "url")!""]
                        [#assign itemMap = cmsfn.asContentMap(wrapForI18nNode)]
                        <a class="d-block py-3 pt-sm-6 pb-sm-5 border-bottom list-item" href="${urlIrItem}">
                            <small class="text-grey mb-2">[#if itemMap.date?has_content]${itemMap.date?string("dd MMM yyyy")}[/#if]</small>
                            <div class="title">${itemMap.mainTitle!}</div>
                        </a>
                    [/#list]
                </div>
                <a class="link-all" href="${getLinkByReference(content, "allIRPage", "website")}">
                    <svg class="mr-2" xmlns="http://www.w3.org/2000/svg" width="24" height="2" viewBox="0 0 24 2">
                        <line x2="24" fill="none" stroke="#F05A23" stroke-linecap="round" stroke-width="2"></line>
                    </svg>
                    ${content.allIRTitle!"Investor Relations"}
                </a>
            </div>
        [/#if]
    </div>
</div>
