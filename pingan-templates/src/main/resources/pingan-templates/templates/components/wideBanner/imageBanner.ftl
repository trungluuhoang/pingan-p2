[#include "/pingan-templates/templates/functions/link.ftl"/]
[#assign link = getLink(cmsfn.asJCRNode(content), "image")!""]
[#assign damAlt = setImageAlt(content.image, link, "damAlt")!]

[#if cmsfn.isEditMode()]
    <div class="col-sm-4 mb-7 mb-sm-0">
[#else]
    <div class="col-sm-6 mb-7 mb-sm-0">
[/#if]
        <img class="w-100" src="${link!}" alt="${damAlt!}">
</div>