<section class="section--top">
    <div class="container-fluid container-p-4 container-lg-p-10">
        <div class="row row-listing">

            [@cms.area name="leftColumn"/]

            [@cms.area name="rightColumn"/]
        </div>
    </div>
</section>