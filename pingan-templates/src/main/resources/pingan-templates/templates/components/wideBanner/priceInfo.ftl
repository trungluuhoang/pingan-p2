[#include "/pingan-templates/templates/functions/link.ftl"/]
[#assign url = getLink(cmsfn.asJCRNode(content), "url")!""]

<h5 class="h3-responsive title">${content.title!}</h5><a class="d-desktop text-grey mt-auto" href="${url}">
<svg xmlns="http://www.w3.org/2000/svg" width="24" height="2" viewBox="0 0 24 2">
    <line x2="24" transform="translate(0 1)" fill="none" stroke="#F05A23" stroke-width="2"></line>
</svg><span class="ml-2">${content.urlText!""}</span></a>
