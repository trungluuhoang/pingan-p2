<h5 class="text-primary mb-6">${content.title!}</h5>
<div class="documents">
    [@cms.area name="documents"/]

    <div class="row w-100 mt-10">
        <div class="m-auto">
            <div class="btn-loadmore">
                <button class="btn btn-sm btn-outline-grey rounded-pill border-border-grey btn-icon btn-arrow" type="button" aria-expanded="false">
                    <svg class="img-icon" xmlns="http://www.w3.org/2000/svg" width="13.116" height="7.867" viewBox="0 0 13.116 7.867">
                        <path d="M12325.452,862.175l5.143,5.052,5.145-5.052" transform="translate(-12324.037 -860.761)" fill="none" stroke="#f05a23" stroke-linecap="round" stroke-width="2"></path>
                    </svg><span class="title--close">Load More</span><span class="title--open">Load More</span>
                </button>
            </div>
        </div>
    </div>
</div>

