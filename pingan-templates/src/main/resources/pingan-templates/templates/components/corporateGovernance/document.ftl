[#include "/pingan-templates/templates/functions/link.ftl"/]
[#assign url = getLink(cmsfn.asJCRNode(content), "url")!""]

<a class="document document-pdf mb-3" href="${url!}" target="_blank">${content.title!}</a>

