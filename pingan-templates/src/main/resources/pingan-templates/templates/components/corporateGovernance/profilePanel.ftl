[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign profileListUUID = content.profileList!]
[#assign profileListParent = cmsfn.contentById(profileListUUID, "pa-profiles")!]
[#assign name = content.title!profileListParent.jcrName!]
[#if cmsfn.isEditMode()]
    <h4 class="text-primary mb-4">Tab: ${name!}</h4>
[/#if]
[#if profileListParent?has_content]
<div class="grid--people">
    <div class="row row-md-m-4 row-m-2">
        [#list cmsfn.children(profileListParent, "mgnl:paProfile") as profile]
            [#assign wrapForI18n = cmsfn.wrapForI18n(profile)]
            [#if wrapForI18n.display?has_content && wrapForI18n.display]
                [#assign imageUrl = getAssetLink(cmsfn.asJCRNode(wrapForI18n), "profileImage")!]
                [#assign imageAlt =setImageAlt(wrapForI18n, imageUrl, "profileImageAlt")!]
                <div class="col-md-4 col-6 mb-6">
                    <div class="tile tile-people">
                        <div class="thumbnail mb-4 mb-md-3">
                            <img class="w-100 rounded object-fit-cover hover-elevation cursor-pointer"
                                 src="${imageUrl!}" alt="${imageAlt!}">
                        </div>
                        <h5 class="mb-2 name">${wrapForI18n.profileName!}</h5>
                        <div class="desc">${getProfileTitle(cmsfn.asJCRNode(wrapForI18n), "title")}</div>
                        <div class="longdesc">${wrapForI18n.description!}</div>
                    </div>
                </div>
            [/#if]
        [/#list]
    </div>
</div>
[/#if]
[@cms.area name="info"/]