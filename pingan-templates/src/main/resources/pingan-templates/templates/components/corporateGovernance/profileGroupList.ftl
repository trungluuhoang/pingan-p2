<section>
    <div class="row row-m-4 row-sm-m-6 row-md-m-3 page-vtab-content">
        [#if content.list?has_content]
        <div class="col-md-3 col-navs">
            <div class="nav nav--tab" role="tablist">
                [#list cmsfn.children(content.list, "mgnl:component") as item]
                    [#assign profileListUUID = item.profileList!]
                    [#assign profileListParent = cmsfn.contentById(profileListUUID, "pa-profiles")!]
                    [#assign name = item.title!profileListParent.jcrName!]
                    <a class="nav-link h4-responsive nav-link [#if item?index==0]active[/#if]" data-toggle="tab" href="#${name?replace(" ", "_")?replace("&", "_")?lower_case}" role="tab" aria-selected="true">${name!}</a>
                [/#list]
            </div>
        </div>
        [/#if]
        <div class="col-md-9 col-content">
            <div class="tab-content">
                [@cms.area name="list"/]
            </div>
        </div>
    </div>
</section>
<div class="modal fade modal-popup modal-popup--people" tabindex="-1" role="dialog" aria-hidden="true" id="modal_popup_people">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <svg width="16.828" height="16.828" viewBox="0 0 16.828 16.828">
                        <g transform="translate(-1316.257 -229.257)">
                            <line x2="19.799" transform="translate(1317.672 230.672) rotate(45)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
                            <line y2="19.799" transform="translate(1331.671 230.672) rotate(45)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
                        </g>
                    </svg>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="text-primary title h3-responsive">Name</h5>
                <div class="longdesc"></div>
            </div>
        </div>
    </div>
</div>