[#list components as component ]
    [#assign profileListUUID = component.profileList!]
    [#assign profileListParent = cmsfn.contentById(profileListUUID, "pa-profiles")!]
    [#assign name = component.title!profileListParent.jcrName!]
    <div class="tab-pane [#if component?index==0 || cmsfn.isEditMode()]show active[/#if]" role="tabpanel" id="${name?replace(" ", "_")?replace("&", "_")?lower_case}">
        [@cms.component content=component /]
    </div>
[/#list]
