[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign carouselListUUID = content.carouselList!]
[#assign carouselListParent = cmsfn.contentById(carouselListUUID, "pa-carousel")!]

<section class="section--top">
  <div class="container-fluid container-p-4 container-lg-p-10">
    <div class="swiper--top-container">
      <div class="swiper swiper--top">
        <div class="swiper-container swiper-container-initialized swiper-container-horizontal">
          <div class="swiper-wrapper">
          [#list cmsfn.children(carouselListParent, "mgnl:paCarousel") as item]
            [#assign wrapForI18nContent = cmsfn.wrapForI18n(item)]
            [#assign urlItem = getLink(cmsfn.asJCRNode(wrapForI18nContent), "url")!""]

            [#assign desktopImage = getAssetLink(cmsfn.asJCRNode(wrapForI18nContent), "desktopImage")!]
            [#assign mobileImage = getAssetLink(cmsfn.asJCRNode(wrapForI18nContent), "mobileImage")!]
            [#assign desktopImageAlt=setImageAlt(wrapForI18nContent, desktopImage,"desktopImageAlt")!]
            [#assign mobileImageAlt=setImageAlt(wrapForI18nContent,mobileImage, "mobileImageAlt")!]
            <div class="swiper-slide">
              <div class="banner">
                <img class="w-100 rounded-lg img-bg d-desktop" alt="${desktopImageAlt!}" src="${desktopImage!}">
                <img class="w-100 rounded-lg img-bg d-responsive" alt="${mobileImageAlt!}" src="${mobileImage!}">
                <div class="banner-desc-container">
                  <div class="banner-desc">
                    [#assign titleColor = ""]
                    [#if wrapForI18nContent.titleColor?has_content && wrapForI18nContent.titleColor == "gray"]
                        [#assign titleColor = "color:#B4B4B4"]
                    [/#if]
                    <h2 class="title" style="${titleColor}">${wrapForI18nContent.mainTitle!}</h2>
                    [#assign urlAttrs = ' href="' + (urlItem?replace('[\'"]\\s*target=[\'"]_blank\\s*$','','r')?trim) + '" ']
                    [#assign glightbox = ' glightbox ']
                    [#if wrapForI18nContent.openNewTab?has_content && wrapForI18nContent.openNewTab]
                      [#assign urlAttrs = urlAttrs + ' target="_blank"']
                      [#assign glightbox = '']
                    [/#if]
                    [#if wrapForI18nContent.urlType?has_content && wrapForI18nContent.urlType == 'video']
                      <a class="btn btn-outline-white btn-icon ${glightbox}" ${urlAttrs}>
                        <svg class="img-icon" xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16">
                          <path d="M7.072,2.321a1,1,0,0,1,1.857,0l6.523,16.307A1,1,0,0,1,14.523,20H1.477a1,1,0,0,1-.928-1.371Z" transform="translate(20) rotate(90)"></path>
                        </svg>
                        ${wrapForI18nContent.urlText!"Watch Video"}
                      </a>
                    [#else]
                      <a class="btn btn-outline-white btn-icon" ${urlAttrs}>
                        <svg class="img-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="2" viewBox="0 0 24 2">
                          <line x2="24" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2" opacity="0.75"></line>
                        </svg>${wrapForI18nContent.urlText!"Learn More"}</a>
                    [/#if]
                  </div>
                </div>
              </div>
            </div>
          [/#list]
          </div>
          <div class="swiper-button-prev" tabindex="0" role="button" aria-label="Previous slide">
            <svg xmlns="http://www.w3.org/2000/svg" width="15.125" height="28.966" viewBox="0 0 15.125 28.966">
              <path d="M7849.8-493.4l-11,12.365,11,12.365" transform="translate(-7836.789 495.518)" fill="none" stroke="#fff"
                stroke-linecap="round" stroke-width="3"></path>
            </svg>
          </div>
          <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide">
            <svg xmlns="http://www.w3.org/2000/svg" width="15.125" height="28.966" viewBox="0 0 15.125 28.966">
              <path d="M7838.8-493.4l11,12.365-11,12.365" transform="translate(-7836.679 495.518)" fill="none" stroke="#fff"
                stroke-linecap="round" stroke-width="3"></path>
            </svg>
          </div>
          <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
        </div>
      </div>
    </div>
  </div>
</section>
