[#include "/pingan-templates/templates/functions/link.ftl"/]
[#assign url = getLink(cmsfn.asJCRNode(content), "url")!""]
[#assign imagesList = content.imagesList!]
[#assign numOfImages = cmsfn.children(imagesList)?size?string["00"]!]

[#if content.imageOnLeft]
  [#assign infoClass = "order-0 order-sm-1"]
  [#assign swiperClass = "order-1 order-sm-0"]
[#else]
  [#assign infoClass = "order-1 order-sm-0"]
  [#assign swiperClass = "order-0 order-sm-1"]
[/#if]

<section>
  <div class="container">
    <div class="home-numbered-swiper">
      <div class="row">
        <div class="col-sm-6 home-numbered-swiper-col-desc ${infoClass}">
          <div class="h-100 desc-container">
            <div class="position-relative h-100">
              <h5 class="text-primary font-weight-normal mb-4">${content.mainTitle!}</h5>
              <h2 class="text-green mb-3">${content.subTitle!}</h2>
              <h5 class="font-weight-normal mb-6 mb-sm-4">${content.description!}</h5>
              <a class="read-more" href="${url}">
                <svg class="img-icon" xmlns="http://www.w3.org/2000/svg" width="20" height="2" viewBox="0 0 20 2">
                  <line x2="20" transform="translate(0 1)" fill="none" stroke="#F05A23" stroke-width="2"></line>
                </svg>
                <div class="d-inline text-grey ml-2">${content.urlText}</div>
              </a>
              <div class="position-absolute d-desktop-flex align-items-baseline w-100 page">
                <h5 class="font-weight-normal mr-1 current-page"></h5>
                <small class="text-grey">/ ${numOfImages}</small>
                <div class="ml-3 flex-grow-1 border-bottom border-grey pb-1"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6 home-numbered-swiper-col-img ${swiperClass}">
          <div class="position-relative img-container">
            <div class="swiper swiper--home-numbered">
              <div class="swiper-container swiper-container-initialized swiper-container-horizontal">
                <div class="swiper-wrapper">
                  [#list cmsfn.children(content.imagesList) as item]
                    [#assign imgUrl = getAssetLink(cmsfn.asJCRNode(item), "field")!]
                    [#assign imageAlt=setImageAlt(item,imgUrl, "alt")!]
                    <div class="swiper-slide">
                      <img class="w-100 rounded-lg" src="${imgUrl}" alt="${imageAlt!}">
                    </div>
                  [/#list]
                </div>
                <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
              </div>
            </div>
            <div class="position-absolute big-title"></div>
            <div class="btn btn-primary position-absolute swiper-button-next--home-numbered" tabindex="0" role="button" aria-label="Next slide">
              <svg xmlns="http://www.w3.org/2000/svg" width="43.73" height="16.045" viewBox="0 0 43.73 16.045">
                <g transform="translate(-891.995 -2900.478)">
                  <path d="M7838.8-493.4l6.438,5.9-6.438,5.9" transform="translate(-6911.729 3395.997)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"></path>
                  <line x2="40" transform="translate(893.495 2908.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"></line>
                </g>
              </svg>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>