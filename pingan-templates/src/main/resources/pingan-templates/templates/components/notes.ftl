<div class="container">
  <section>
    <div class="mt-5 mt-md-7 grey-table-footer text-grey">
      <small>
        <p>${content.text}</p>
      </small>
    </div>
  </section>
</div>