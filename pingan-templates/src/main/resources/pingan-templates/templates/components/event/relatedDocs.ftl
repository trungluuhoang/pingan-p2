[#include "/pingan-templates/templates/macros/events.ftl"/]

<section class="section--related-docs">
    <div class="event-year related-docs" id="year_Related Documents">
        <div class="header py-3 px-4 py-sm-4 px-sm-6 rounded text-grey mb-6 mb-sm-8 h5-responsive">${content.title!"Related documents"}</div>
        <div class="event-dates">
            [@renderRelatedDoc content.relatedDocs!/]
        </div>
    </div>
</section>