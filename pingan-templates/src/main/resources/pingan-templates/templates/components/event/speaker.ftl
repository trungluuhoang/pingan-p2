[#include "/pingan-templates/templates/functions/link.ftl"/]
[#assign desktopImage = getAssetLink(cmsfn.asJCRNode(content), "desktopImage")!""]
[#assign desktopImageAlt =setImageAlt(content, desktopImage, "desktopImageAlt")!]

<div class="col-6 col-md-3">
    <div class="mb-5"><img class="w-100 rounded-circle mb-5" src="${desktopImage!}" alt="${desktopImageAlt!}">
        <h5 class="font-weight-medium mb-3">${content.title!}</h5>${cmsfn.decode(content).description!}
    </div>
</div>
