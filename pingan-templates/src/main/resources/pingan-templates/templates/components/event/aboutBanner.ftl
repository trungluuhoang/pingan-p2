[#include "/pingan-templates/templates/functions/link.ftl"/]
[#assign desktopImage = getAssetLink(cmsfn.asJCRNode(content), "desktopImage")!""]
[#assign mobileImage = getAssetLink(cmsfn.asJCRNode(content), "mobileImage")!""]
[#assign desktopImageAlt =setImageAlt(content, desktopImage, "desktopImageAlt")!]
[#assign mobileImageAlt =setImageAlt(content, mobileImage, "mobileImageAlt")!]

[#assign url = getLink(cmsfn.asJCRNode(content), "url")!""]

<div class="banner banner-video mx-0 mb-8">
    <img class="w-100 rounded-lg img-bg d-desktop" src="${desktopImage!}" alt="${desktopImageAlt!}">
    <img class="w-100 rounded-lg img-bg d-responsive" src="${mobileImage!}" alt="${mobileImageAlt!}">
    <div class="banner-desc-container">
        <div class="banner-desc">
            [#if content.title?has_content]
                <h2 class="title">${content.title}</h2>
            [/#if]
            [#if content.description?has_content]
                <p class="desc mb-5">${content.description}</p>
            [/#if]

            [#if content.text?has_content || content.subText?has_content]
                <div class="mb-4">
                    <p class="font-weight-bold mb-0">${content.text!}</p>
                    <small>${content.subText!}</small>
                </div>
            [/#if]

            [#if url?has_content]
                [#assign urlAttrs = ' href="' + (url?replace('[\'"]\\s*target=[\'"]_blank\\s*$','','r')?trim) + '" ']
                [#assign glightbox = '']
                [#if content.urlType?has_content && content.urlType == "video"]
                    [#assign glightbox = ' glightbox ']
                [/#if]
                [#if content.openNewTab?has_content && content.openNewTab]
                    [#assign urlAttrs = urlAttrs + ' target="_blank"']
                    [#assign glightbox = '']
                [/#if]
                <a class="btn btn-outline-white btn-icon ${glightbox}" ${urlAttrs}>
                    [#if content.urlType?has_content && content.urlType == "video"]
                        <svg class="img-icon" xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16">
                            <path d="M7.072,2.321a1,1,0,0,1,1.857,0l6.523,16.307A1,1,0,0,1,14.523,20H1.477a1,1,0,0,1-.928-1.371Z" transform="translate(20) rotate(90)"></path>
                        </svg>
                    [#else]
                        <svg class="img-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="2" viewBox="0 0 24 2">
                            <line x2="24" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2" opacity="0.75"></line>
                        </svg>
                    [/#if]
                    ${content.urlText}
                </a>
            [/#if]
        </div>
    </div>
</div>
