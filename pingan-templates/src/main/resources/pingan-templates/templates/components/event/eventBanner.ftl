[#include "/pingan-templates/templates/functions/link.ftl"/]
[#assign desktopImage = getAssetLink(cmsfn.asJCRNode(content), "desktopImage")!""]
[#assign mobileImage = getAssetLink(cmsfn.asJCRNode(content), "mobileImage")!""]
[#assign desktopImageAlt =setImageAlt(content, desktopImage, "desktopImageAlt")!]
[#assign mobileImageAlt =setImageAlt(content, mobileImage, "mobileImageAlt")!]
[#assign url = getLink(cmsfn.asJCRNode(content), "url")!""]

<section class="section--top">
    <div class="banner">
        <img class="w-100 rounded-lg img-bg d-desktop" src="${desktopImage}" alt="${desktopImageAlt!}">
        <img class="w-100 rounded-lg img-bg d-responsive" src="${mobileImage}" alt="${mobileImageAlt!}">
        <div class="banner-desc-container">
            <div class="banner-desc">
                [#assign titleAndDescColor = ""]
                [#if content.titleAndDescColor?has_content && content.titleAndDescColor == "gray"]
                    [#assign titleAndDescColor = "color:#B4B4B4"]
                [/#if]
                [#if content.title?has_content]
                    <h2 class="title" style="${titleAndDescColor}">${content.title}</h2>
                [/#if]
                [#if content.description?has_content]
                    <p class="desc mb-5" style="${titleAndDescColor}">${content.description}</p>
                [/#if]
            </div>
        </div>
    </div>
    <div class="bg-light py-4 px-5 py-md-8 px-md-10 rounded shadow event-details">
        <div class="row">
            <div class="col-md-3 mb-3 mb-md-0">
                [#if content.startDate?has_content]
                    <h5 class="text-primary mb-2">Start Date</h5>
                    [#if content.showStartTime?has_content && content.showStartTime]
                        <div>${content.startDate?string("dd MMM yyyy h:mm a")}</div>
                    [#else]
                        <div>${content.startDate?string("dd MMM yyyy")}</div>
                    [/#if]
                [/#if]
            </div>
            <div class="col-md-3 mb-3 mb-md-0">
                [#if content.endDate?has_content]
                    <h5 class="text-primary mb-2">End Date</h5>
                    [#if content.showEndTime?has_content && content.showEndTime]
                        <div>${content.endDate?string("dd MMM yyyy h:mm a")!}</div>
                    [#else]
                        <div>${content.endDate?string("dd MMM yyyy")!}</div>
                    [/#if]
                    [#--[#assign endDate = content.endDate?string("dd MMM yyyy h:mm a")!""]--]
                    [#--<div>${endDate!""}</div>--]
                [/#if]
            </div>
            [#if content.venue?has_content]
                <div class="col-md-6 mb-3 mb-md-0">
                    <h5 class="text-primary mb-2">Venue / Webcast</h5>
                    [#if content.venue?starts_with("http")]
                        <div>
                            <a class="d-flex mb-3 text-dark-grey item item--video glightbox" href="${content.venue}">
                                <svg class="mr-3 flex-shrink-0 img-icon" xmlns="http://www.w3.org/2000/svg" width="22" height="22.966"
                                    viewBox="0 0 22.966 22.966">
                                    <path d="M11.483,0A11.483,11.483,0,1,0,22.966,11.483,11.483,11.483,0,0,0,11.483,0Zm3.969,12.092L9.71,15.68a.718.718,0,0,1-1.1-.609V7.895a.718.718,0,0,1,1.1-.609l5.741,3.588a.718.718,0,0,1,0,1.217Z"
                                        fill="#f05a23"></path>
                                </svg>
                                ${content.venue}
                            </a>
                        </div>
                    [#else]
                        <div>${content.venue!""}</div>
                    [/#if]
                </div>
            [/#if]
        </div>
    </div>
</section>
