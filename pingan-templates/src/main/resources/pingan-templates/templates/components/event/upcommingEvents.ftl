[#include "/pingan-templates/templates/functions/link.ftl"/]
[#assign eventBannerTemplate = 'pingan-templates:components/event/eventBanner']

[#if content.upcommingEvents?has_content]
    [#assign eventList = cmsfn.children(content.upcommingEvents)]
    [#if eventList?size > 0]
        <h5 class="text-primary mb-5 mb-sm-7">Upcoming Events</h5>
    [/#if]
    [#list eventList as item]
        [#assign eventPage = cmsfn.contentById(item.field)]
        [#assign eventBannerComponents = adsearchfn.search("website", "SELECT * from [nt:base] AS p WHERE [mgnl:template]='${eventBannerTemplate}' AND ISDESCENDANTNODE('${eventPage.@path}') ", "JCR-SQL2", "mgnl:component")!]
        [#if eventBannerComponents?size > 0]
            [#assign event = cmsfn.asContentMap(eventBannerComponents[0])]
            <div class="banner rounded-lg">
                <div class="content">
                    <div class="row row-m-4 row-sm-m-8 text-white">
                        <div class="col-auto border-right">
                            <div class="text-center date">
                                <h3 class="font-weight-normal h2-responsive">${event.date?string("dd")}</h3>
                                <div class="w-month small rounded text-white mx-auto h6-responsive month">${event.date?string("MMM")}</div>
                            </div>
                        </div>
                        <div class="col">
                            <h3 class="mb-2"><a href="${cmsfn.link(cmsfn.page(event))}" target="_blank" class="text-white">${event.title!''}</a></h3>
                            <h6 class="font-weight-normal">${event.description!''}</h6>
                        </div>
                    </div>
                    <div class="row row-m-4 row-sm-m-8 text-white">
                        <div class="col-auto border-right d-desktop">
                            <div class="date"></div>
                        </div>
                        <div class="col">
                            [#if event.venue?has_content && event.venue?starts_with("http")]
                                <a class="btn btn-outline-white btn-icon mt-8 btn-watch-webcast" href="${event.venue}" target="_blank">
                                    <svg class="img-icon" xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16">
                                        <path d="M7.072,2.321a1,1,0,0,1,1.857,0l6.523,16.307A1,1,0,0,1,14.523,20H1.477a1,1,0,0,1-.928-1.371Z" transform="translate(20) rotate(90)" fill="#fff"></path>
                                    </svg>
                                    Watch Video
                                </a>
                            [#else]
                                <div>Venue: ${event.venue!""}</div>
                            [/#if]
                        </div>
                    </div>
                </div>
            </div>
            <br/>
        [/#if]

    [/#list]
[/#if]
