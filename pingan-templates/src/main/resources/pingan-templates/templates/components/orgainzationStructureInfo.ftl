[#include "/pingan-templates/templates/functions/link.ftl"/]
[#assign desktopImage = getAssetLink(cmsfn.asJCRNode(content), "desktopImage")!""]
[#assign mobileImage = getAssetLink(cmsfn.asJCRNode(content), "mobileImage")!""]
[#assign desktopImageAlt=setImageAlt(content, desktopImage,"desktopImageAlt")!]
[#assign mobileImageAlt=setImageAlt(content,mobileImage, "mobileImageAlt")!]      

[#if content.displayType?has_content && content.displayType == 'contentOnly']
  <section>
    <p>${content.content!""}</p>
  </section>
[#elseif content.displayType?has_content && content.displayType == 'contentWithTitle']
    <section>
      <h2>${content.title!""}</h2>
      <p>${content.content!""}</p>
    </section>
[#else]
  <section>
    <div class="banner">
      [#if desktopImage?has_content]  
        <img class="w-100 rounded-lg img-bg d-desktop" src="${desktopImage}" alt="${desktopImageAlt!}">
      [/#if]
      [#if mobileImage?has_content]    
        <img class="w-100 rounded-lg img-bg d-responsive" src="${mobileImage}" alt="${mobileImageAlt!}">
      [/#if]     
    </div>
  </section>
[/#if]