[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign image = getAssetLink(cmsfn.asJCRNode(content), "image")!""]
[#assign imageAlt=setImageAlt(content, image, "imageAlt")!]
<img class="" src="${image!}" alt="${imageAlt!}">