<button class="d-block btn btn-outline-white rounded-pill w-fit-content btn-icon mb-2 btn-modal-more">
    <svg class="img-icon img-icon-sm" xmlns="http://www.w3.org/2000/svg" width="10" height="10"
         viewBox="0 0 10 10">
        <g transform="translate(4.241 -8.875)">
            <line x2="8" transform="translate(-3.241 13.875)" fill="none" stroke="#fff"
                  stroke-linecap="round" stroke-width="2"></line>
            <line y2="8" transform="translate(0.759 9.875)" fill="none" stroke="#fff"
                  stroke-linecap="round" stroke-width="2"></line>
        </g>
    </svg>
    <span class="title">${content.title!}</span>
</button>

<div class="modal fade modal-popup modal-popup--more" tabindex="-1" role="dialog" aria-hidden="true"
     id="modal_popup_more">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <svg width="16.828" height="16.828" viewBox="0 0 16.828 16.828">
                        <g transform="translate(-1316.257 -229.257)">
                            <line x2="19.799" transform="translate(1317.672 230.672) rotate(45)" fill="none"
                                  stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
                            <line y2="19.799" transform="translate(1331.671 230.672) rotate(45)" fill="none"
                                  stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
                        </g>
                    </svg>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="text-primary title h3-responsive">${content.title!}</h5>
                <div class="mb-5 border-bottom">
                    <div class="mb-4 font-weight-medium">A Share</div>
                    <div class="row mb-1">
                        <div class="col-12">
                            <div class="p-2 bg-grey text-white rounded">Number of shares</div>
                        </div>
                        <div class="col-12">
                            <div class="p-2 pb-4">${content.aShareNumber!}</div>
                        </div>
                    </div>
                    <div class="row mb-1">
                        <div class="col-12">
                            <div class="p-2 bg-grey text-white rounded">Approximate percentage of issued share
                                capital
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="p-2 pb-4">${content.aSharePercentage!}</div>
                        </div>
                    </div>
                </div>
                <div class="mb-5 border-bottom">
                    <div class="mb-4 font-weight-medium">H Share</div>
                    <div class="row mb-1">
                        <div class="col-12">
                            <div class="p-2 bg-grey text-white rounded">Number of shares</div>
                        </div>
                        <div class="col-12">
                            <div class="p-2 pb-4">${content.hShareNumber!}</div>
                        </div>
                    </div>
                    <div class="row mb-1">
                        <div class="col-12">
                            <div class="p-2 bg-grey text-white rounded">Approximate percentage of issued share capital</div>
                        </div>
                        <div class="col-12">
                            <div class="p-2 pb-4">${content.hSharePercentage!}</div>
                        </div>
                    </div>
                </div>
                <div class="mt-7 font-weight-medium">Total share: ${content.aHTotal!}</div>
            </div>
        </div>
    </div>
</div>
