<section>
    <div class="row row-m-4 row-sm-m-6 row-md-m-3 page-vtab-content">
        [#if content.list?has_content]
        <div class="col-md-3 col-navs" style="">
            <div class="nav nav--tab" role="tablist">
                [#list cmsfn.children(content.list, "mgnl:component") as item]
                    <a class="nav-link h4-responsive nav-link [#if item?index==0]active[/#if]" data-toggle="tab" href="#${cmsfn.decode(item).title?replace(" ", "_")?replace("&", "_")?lower_case}" role="tab" aria-selected="true">${item.title!}</a>

                [/#list]
            </div>
        </div>
        [/#if]
        <div class="col-md-9 col-content">
            <div class="tab-content">
                [@cms.area name="list"/]
            </div>
        </div>
    </div>
</section>