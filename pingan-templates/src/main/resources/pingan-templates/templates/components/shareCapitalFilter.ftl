[#include "/pingan-templates/templates/macros/filters.ftl"/]
[#assign pageContent = cmsfn.page(content)!content]
[#assign siteRoot = cmsfn.root(content, "mgnl:page")! /]

<script type="text/template" id="template_year">
    <div class="header py-3 px-4 py-sm-4 px-sm-6 rounded text-grey mb-6 mb-sm-8 h5-responsive">{item.year}</div>

</script>
<script type="text/template" id="template_share">
    <div class="row row-m-3 row-sm-m-8 mb-8 event-date">
        <div class="col col-content">
            <h5 class="font-weight-normal mb-1 mb-sm-2 h3-responsive">{item.title}</h5>
            <p class="mb-3 mb-sm-5">{item.description}</p>
            <div class="row row-m-1 row-sm-m-2">
                <div class="col-auto">
                    <svg xmlns="http://www.w3.org/2000/svg" width="50.578" height="50.578" viewBox="0 0 50.578 50.578">
                        <g transform="translate(10.06 15.247)">
                            <path d="M198.358,366.345a2.976,2.976,0,0,0-2.7,4.2l-6.049,6.08a2.887,2.887,0,0,0-1.266-.281,2.82,2.82,0,0,0-1.25.281l-2.469-2.469a2.968,2.968,0,1,0-5.658-1.25,3.129,3.129,0,0,0,.328,1.375l-4.189,4.126a2.967,2.967,0,1,0,1.532,2.594,2.9,2.9,0,0,0-.2-1.047l4.361-4.3a3.028,3.028,0,0,0,1.141.219,3.125,3.125,0,0,0,1.266-.281l2.454,2.454a2.887,2.887,0,0,0-.281,1.266,2.97,2.97,0,0,0,5.939,0,2.868,2.868,0,0,0-.281-1.25l6.049-6.08a2.887,2.887,0,0,0,1.266.281,2.962,2.962,0,1,0,.016-5.924Zm-24.695,15.6a.938.938,0,1,1,.938-.938A.94.94,0,0,1,173.663,381.943Zm8.268-8.1a.938.938,0,1,1,.938-.938A.94.94,0,0,1,181.931,373.847Zm6.408,6.408a.938.938,0,1,1,.938-.938A.941.941,0,0,1,188.339,380.255Zm10.019-10a.938.938,0,1,1,.938-.938A.94.94,0,0,1,198.358,370.252Z" transform="translate(-170.693 -366.345)" fill="#bfc1c4"></path>
                            <path d="M43.168,7.411a25.283,25.283,0,1,0,0,35.756,25.269,25.269,0,0,0,0-35.756ZM25.289,46.419a21.13,21.13,0,1,1,21.13-21.13,21.154,21.154,0,0,1-21.13,21.13Z" transform="translate(-10.06 -15.247)" fill="#d8d9da"></path>
                        </g>
                    </svg>
                </div>
                <div class="col d-flex align-items-center">{item.change}<br>{item.total_equity}</div>
            </div>
        </div>
    </div>


</script>

<section>
    <form class="form-filters mb-6">
        <input type="hidden" id="filter_parentPath" value="${pageContent.@path}">
        <input type="hidden" id="filter_endpoint" value="${ctx.contextPath}/.rest/pingan/v1/query/shareCapitals">
        <input type="hidden" id="filter_loadMoreLimit" value="3">
        <input type="hidden" name="offset" id="filter_offset" value="0">

        [@yearFilter [2015, 2014, 2011, 2010, 2007, 2004, 2003, 2001, 1997] /]
    </form>
    <div class="event-year cards"></div>
</section>
