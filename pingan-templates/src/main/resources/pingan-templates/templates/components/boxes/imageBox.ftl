[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign urlLink = getLink(cmsfn.asJCRNode(content), "url")!""]

[#assign desktopImage = getAssetLink(cmsfn.asJCRNode(content), "desktopImage")!]
[#assign desktopImageAlt=setImageAlt(content, desktopImage, "desktopImageAlt")!]
<a class="d-block position-relative tile-more-border-radius tile-more tile-more--img" href="${urlLink}">
    <div class="position-relative tile-content"><img class="w-100 h-100 object-fit-cover tile-more-border-radius" src="${desktopImage!}" alt="${desktopImageAlt!}">
        <div class="position-absolute-0000 d-flex text-white p-10">
            <h5 class="mb-0 font-weight-normal d-desktop mt-auto">${content.title!}</h5>
        </div>
    </div>
    <h5 class="mt-2 mb-0 font-weight-normal line-height-1-5 d-responsive">${content.title!}</h5>
</a>
