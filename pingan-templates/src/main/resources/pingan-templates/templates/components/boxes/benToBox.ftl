<section class="section--tiles">
	<div class="container">
		<div class="tiles">
			<div class="row">
				<div class="col-sm-6 col-lg-3">
					[@cms.area name="box1"/]
				</div>
				<div class="col-sm-6 col-lg-3">
					[@cms.area name="box2"/]
				</div>
				<div class="col-sm-6 col-lg-3">
					[@cms.area name="box3"/]
				</div>
				<div class="col-sm-6 col-lg-3">
					[@cms.area name="box4"/]
				</div>
			</div>
		</div>
	</div>
</section>