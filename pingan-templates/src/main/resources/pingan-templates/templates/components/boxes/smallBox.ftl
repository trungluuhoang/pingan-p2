[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign urlLink = getLink(cmsfn.asJCRNode(content), "url")!""]

[#assign desktopImage = getAssetLink(cmsfn.asJCRNode(content), "desktopImage")!]
[#assign desktopImageAlt=setImageAlt(content, desktopImage,"desktopImageAlt")!]
[#if urlLink?has_content]
    [#assign anchor = content.anchor!""]
    [#assign anchor = anchor?replace('\\s|\\u3000','','r')]
    [#if anchor?has_content]
        [#assign urlLink = urlLink + "#" + anchor]
    [/#if]

    [#assign desktopImage = getAssetLink(cmsfn.asJCRNode(content), "desktopImage")!]
    [#assign desktopImageAlt=setImageAlt(content, desktopImage,"desktopImageAlt")!]
    [#if content.boxType == 'imageBox']
        <a class="d-block position-relative tile-more-border-radius tile-more tile-more--img" href="${urlLink}">
            <div class="position-relative tile-content"><img class="w-100 h-100 object-fit-cover tile-more-border-radius" src="${desktopImage!}" alt="${desktopImageAlt!}">
                <div class="position-absolute-0000 d-flex text-white py-5 px-7">
                    <h5 class="mb-0 font-weight-normal d-desktop mt-auto">${content.title!}</h5>
                </div>
            </div>
            <h5 class="mt-2 mb-0 font-weight-normal line-height-1-5 d-responsive">${content.title!}</h5>
        </a>
    [#elseif content.boxType == 'textBox']
        [#assign bgClass = "bg-orange-gradient"]
        [#if content.bgcolor?has_content && content.bgcolor == 'gray']
            [#assign bgClass = "bg-gray-gradient"]
        [/#if]
        <a class="d-block position-relative tile-more-border-radius tile-more tile-more--orange-gradient" href="${urlLink}">
            <div class="position-relative tile-content ${bgClass}">
                <div class="position-absolute-0000 d-flex flex-column text-white py-5 px-7">
                    <h5 class="mb-0 font-weight-normal text-white mb-auto">${content.title!}</h5>
                    <div class="text-white-75">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="2" viewBox="0 0 24 2">
                            <line x2="24" transform="translate(0 1)" fill="none" stroke="#fff" stroke-width="2" opacity="0.75"></line>
                        </svg><span class="ml-2">Learn More</span>
                    </div>
                </div>
            </div>
        </a>
    [#elseif content.boxType == 'videoBox']
        <div class="d-block position-relative tile-more-border-radius tile-more tile-more--video">
            <div class="position-relative tile-content"><img class="w-100 h-100 object-fit-cover tile-more-border-radius" src="${desktopImage!}" alt="${desktopImageAlt!}">
                <div class="position-absolute-0000 d-flex py-5 px-7"><a class="btn btn-outline-white btn-icon mt-auto w-fit-content glightbox" href="${urlLink}">
                        <svg class="img-icon" xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16">
                            <path d="M7.072,2.321a1,1,0,0,1,1.857,0l6.523,16.307A1,1,0,0,1,14.523,20H1.477a1,1,0,0,1-.928-1.371Z" transform="translate(20) rotate(90)" fill="#fff"></path>
                        </svg>Play</a></div>
            </div>
        </div>
    [/#if]
[#else]
    [#if content.boxType == 'imageBox']
        <div class="d-block position-relative tile-more-border-radius tile-more tile-more--img">
            <div class="position-relative tile-content"><img class="w-100 h-100 object-fit-cover tile-more-border-radius" src="${desktopImage!}" alt="${desktopImageAlt!}">
                <div class="position-absolute-0000 d-flex text-white py-5 px-7">
                    <h5 class="mb-0 font-weight-normal d-desktop mt-auto">${content.title!}</h5>
                </div>
            </div>
            <h5 class="mt-2 mb-0 font-weight-normal line-height-1-5 d-responsive">${content.title!}</h5>
        </div>
    [#elseif content.boxType == 'textBox']
        [#assign bgClass = "bg-orange-gradient"]
        [#if content.bgcolor?has_content && content.bgcolor == 'gray']
            [#assign bgClass = "bg-gray-gradient"]
        [/#if]
        <div class="d-block position-relative tile-more-border-radius tile-more tile-more--orange-gradient">
            <div class="position-relative tile-content ${bgClass}">
                <div class="position-absolute-0000 d-flex flex-column text-white py-5 px-7">
                    <h5 class="mb-0 font-weight-normal text-white mb-auto">${content.title!}</h5>
                </div>
            </div>
        </div>
    [#elseif content.boxType == 'videoBox']
        <div class="d-block position-relative tile-more-border-radius tile-more tile-more--video">
            <div class="position-relative tile-content"><img class="w-100 h-100 object-fit-cover tile-more-border-radius" src="${desktopImage!}" alt="${desktopImageAlt!}">
                <div class="position-absolute-0000 d-flex py-5 px-7">
                </div>
            </div>
        </div>
    [/#if]
[/#if]