[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign urlLink = getLink(cmsfn.asJCRNode(content), "url")!""]
[#assign backgroundColorClass = "bg-light text-secondary"]
[#if content.backgroundColor == "1"]
  [#assign backgroundColorClass = "tile-darken"]
[/#if]
[#assign desktopImage = getAssetLink(cmsfn.asJCRNode(content), "desktopImage")!]
[#assign desktopImageAlt=setImageAlt(content, desktopImage, "desktopImageAlt")!]
<a class="px-8 py-6 px-sm-10 py-sm-8 mb-4 mb-sm-0 ${backgroundColorClass} rounded d-flex hover-elevation hover-text-decor-none text-center tile tile--icon" href="${urlLink}">
	<div class="m-auto d-flex d-sm-block align-items-center">
		<div class="p-0 pr-10 px-sm-8 pb-sm-8 mb-0 mb-sm-8 border-secondary tile-icon-container">
			<img src="${desktopImage!}" alt="${desktopImageAlt!}">
		</div>
		<p class="ml-10 ml-sm-0 mb-0">${content.title!}</p>
	</div>
</a>