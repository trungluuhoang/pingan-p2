<div class="px-6 py-5 px-sm-10 py-sm-8 mb-4 mb-sm-0 bg-orange-gradient text-white rounded hover-elevation hover-text-decor-none tile tile--orange-gradient">
	<h5 class="h3-responsive font-weight-normal text-capitalize mb-4">${content.title!}</h5>
	<p class="mb-0">${content.description!}</p>
</div>