<section class="section--more">
    <div class="container">
        <h3 class="text-center mb-9 mb-lg-10">${content.title!}</h3>
        <div class="container-grid">
            [@cms.area name="leftColumn"/]
            [@cms.area name="rightColumn"/]
        </div>
    </div>
</section>
