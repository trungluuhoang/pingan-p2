<section>
	<div class="container-w-lg">
		<div class="row row-m-4 row-sm-m-10">
			<div class="col-sm-6 mb-10">
				[@cms.area name="box1"/]
			</div>
			<div class="col-sm-6 mb-10">
				[@cms.area name="box2"/]
			</div>
			<div class="col-sm-6 mb-10">
				[@cms.area name="box3"/]
			</div>
			<div class="col-sm-6 mb-10">
				[@cms.area name="box4"/]
			</div>
		</div>
	</div>
</section>