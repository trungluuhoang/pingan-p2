[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign desktopImage = getAssetLink(cmsfn.asJCRNode(content), "desktopImage")!]
[#assign desktopImageAlt=setImageAlt(content, desktopImage, "desktopImageAlt")!]
<img class="w-100 rounded mb-3" src="${desktopImage!}" alt="${desktopImageAlt!}">
<h5 class="mb-3">${content.title!}</h5>
<p>${content.description!}</p>