[#include "/pingan-templates/includes/ftls/functions.ftl"]
<div class="container">
    <div class="details-page-content">
        <section>
            <div class="row">
                <div class="col-md-4 mb-6 mb-md-0">
                    <div class="row row-m-2">
                        <!--  <div class="col-auto">
                            <svg xmlns="http://www.w3.org/2000/svg" width="50.578" height="50.578" viewBox="0 0 50.578 50.578">
                                <g id="contacts" transform="translate(0 0.001)">
                                    <path id="path_border" d="M43.168,7.411a25.283,25.283,0,1,0,0,35.756,25.269,25.269,0,0,0,0-35.756ZM25.289,46.419a21.13,21.13,0,1,1,21.13-21.13,21.154,21.154,0,0,1-21.13,21.13Z" transform="translate(0 0)" fill="#d8d9da"></path>
                                    <path id="path_ppl" d="M165.119,159.216a6.295,6.295,0,1,0-10.424,0,10.522,10.522,0,0,0-5.3,9.13,2.077,2.077,0,0,0,4.153,0,6.362,6.362,0,0,1,12.724,0,2.077,2.077,0,0,0,4.153,0A10.523,10.523,0,0,0,165.119,159.216Zm-5.212-5.67a2.143,2.143,0,1,1-2.143,2.143A2.145,2.145,0,0,1,159.907,153.545Z" transform="translate(-134.618 -134.619)" fill="#bfc1c4"></path>
                                </g>
                            </svg>
                        </div>  -->
                        <div class="col">
                            <div class="mb-1">Ping An Group</div>
                            <div class="small text-grey">${getDate(content)}</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <h3 class="text-primary mb-6 mb-md-8">${content.mainTitle!}</h3>
                    <div class="content pb-6 border-bottom">
                        [#if content.description?has_content]
                            ${cmsfn.decode(content).description!}
                        [/#if]
                    </div>
                    <div class="row row-m-2 mt-5 tags">
                        <div class="col-auto title">Tags</div>
                        <div class="col-auto separator">|</div>
                        <div class="col">
                            <div class="row">
                                <div class="col-auto"><a href="#">General Meeting</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>