<section class="section--tiles">
    <div class="container">
        <div class="section-content container-grid container-grid--main">
            [@cms.area name="grid"/]
        </div>
    </div>
</section>