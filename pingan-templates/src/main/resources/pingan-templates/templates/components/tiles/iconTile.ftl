[#assign pageUUID = content.page!]
[#assign page = cmsfn.contentById(pageUUID, "website")!]

[#if page?has_content]
<a class="hover-elevation hover-text-decor-none tile tile--icon text-dark-grey rounded bg-light d-flex text-center"
   href="${cmsfn.link(page)!}">
    <div class="m-auto d-flex d-sm-block align-items-center">
        <div class="p-0 pr-10 px-sm-8 pb-sm-8 mb-0 mb-sm-8 border-secondary tile-icon-container">
            [#switch content.type!]
                [#case 'type1']
                    <svg xmlns="http://www.w3.org/2000/svg" width="66" height="68" viewBox="0 0 66 68">
                        <g transform="translate(-1015 -1124)">
                            <rect width="29" height="29" rx="14.5" transform="translate(1015 1124)" fill="#005e3c"></rect>
                            <rect width="29" height="29" rx="2" transform="translate(1052 1124)" fill="#005e3c"></rect>
                            <g transform="translate(1015 1163)" fill="none" stroke="#f05a23" stroke-width="8">
                                <rect width="29" height="29" rx="2" stroke="none"></rect>
                                <rect x="4" y="4" width="21" height="21" rx="2" fill="none"></rect>
                            </g>
                            <g transform="translate(-312 11)">
                                <rect width="9" height="9" rx="4.5" transform="translate(1364 1153)" fill="#a5a5a5"></rect>
                                <rect width="9" height="9" rx="4.5" transform="translate(1364 1170)" fill="#005e3c"></rect>
                                <rect width="9" height="9" rx="4.5" transform="translate(1382 1153)" fill="#005e3c"></rect>
                                <rect width="9" height="9" rx="4.5" transform="translate(1382 1170)" fill="#a5a5a5"></rect>
                            </g>
                        </g>
                    </svg>
                    [#break]
                [#case 'type2']
                    <svg xmlns="http://www.w3.org/2000/svg" width="70.004" height="70.004" viewBox="0 0 70.004 70.004">
                        <g id="Group_1905" data-name="Group 1905" transform="translate(-1465.998 -1119.998)">
                            <path id="Union_4" data-name="Union 4" d="M25,50A25,25,0,1,0,0,25,25,25,0,0,0,25,50Z" transform="translate(1465.998 1119.998)" fill="#005e3c"></path>
                            <g id="Union_8" data-name="Union 8" transform="translate(1536.002 1190.002) rotate(180)" fill="none">
                                <path d="M25,50A25,25,0,1,0,0,25,25,25,0,0,0,25,50Z" stroke="none"></path>
                                <path d="M 25.00104713439941 42.00043106079102 C 34.37544631958008 42.00043106079102 42.00209045410156 34.37416076660156 42.00209045410156 25.00021743774414 C 42.00209045410156 15.62627410888672 34.37544631958008 8.000002861022949 25.00104713439941 8.000002861022949 C 15.62664699554443 8.000002861022949 8.000003814697266 15.62627410888672 8.000003814697266 25.00021743774414 C 8.000003814697266 34.37416076660156 15.62664699554443 42.00043106079102 25.00104713439941 42.00043106079102 M 25.00104713439941 50.00043106079102 C 11.19334697723389 50.00043106079102 4.277256721252343e-06 38.80746078491211 4.277256721252343e-06 25.00021743774414 C 4.277256721252343e-06 11.19297504425049 11.19334697723389 3.152029876218876e-06 25.00104713439941 3.152029876218876e-06 C 38.80874633789062 3.152029876218876e-06 50.00209045410156 11.19297504425049 50.00209045410156 25.00021743774414 C 50.00209045410156 38.80746078491211 38.80874633789062 50.00043106079102 25.00104713439941 50.00043106079102 Z" stroke="none" fill="#f05a23"></path>
                            </g>
                        </g>
                    </svg>
                    [#break]

                [#case 'type3']
                    <svg xmlns="http://www.w3.org/2000/svg" width="74.521" height="47.504" viewBox="0 0 74.521 47.504">
                        <g id="Group_1568" data-name="Group 1568" transform="translate(-1066 -1451.056)">
                            <g id="Group_1565" data-name="Group 1565" transform="translate(-15)">
                                <rect id="Rectangle_758" data-name="Rectangle 758" width="8" height="36" rx="2" transform="translate(1095 1462.56)" fill="#f05a23"></rect>
                                <rect id="Rectangle_759" data-name="Rectangle 759" width="8" height="47.383" rx="2" transform="translate(1109 1451.177)" fill="#f05a23"></rect>
                                <rect id="Rectangle_760" data-name="Rectangle 760" width="8" height="24" rx="2" transform="translate(1081 1474.56)" fill="#f05a23"></rect>
                            </g>
                            <g id="Group_1548" data-name="Group 1548" transform="translate(2072.447 -505.671) rotate(45)">
                                <rect id="Rectangle_659" data-name="Rectangle 659" width="34.557" height="6.174" rx="2" transform="translate(727.328 2047.817) rotate(90)" fill="#005e3c"></rect>
                                <g id="Group_1426" data-name="Group 1426" transform="translate(704.383 2042.883)">
                                    <rect id="Rectangle_660" data-name="Rectangle 660" width="28.233" height="6.174" rx="2" transform="translate(24.33 4.366) rotate(135)" fill="#005e3c"></rect>
                                    <rect id="Rectangle_661" data-name="Rectangle 661" width="28.234" height="7.051" rx="2" transform="translate(20.558 0) rotate(45)" fill="#005e3c"></rect>
                                </g>
                            </g>
                        </g>
                    </svg>
                    [#break]
                [#default]
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="99" height="54" viewBox="0 0 99 54">
                        <defs>
                            <clipPath id="clip-path">
                                <rect id="Rectangle_627" data-name="Rectangle 627" width="50" height="20" transform="translate(0 -0.118)" fill="#a5a5a5" stroke="#a5a5a5" stroke-width="1"></rect>
                            </clipPath>
                            <clipPath id="clip-path-2">
                                <rect id="Rectangle_627-2" data-name="Rectangle 627" width="49" height="20" transform="translate(-0.283 -0.118)" fill="#a5a5a5" stroke="#a5a5a5" stroke-width="1"></rect>
                            </clipPath>
                            <clipPath id="clip-path-3">
                                <rect id="Rectangle_627-3" data-name="Rectangle 627" width="66" height="26" transform="translate(0.231 -0.038)" fill="#a5a5a5" stroke="#a5a5a5" stroke-width="1"></rect>
                            </clipPath>
                        </defs>
                        <g id="Group_1569" data-name="Group 1569" transform="translate(-1420 -1456)">
                            <g id="Group_1550" data-name="Group 1550" transform="translate(1420 1469.332)">
                                <g id="Group_1363" data-name="Group 1363" transform="translate(0 0)">
                                    <ellipse id="Ellipse_125" data-name="Ellipse 125" cx="10" cy="10.5" rx="10" ry="10.5" transform="translate(14 -0.332)" fill="#005e3c"></ellipse>
                                    <g id="Mask_Group_137" data-name="Mask Group 137" transform="translate(0 20.786)" clip-path="url(#clip-path)">
                                        <path id="Path_307" data-name="Path 307" d="M14.205,0A14.205,14.205,0,1,1,0,14.205,14.205,14.205,0,0,1,14.205,0Z" transform="translate(10.718 5.031)" fill="none" stroke="#005e3c" stroke-width="6"></path>
                                    </g>
                                </g>
                            </g>
                            <g id="Group_1555" data-name="Group 1555" transform="translate(1470.283 1469.332)">
                                <g id="Group_1554" data-name="Group 1554" transform="translate(0 0)">
                                    <g id="Group_1550-2" data-name="Group 1550">
                                        <g id="Group_1363-2" data-name="Group 1363" transform="translate(0)">
                                            <circle id="Ellipse_125-2" data-name="Ellipse 125" cx="10.5" cy="10.5" r="10.5" transform="translate(14.717 -0.332)" fill="#005e3c"></circle>
                                            <g id="Mask_Group_137-2" data-name="Mask Group 137" transform="translate(0 20.786)" clip-path="url(#clip-path-2)">
                                                <path id="Path_307-2" data-name="Path 307" d="M14.205,0A14.205,14.205,0,1,1,0,14.205,14.205,14.205,0,0,1,14.205,0Z" transform="translate(10.729 5.031)" fill="none" stroke="#005e3c" stroke-width="6"></path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                            <g id="Group_1552" data-name="Group 1552" transform="translate(1435.769 1456)">
                                <g id="Group_1363-3" data-name="Group 1363" transform="translate(0)">
                                    <circle id="Ellipse_125-3" data-name="Ellipse 125" cx="13.5" cy="13.5" r="13.5" transform="translate(20.231 0)" fill="#f05a23"></circle>
                                    <g id="Mask_Group_137-3" data-name="Mask Group 137" transform="translate(0 28.038)" clip-path="url(#clip-path-3)">
                                        <path id="Path_307-3" data-name="Path 307" d="M19,0A19,19,0,1,1,0,19,19,19,0,0,1,19,0Z" transform="translate(14.785 6.102)" fill="none" stroke="#f05a23" stroke-width="6"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
            [/#switch]
        </div>
        <p class="ml-10 ml-sm-0 mb-0"></p>
        <div class="icon-width">${content.desc1!}<br>${content.desc2!}</div>
    </div>
</a>
[/#if]
