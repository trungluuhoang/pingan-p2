[#assign eventBannerTemplate = 'pingan-templates:components/event/eventBanner']
[#assign eventUUID = content.event!]
[#assign eventPage = cmsfn.contentById(eventUUID, "website")!]
[#assign eventPageLink = eventPage!""]
[#if eventPage?has_content && !cmsfn.asJCRNode(eventPage).hasProperty("mgnl:deleted")]
    [#assign eventPageLink = cmsfn.link(eventPage)!]
[/#if]
[#assign eventAllUUID = content.eventAll!]
[#assign eventAllPage = cmsfn.contentById(eventAllUUID, "website")!]
[#assign eventBannerComponents = adsearchfn.search("website", "SELECT * from [nt:base] AS p WHERE [mgnl:template]='${eventBannerTemplate}' AND ISDESCENDANTNODE('${eventPage.@path}') ", "JCR-SQL2", "mgnl:component")!]
[#if eventBannerComponents?size > 0]
    [#assign event = cmsfn.asContentMap(eventBannerComponents[0])]
    [#assign startDateMonth = ""]
    [#assign startDateDay = ""]
    [#if event.startDate?has_content]
        [#assign startDateMonth = event.startDate?string("MMM")]
        [#assign startDateDay = event.startDate?string("dd")]
    [#elseif event.date?has_content]
        [#assign startDateMonth = event.date?string("MMM")]
        [#assign startDateDay = event.date?string("dd")]
    [/#if]
<div class="hover-elevation hover-text-decor-none tile tile--event rounded-lg d-flex flex-column">
    <h3 class="text-white mb-5">IR Events</h3>
    <div class="row row-m-2 text-white mb-4 irevent-date">
        <div class="col-auto border-right">
            <div class="text-center date">
                <h3 class="font-weight-normal h2-responsive">${startDateDay}</h3>
                <div class="w-month small rounded text-white mx-auto h6-responsive month">${startDateMonth}</div>
            </div>
        </div>
        <div class="col">
            <h6 class="font-weight-normal mb-0">${event.title!''}</h6>
        </div>
    </div>

    <a class="btn btn-outline-white btn-icon w-fit-content mb-8 text-nowrap" href="${eventPageLink}" target="_blank">Learn More</a>
    [#if eventAllPage?has_content]
    <a class="d-block text-white-75 mt-auto" href="${cmsfn.link(eventAllPage)}">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="2" viewBox="0 0 24 2">
            <line x2="24" transform="translate(0 1)" fill="none" stroke="#fff" stroke-width="2" opacity="0.75"></line>
        </svg><span class="ml-2">All IR Events</span></a>
    [/#if]
</div>
[/#if]
