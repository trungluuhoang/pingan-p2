[#include "/pingan-templates/templates/functions/link.ftl"/]
[#assign pageUUID = content.page!]
[#assign page = cmsfn.contentById(pageUUID, "website")!]
[#assign bgImage = getAssetLink(cmsfn.asJCRNode(content), "bgImage")!""]
[#if bgImage?has_content && bgImage != ""]
    [#assign bgImage = "background-image:url(" + bgImage + ")"]
[/#if]
[#if page?has_content]
<div class="hover-elevation hover-text-decor-none tile tile--report text-white rounded d-flex flex-column" style="${bgImage}">
    <h3 class="h3-responsive text-capitalize mb-4 title">${content.title!}</h3>
    <a class="d-block text-white-75 mt-auto" href="${cmsfn.link(page)}">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="2" viewBox="0 0 24 2">
            <line x2="24" transform="translate(0 1)" fill="none" stroke="#fff" stroke-width="2" opacity="0.75"></line>
        </svg><span class="ml-2">Learn More</span></a>
</div>
[/#if]
