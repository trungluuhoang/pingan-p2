[#include "/pingan-templates/templates/functions/link.ftl"]
[#function getBackground node propName]
    [#assign bgStyle = ""]
    [#if node.hasNode(propName)!]
        [#assign curNode = node.getNode(propName)]
        [#if curNode.hasProperty("field")]
            [#assign field = curNode.getProperty("field").getString()!]
            [#if field == "color" && curNode.hasProperty("color")]
                [#assign color = curNode.getProperty("color").getString()!""]
                [#assign color = color?trim?replace('^#','','r')]
                [#if color?has_content]
                    [#assign bgStyle = "background:#" + color]
                [/#if]
            [/#if]
            [#if field == "image" && curNode.hasProperty("image")]
                [#assign uuid = curNode.getProperty("image").getString()!]
                [#if uuid?has_content && !uuid?starts_with("jcr:")]
                    [#assign uuid = "jcr:"+ uuid!]
                [/#if]
                [#assign imageUrl = damfn.getAssetLink(uuid)!]
                [#if imageUrl?has_content]
                    [#assign bgStyle = "background:url(" + imageUrl + ") no-repeat center / cover"]
                [/#if]
            [/#if]
        [/#if]
    [/#if]
    [#return bgStyle]
[/#function]

[#assign pageUUID = content.page!]
[#assign page = cmsfn.contentById(pageUUID, "website")!]

[#if page?has_content]
[#assign bgStyle = getBackground(cmsfn.asJCRNode(content), "background")!""]

<div class="hover-elevation hover-text-decor-none tile tile--orange-gradient bg-orange-gradient text-white rounded d-flex flex-column" style="${bgStyle}">
    <h3 class="h3-responsive text-capitalize mb-4">${content.title!}</h3>
    <h6 class="font-weight-normal mb-0">${content.description!}</h6>
    <a class="d-block text-white-75 mt-auto" href="${cmsfn.link(page)}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="2" viewBox="0 0 24 2">
            <line x2="24" transform="translate(0 1)" fill="none" stroke="#fff" stroke-width="2" opacity="0.75"></line>
        </svg><span class="ml-2">Learn More</span>
    </a>
</div>
[/#if]
