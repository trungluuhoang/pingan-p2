[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign reportPageUUID = content.reportsPage!]
[#assign reportPage = cmsfn.contentById(reportPageUUID, "website")!]

[#if reportPage?has_content]
<div class="hover-elevation hover-text-decor-none tile tile--reports text-white rounded">
    <div class="row row-m-2">
        <div class="col-sm-6 mb-6 mb-md-0">
            <h3 class="h3-responsive text-capitalize mb-4 title">${content.title!}</h3>
            <p class="mb-5">${content.description!}</p>
            <a class="btn btn-outline-white btn-icon" href="${cmsfn.link(reportPage)}">
                <svg class="img-icon" xmlns="http://www.w3.org/2000/svg" width="15.918" height="13" viewBox="0 0 15.918 13">
                    <g transform="translate(-264 -1380)">
                        <path d="M5.183,23.5a.15.15,0,0,0-.145.111L2,31.194v.265c0,.195-.012.265.15.265H14.024a.674.674,0,0,0,.65-.481l2.713-7.478V23.5Z" transform="translate(262.531 1361.276)" fill="#fff"></path>
                        <path d="M3.378,9.745H14.326V8.082a.726.726,0,0,0-.725-.725H7.035L5.708,5.5H.725A.726.726,0,0,0,0,6.225V17.314l2.728-7.088A.673.673,0,0,1,3.378,9.745Z" transform="translate(264 1374.5)" fill="#fff"></path>
                    </g>
                </svg>Library</a>
        </div>
        [#assign jcrQuery = "SELECT * from [nt:base] AS p WHERE ISDESCENDANTNODE('/') order by [date] desc"]
        [#assign latestLibraries = adsearchfn.search("pa-ir-library",jcrQuery!, "JCR-SQL2", "mgnl:paIrLibrary", 4, 0)!]

        <div class="col-sm-6">
            [#list latestLibraries as item]
                [#assign wrapForI18nNode = cmsfn.wrapForI18n(item)]
                [#assign itemMap = cmsfn.asContentMap(wrapForI18nNode)]

                [#assign itemDateStr = "\x3000"]
                [#if itemMap.releaseDate?has_content]
                    [#assign itemDateStr = itemMap.releaseDate?string("dd MMM yyyy")]
                [#elseif itemMap.date?has_content]
                    [#assign itemDateStr = itemMap.date?string("dd MMM yyyy")]
                [/#if]

                [#if itemMap.type == 'video']
                    <a class="d-flex mb-6 mb-md-8 text-white hover-text-primary item item--video glightbox" href="${getLink(item, "link")!}">
                        <svg class="flex-shrink-0 mr-5" xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60">
                            <g transform="translate(-562 -1348)">
                                <circle cx="30" cy="30" r="30" transform="translate(562 1348)" fill="#fff"></circle>
                                <path d="M11.483,0A11.483,11.483,0,1,0,22.966,11.483,11.483,11.483,0,0,0,11.483,0Zm3.969,12.092L9.71,15.68a.718.718,0,0,1-1.1-.609V7.895a.718.718,0,0,1,1.1-.609l5.741,3.588a.718.718,0,0,1,0,1.217Z" transform="translate(581 1367)" fill="#f05a23"></path>
                            </g>
                        </svg>
                        <div class="pt-2">
                            <div class="small mb-2">${itemDateStr}</div>
                            <h6 class="font-weight-normal mb-0">${itemMap.title!}</h6>
                        </div>
                    </a>

                [#else]
                    <a class="d-flex mb-6 mb-md-8 text-white hover-text-primary item item--pdf"
                       href="${getLink(item, "link")!}" target="_blank">
                        <svg class="flex-shrink-0 mr-5" xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60">
                            <g transform="translate(-562 -1257)">
                                <circle cx="30" cy="30" r="30" transform="translate(562 1257)" fill="#fff"></circle>
                                <g transform="translate(-696.47 44.942)">
                                    <path d="M275,15v3.617h3.617Z" transform="translate(1016.58 1216.833)" fill="#f05a23"></path>
                                    <g transform="translate(1280.47 1231.058)">
                                        <path d="M60,301.459H75.5V295H60Zm10.141-4.833h2.242v.67H70.9v.712h1.218v.619H70.9v1.3h-.763Zm-3.562,0h1.232a1.823,1.823,0,0,1,.716.13,1.441,1.441,0,0,1,.826.877,1.928,1.928,0,0,1,.1.639,1.9,1.9,0,0,1-.116.681,1.464,1.464,0,0,1-.333.521,1.492,1.492,0,0,1-.521.335,1.851,1.851,0,0,1-.677.119H66.579Zm-3.13,0h1.409a.968.968,0,0,1,.435.1,1.115,1.115,0,0,1,.339.256,1.222,1.222,0,0,1,.223.358,1.066,1.066,0,0,1,.081.4,1.158,1.158,0,0,1-.077.419,1.2,1.2,0,0,1-.214.358,1.008,1.008,0,0,1-.333.251.992.992,0,0,1-.433.093h-.67v1.065h-.763Z" transform="translate(-60 -279.756)" fill="#f05a23"></path>
                                        <path d="M142.367,340.221a.516.516,0,0,0,.1-.339.59.59,0,0,0-.03-.2.41.41,0,0,0-.081-.14.312.312,0,0,0-.116-.081.355.355,0,0,0-.13-.026h-.6v.9h.623A.3.3,0,0,0,142.367,340.221Z" transform="translate(-137.299 -321.895)" fill="#f05a23"></path>
                                        <path d="M69.56,5.942V0H60V13.693H75.5V5.942Z" transform="translate(-60)" fill="#f05a23"></path>
                                        <path d="M202.922,341.323a.752.752,0,0,0,.272-.207.942.942,0,0,0,.167-.312,1.266,1.266,0,0,0,.058-.393,1.282,1.282,0,0,0-.058-.4.893.893,0,0,0-.17-.309.739.739,0,0,0-.274-.2.925.925,0,0,0-.367-.07h-.47V341.4h.47A.888.888,0,0,0,202.922,341.323Z" transform="translate(-194.738 -321.895)" fill="#f05a23"></path>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        <div class="pt-2">
                            <div class="small mb-2">${itemDateStr}</div>
                            <h6 class="font-weight-normal mb-0">${itemMap.title!}</h6>
                        </div>
                    </a>
                [/#if]
            [/#list]
        </div>
    </div>
</div>
[/#if]
