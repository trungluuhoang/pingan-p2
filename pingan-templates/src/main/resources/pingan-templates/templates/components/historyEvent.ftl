[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign historyListUUID = content.historyList!]
[#assign historyListParent = cmsfn.contentById(historyListUUID, "pa-history")!]

<div class="container">
  <div class="details-page-content">
    <section>
      <div class="row row-m-4 row-sm-m-6 row-md-m-3 page-vtab-content">
        <div class="col-md-3 col-navs">
          <div data-scrollmagic-pin-spacer="" class="scrollmagic-pin-spacer">
            <div class="nav nav--tab" role="tablist">
              [#list cmsfn.children(historyListParent, "mgnl:folder") as group]
                <a class="nav-link h4-responsive nav-link [#if group?index==0]active[/#if]"
                  data-toggle="tab"
                  href="#history-${group.@name!}"
                  role="tab"
                  aria-selected="false">${group.jcrName!}</a>
              [/#list]
            </div>
          </div>
        </div>
        <div class="col-md-9 col-content">
          <div class="tab-content">
            [#list cmsfn.children(historyListParent, "mgnl:folder") as group]
              <div class="tab-pane [#if group?index==0]active[/#if]" role="tabpanel" id="history-${group.@name!}">
                [#list cmsfn.children(group, "mgnl:folder") as year]
                  <div class="position-relative year">
                    <div class="position-absolute year-title">${year.@name!}</div>
                    <div class="grid" style="position: relative; height: 878.578px;">
                      <div class="grid-sizer"></div>
                      <div class="gutter-sizer"></div>
                      [#list cmsfn.children(year, "mgnl:paHistory") as history]
                        [#assign wrapForI18n = cmsfn.wrapForI18n(history)]
                        [#assign url = getLink(cmsfn.asJCRNode(wrapForI18n), "url")!""]
                        [#if wrapForI18n.display?has_content && wrapForI18n.display]
                          <div class="grid-item" data-index="${history?index}">
                            <div class="top-spacer"></div>
                            
                            [#assign imgUrl = getAssetLink(cmsfn.asJCRNode(wrapForI18n), "image")!]
                            [#assign imageAlt=setImageAlt(wrapForI18n, imgUrl, "imageAlt")!]
                            <img class="rounded w-100 mb-5 mb-md-6" src="${imgUrl!}" alt="${imageAlt!}">
                            <h5 class="title mb-3">${wrapForI18n.title}</h5>
                            <div class="desc">${wrapForI18n.description}</div>
                          [#if url?has_content]
                              [#assign urlAttrs = ' href="' + (url?replace('[\'"]\\s*target=[\'"]_blank\\s*$','','r')?trim) + '" ']
                              [#assign glightbox = ' glightbox ']
                              [#if wrapForI18n.openNewTab?has_content && wrapForI18n.openNewTab]
                                [#assign urlAttrs = urlAttrs + ' target="_blank"']
                                [#assign glightbox = '']
                              [/#if]

                              [#if wrapForI18n.urlType?has_content && wrapForI18n.urlType == "video"]
                                  <a class="btn btn-outline-orange btn-icon ${glightbox}" ${urlAttrs}>
                                      <svg class="img-icon" xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16">
                                          <path d="M7.072,2.321a1,1,0,0,1,1.857,0l6.523,16.307A1,1,0,0,1,14.523,20H1.477a1,1,0,0,1-.928-1.371Z" transform="translate(20) rotate(90)"></path>
                                      </svg>
                                      ${wrapForI18n.urlText!}</a>
                              [#else]
                                  <a class="read-more" ${urlAttrs}>
                                      <svg class="img-icon" xmlns="http://www.w3.org/2000/svg" width="20" height="2" viewBox="0 0 20 2">
                                          <line x2="20" transform="translate(0 1)" fill="none" stroke="#F05A23" stroke-width="2"></line>
                                      </svg>
                                      <div class="d-inline text-grey ml-2">${wrapForI18n.urlText!}</div>
                                  </a>
                              [/#if]
                          [/#if]
                          </div>
                        [/#if]
                      [/#list]
                    </div>
                  </div>
                [/#list]
              </div>
            [/#list]
          </div>
        </div>
      </div>
    </section>
  </div>
</div>
