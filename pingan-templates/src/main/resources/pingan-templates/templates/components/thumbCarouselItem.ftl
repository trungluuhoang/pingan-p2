[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign item = content.item!]
[#assign itemType = item.field!]
[#assign imageLink = ""]
[#assign videoLink = ""]
[#assign title = ""]
[#assign subTitle = ""]
[#assign imageAlt = ""]

[#if itemType=="dam"]
    [#assign asset = damfn.getAsset("jcr:"+ item.asset)!]
    [#if asset.title?has_content]
        [#assign title = asset.title!]
    [/#if]
    [#if asset.subject?has_content]
        [#assign subTitle =asset.subject!]
    [/#if]
    [#assign videoLink = getAssetLink(cmsfn.asJCRNode(item), "asset")!]
    [#assign imageLink = getAssetLink(cmsfn.asJCRNode(item), "asset")!]
[#elseif itemType=="pa-profiles"]
    [#assign profile = cmsfn.contentById(item.profile, "pa-profiles")!]
    [#if profile?has_content && !cmsfn.asJCRNode(profile).hasProperty("mgnl:deleted")]
        [#assign imageLink = getAssetLink(cmsfn.asJCRNode(profile), "profileImage")!]
        [#assign title = profile.profileName!]
        [#assign subTitle= getProfileTitle(cmsfn.asJCRNode(profile), "title")!]
    [/#if]
[/#if]

[#if item.title?has_content]
    [#assign title = item.title!]
[/#if]

[#if item.subTitle?has_content]
    [#assign subTitle = item.subTitle!]
[/#if]
[#if item.image?has_content]
    [#assign imageLink = getAssetLink(cmsfn.asJCRNode(item), "image")!]
[/#if]

[#assign imageAlt = setImageAlt(item, imageLink, "imageAlt")!]
    


[#if cmsfn.isEditMode()]
    <div class="swiper-slide" style="width: 176px; margin-right: 40px;">
[#else]
    <div class="swiper-slide swiper-slide-active"" >
[/#if]

    <div class="tile">
        <div class="thumbnail mb-4 mb-md-3">
            [#if content.type?has_content && content.type == 'video']
                <a class="glightbox" href="${videoLink!}"><img class="w-100 rounded object-fit-cover" src="${imageLink!}" alt="${imageAlt!}"></a>
                <a class="btn btn-icon mt-auto glightbox" href="${videoLink!}">
                    <img class="play-icon" src="../.resources/pingan-templates/webresources/assets/images/icon-playvideo.png" alt="Ping An Group">
                </a>
                <div class="video-info-wrap" style="display:none">
                    <p>Download</p>
                    <a href="${content.downloadUrl!imageLink!''}" target="__blank">
                        <img class="down-video-icon" src="../.resources/pingan-templates/webresources/assets/images/icon-download-large.png" alt="Ping An Group">
                    </a>
                </div>
            [#elseif content.downloadUrl?has_content]
                <a href="${content.downloadUrl}" target="__blank"><img class="w-100 rounded object-fit-cover" src="${imageLink!}" alt="${imageAlt!}"></a>
                <a class="btn btn-icon mt-auto" href="${content.downloadUrl}" target="__blank">
                    <img class="play-icon" src="../.resources/pingan-templates/webresources/assets/images/icon-download.png" alt="Ping An Group">
                </a>
            [#else]
                <img class="w-100 rounded object-fit-cover" src="${imageLink!}" alt="${imageAlt!}">
            [/#if]
        </div>
        <h5 class="mb-2 name">${title!}</h5>
        <div class="desc">${subTitle!}</div>
        <div class="longdesc"></div>
    </div>
</div>
