[#list components as component ]
    <div class="tab-pane [#if component?index==0 || cmsfn.isEditMode()]show active[/#if]" role="tabpanel" id="${cmsfn.decode(component).title?replace(" ", "_")?replace("&", "_")?lower_case}">
        [@cms.component content=component /]
    </div>
[/#list]
