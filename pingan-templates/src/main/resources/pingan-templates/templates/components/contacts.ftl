[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign contactListUUID = content.contactList!]
[#assign contactListParent = cmsfn.contentById(contactListUUID, "pa-ir-contacts")!]

<section>
  <div class="row row-m-4 row-sm-m-6 row-md-m-3 page-vtab-content">
    <div class="col-md-3 col-navs">
      [#if content.tips?has_content]
        <aside class="remarksBox col-12 remarksBox-mobile">${cmsfn.decode(content).tips!""}</aside>
      [/#if]
      <div class="nav nav--tab" role="tablist">
        [#list cmsfn.children(contactListParent, "mgnl:folder") as group]
          <a class="nav-link h4-responsive nav-link ${(group_index == 0)?string('active', '')}" data-toggle="tab" href="#share_analyst_contacts_${group_index}" role="tab" aria-selected="true">${group.jcrName!""}</a>
        [/#list]
        [#if content.tips?has_content]
          <aside class="remarksBox col-12">${cmsfn.decode(content).tips!""}</aside>
        [/#if]
      </div>
    </div>
    <div class="col-md-9 col-content">
      <div class="tab-content">
        [#list cmsfn.children(contactListParent, "mgnl:folder") as group]
          <div class="ir-contacts tab-pane ${(group_index == 0)?string('show active', '')}" role="tabpanel" id="share_analyst_contacts_${group_index}">
            <table cellpadding="0" cellspacing="0">
              <thead>
                <tr>
                  <th style="width:23%">Company</th>
                  <th style="width:23%">Analyst</th>
                  <th style="width:54%">Email/Telephone</th>
                </tr>
              </thead>
              <tbody>
                [#list cmsfn.children(group, "mgnl:paIRContacts") as contacts]
                  [#assign wrapForI18n = cmsfn.wrapForI18n(contacts)]
                  <tr>
                    <td>${wrapForI18n.professional}</td>
                    <td>${wrapForI18n.name}</td>
                    <td>${wrapForI18n.email}</td>
                  </tr>
                [/#list]
              </tbody>
            </table>
          </div>
        [/#list]
      </div>
    </div>
  </div>
</section>
