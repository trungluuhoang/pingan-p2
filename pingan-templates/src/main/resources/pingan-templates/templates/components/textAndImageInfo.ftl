[#include "/pingan-templates/templates/functions/link.ftl"/]
[#assign desktopImage = getAssetLink(cmsfn.asJCRNode(content), "desktopImage")!""]
[#assign mobileImage = getAssetLink(cmsfn.asJCRNode(content), "mobileImage")!""]
[#assign desktopImageAlt=setImageAlt(content, desktopImage,"desktopImageAlt")!]
[#assign mobileImageAlt=setImageAlt(content,mobileImage, "mobileImageAlt")!]

[#assign anchor = ""]
[#if content.anchor?has_content]
	[#assign anchor = "<i class=\"anchor-elem-scroll-behavior\" id=\"" + content.anchor + "\"></i>"]
[/#if]

<section class="section--top">
    <h5 class="text-primary mb-4">${content.title!}${anchor}</h5>

    [#if content.displayType?has_content && content.displayType == 'textWithMoreInfo']
        <div class="row row-m-4 row-sm-m-6">
            <div class="col-sm mb-10 mb-sm-0">${cmsfn.decode(content).description!}</div>
            <div class="col-sm-auto mb-4 mb-sm-0">
                <div class="px-8 py-6 mb-8 px-sm-10 py-sm-8 mb-sm-0 bg-orange-gradient text-white rounded h-100 tile-more">
                    <h5 class="font-weight-normal text-capitalize mb-4">more about our share information</h5>
                    [@cms.area name="shareInfo"/]
                    <button class="d-block btn btn-outline-white rounded-pill w-fit-content btn-icon btn-modal-adr">
						<svg class="img-icon img-icon-sm" xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10">
							<g transform="translate(4.241 -8.875)">
								<line x2="8" transform="translate(-3.241 13.875)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
								<line y2="8" transform="translate(0.759 9.875)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
							</g>
						</svg><span class="title">ADR</span>
					</button>
					<div class="modal fade modal-popup modal-popup--more" tabindex="-1" role="dialog" aria-hidden="true" id="modal_popup_adr">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
									<svg width="16.828" height="16.828" viewBox="0 0 16.828 16.828">
										<g transform="translate(-1316.257 -229.257)">
											<line x2="19.799" transform="translate(1317.672 230.672) rotate(45)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
											<line y2="19.799" transform="translate(1331.671 230.672) rotate(45)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="2"></line>
										</g>
									</svg>
								</button>
							</div>
							<div class="modal-body">
								<h5 class="text-primary title h3-responsive">ADR</h5>
								<p class="mb-9">A Level I American Depositary Receipt program in respect of the Company's H shares was established on March 28, 2005. American Depositary Shares evidenced by American Depositary Receipts are traded in the over-the-counter markets in the United States. Each American Depositary Share represents 20 H shares. Since July 9, 2007, ADR:ORD has been changed from 1:20 to 1:2.</p>
								<p class="mb-0">NAME: PING AN</p>
								<p class="mb-0">SYMBOL: PNGAY</p>
								<p class="mb-0">CUSIP: 72341E304</p>
								<p class="mb-0">Depositary Bank: THE BANK OF NEW YORK MELLON CORPORATION</p>
								<p class="mb-0">Address: 101 Barclay Street, New York NY 10286, U.S.A.</p>
								<p class="mb-0">Toll Free Tel Number for domestic callers: 1-888-BNY-ADRs</p>
								<p class="mb-0">International Callers can call: 610-382-7836</p>
								<p class="mb-0">Website: http://adrbny.com</p>
							</div>
						</div>
					</div>
				</div>
                </div>
            </div>
        </div>


    [#elseif content.displayType?has_content && content.displayType == 'textWithImage']
        <div class="row row-m-4 row-sm-m-6">
            <div class="col-sm mb-10 mb-sm-0">${cmsfn.decode(content).description!}</div>
            <div class="col-sm-auto">
                <div class="position-relative">
                    <img class="d-desktop" src="${desktopImage!}" alt="${desktopImageAlt!}">
                    <img class="d-responsive" src="${mobileImage!}" alt="${mobileImageAlt!}">
                </div>
            </div>
        </div>
    [#else]
        <p class="mb-6">${cmsfn.decode(content).description!}</p>
    [/#if]
</section>
