[#-------------- RENDERING --------------]
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" xml:lang="${cmsfn.language()}" lang="${cmsfn.language()}"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" xml:lang="${cmsfn.language()}" lang="${cmsfn.language()}"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" xml:lang="${cmsfn.language()}" lang="${cmsfn.language()}"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" xml:lang="${cmsfn.language()}" lang="${cmsfn.language()}"><!--<![endif]-->
	<head>

		[@cms.page /]

		[@cms.area name="htmlHeader" contextAttributes={"pageDef":def} /]
		[@cms.area name="canonicalArea"/]

		[#-- Scripts to be rendered in the header. --]
		[@cms.area name="headerScripts"/]
	</head>
	<body class="index">
		[#-- Scripts to be rendered at the beginning of the body. --]
		[@cms.area name="bodyBeginScripts"/]

	<!--[if lt IE 7]>
		<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->

		    [@cms.area name="navigation"/]
			<div class="page-content">
				[@cms.area name="main"/]
			</div>
		    [@cms.area name="footer"/]

		[#-- Theme jsFiles --]
		[#assign site =	sitefn.site()!]
		[#assign theme = sitefn.theme(site)!]
		[#list theme.jsFiles as jsFile]
			<script src="${jsFile.link}"></script>
		[/#list]

		[#-- We're using the prototype's jsFiles to be rendered at the bottom of the page --]
		[#if def.jsFiles??]
			[#list def.jsFiles as jsFile]
				<script type="text/javascript" src="${jsFile.link}"></script>

			[/#list]
		[/#if]

		[#-- Scripts to be rendered at the end of the body. --]
		[@cms.area name="bodyEndScripts"/]
	</body>
</html>
