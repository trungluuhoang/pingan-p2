[#include "/pingan-templates/templates/pages/areas/breadcrumbs.ftl"/]

<div class="container">
    <div class="details-page-content">
        [@cms.area name="eventBanner"/]
        [@cms.area name="eventAbout"/]
        [@cms.area name="speakers"/]
        [@cms.area name="relatedDocs"/]
    </div>
</div>