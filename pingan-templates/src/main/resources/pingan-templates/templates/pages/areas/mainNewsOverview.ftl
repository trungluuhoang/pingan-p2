[#include "/pingan-templates/templates/pages/areas/breadcrumbs.ftl"/]
[#include "/pingan-templates/includes/ftls/functions.ftl"]

[#assign siteRoot = cmsfn.root(content, "mgnl:page")!content]
[#assign page = ctx.getParameter("page")!]

[#assign yearsParam = ctx.getParameter("years")!]
[#assign searchYearsParam = ctx.getParameterValues("year")![]]
[#assign searchYearsQuery = '']
[#assign newsDetailsTemplate = 'pingan-templates:pages/newsDetails']

[#assign newsCategoriesPath = '/news-categories']
[#assign categoriesParam = ctx.getParameter("categories")!]
[#assign searchCategoriesParam = ctx.getParameterValues("category")![]]
[#assign searchCategoriesQuery = '']

[#assign maxRecords= 10]
[#assign teasers = []]

[#if !searchYearsParam?has_content && cmsfn.page(content).newsYear?has_content]
    [#assign searchYearsParam = [cmsfn.page(content).newsYear]]
[/#if]
[#if yearsParam?has_content && yearsParam == 'All']
    [#assign searchYearsParam = []]
[/#if]
[#list searchYearsParam]
    [#items as year]
        [#assign searchYearsQuery += '([date] >= CAST("${year}-01-01T00:00:00.000Z" AS DATE) AND [date] <= CAST("${year}-12-31T23:59:59.999Z" AS DATE)) OR ']
    [/#items]

    [#assign searchYearsQuery = 'AND (${searchYearsQuery?keep_before_last(" OR ")})']
[/#list]

[#assign newsCategories = cmsfn.contentByPath(newsCategoriesPath,"category")!]
[#if newsCategories?has_content]
    [#assign newsCategoriesList = cmsfn.children(newsCategories,"mgnl:category")!]
    [#if categoriesParam?has_content && categoriesParam == 'All']
        [#assign searchCategoriesParam = []]
        [#list newsCategoriesList]
            [#items as category]
                [#assign searchCategoriesParam += [category.@uuid]]
            [/#items]
        [/#list]
    [/#if]

[/#if]
[#list searchCategoriesParam]
    [#items as category]
        [#assign replaced = category?string?replace('-','')!]
        [#assign searchCategoriesQuery += 'p.categoriesUUIDs LIKE "%${replaced}%" OR ']
    [/#items]

    [#assign searchCategoriesQuery = 'AND (${searchCategoriesQuery?keep_before_last(" OR ")})']
[/#list]

[#assign jcrQuery = "SELECT * from [nt:base] AS p WHERE [mgnl:template]='${newsDetailsTemplate}' AND ISDESCENDANTNODE('${siteRoot.@path!}') ${searchYearsQuery} ${searchCategoriesQuery} order by [date] desc"]

[#if page?has_content]
    [#assign page = page?number]
[#else]
    [#assign page = 1]
[/#if]
[#assign count = 0]
[#if jcrQuery?has_content]
    [#assign offset = (page-1)*maxRecords]
    [#assign relatedSearch = adsearchfn.search("website",jcrQuery!, "JCR-SQL2", "mgnl:page", maxRecords, offset)!]
    [#assign count = adsearchfn.count("website",jcrQuery!, "JCR-SQL2", "mgnl:page")!]
[/#if]

[#if (relatedSearch![])?size > 0]
    [#list relatedSearch as node]
        [#assign tease = cmsfn.asContentMap(node)!]
        [#if tease?has_content && tease != cmsfn.page(content)]
            [#assign tease = cmsfn.wrapForI18n(tease)]
            [#assign teasers += [tease]]
        [/#if]
    [/#list]
[/#if]

<section class="section--nav-tabs" style="">
    <div style="inset: auto; margin: 0px 0px 20px; display: block; position: relative; box-sizing: content-box; min-height: 58px; height: auto; padding-top: 0px; padding-bottom: 0px;" data-scrollmagic-pin-spacer="" class="scrollmagic-pin-spacer">
        <div class="bg-white mb-4 header-sticky-acc" style="position: relative; margin: auto; inset: auto; box-sizing: border-box; width: 100%;">
            <div class="container">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item mb-0 mr-2 d-flex align-items-center">
                        <a class="nav-link py-3 px-1 active" data-toggle="tab" role="tab" id="tab_filter" href="#tab_content_filter" aria-controls="tab_content_filter" aria-selected="true">
                            <svg class="mr-2 img-icon" xmlns="http://www.w3.org/2000/svg" width="16.253" height="16.316" viewBox="0 0 16.253 16.316">
                                <path d="M15.619.105.642,0h0A.638.638,0,0,0,.161,1.06l5.4,6.1v8.523a.638.638,0,0,0,1.109.429l3.808-4.188a.638.638,0,0,0,.166-.429V7.16l5.445-5.989A.638.638,0,0,0,15.619.105ZM9.532,6.485a.637.637,0,0,0-.166.429v4.33L6.833,14.03V6.914a.637.637,0,0,0-.16-.422L2.063,1.285l12.12.085Zm0,0" transform="translate(0)" fill="#646565"></path>
                            </svg>
                            Filter
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="tab-content container">
        <div class="tab-pane fade tab-pane--filter show active" role="tabpanel" id="tab_content_filter" aria-labelledby="tab_filter">
            <form class="form-filters" action="${cmsfn.link(cmsfn.page(content)!)!}" id="FilterForm">
                <div class="row row-filter mb-4 mb-sm-1">
                    <div class="col col-pills">
                        <div class="d-flex flex-wrap checkbox-pills">
                            <div class="mr-1 mb-2 checkbox-pill checkbox-pill--filter">
                                <input type="checkbox" value="All" id="filter_year_All" name="years">
                                <label class="badge badge-pill border py-2 px-4 mb-0 small font-weight-normal h6-responsive" for="filter_year_All">All</label>
                            </div>
                            [#assign nextYear = 2022]
                            [#assign yearToStart = 2016]
                            [#list nextYear..yearToStart as year]
                                <div class="mr-1 mb-2 checkbox-pill checkbox-pill--filter">
                                    <input type="checkbox" value="${year?c}" id="filter_undefined_${year?c}" name="year">
                                    <label class="badge badge-pill border py-2 px-4 mb-0 small font-weight-normal h6-responsive" for="filter_undefined_${year?c}">${year?c}</label>
                                </div>
                            [/#list]
                        </div>
                    </div>
                </div>
                <div class="row row-filter mb-4 mb-sm-1">
                    <div class="col col-pills">
                        <div class="d-flex flex-wrap checkbox-pills">
                            <div class="mr-1 mb-2 checkbox-pill checkbox-pill--filter">
                                <input type="checkbox" value="All" id="filter_category_All" name="categories">
                                <label class="badge badge-pill border py-2 px-4 mb-0 small font-weight-normal h6-responsive" for="filter_category_All">All</label>
                            </div>
                            [#if newsCategoriesList?has_content]
                                [#list newsCategoriesList as category]
                                    <div class="mr-1 mb-2 checkbox-pill checkbox-pill--filter">
                                        <input type="checkbox" value="${category.@uuid}" id="filter_undefined_${category.name}" name="category">
                                        <label class="badge badge-pill border py-2 px-4 mb-0 small font-weight-normal h6-responsive" for="filter_undefined_${category.name}">${category.name}</label>
                                    </div>
                                [/#list]
                            [/#if]
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<div class="container">
    <div class="details-page-content">
        <section>
            <div class="grey-table grey-table--responsive">
                <div class="container-fluid rounded grey-table-header d-desktop">
                    <div class="pt-3 pb-2">
                        <div class="row">
                            <div class="col-xl-2 col-md-3 col-4">Date</div>
                            <div class="col">Announcements</div>
                        </div>
                    </div>
                </div>
                [#if teasers?has_content && teasers?size > 0 ]

                    <div class="container-fluid grey-table-body">
                        [#list teasers as teaser]

                             <a class="row py-sm-4 pb-4 text-dark-grey" href="${cmsfn.link(teaser)}">
                                <div class="col-xl-2 col-md-3 col-sm-4 col-title">${getDate(teaser)}</div>
                                <div class="col">
                                    [#assign tags = tagfn.getTags(teaser)! /]
                                    [#if tags?has_content]
                                        <div class="mb-2 text-grey">
                                        [#list tags as tag ]
                                            ${tag} |
                                        [/#list]
                                        </div>
                                    [/#if]

                                    <h5 class="font-weight-normal row-hover-text-primary">${teaser.title!}</h5>
                                </div>
                             </a>

                        [/#list]
                    </div>


                    <div class="mt-5 mt-md-7 grey-table-footer">
                        <nav aria-label="pagination">
                            <ul class="pagination">
                                [#if count > 0 ]
                                    [#assign separate = "?"]
                                    [#assign pageURL = state.getOriginalBrowserURL()]
                                    [#if ctx.getParameters()?size > 0]
                                        [#assign separate = "&"]
                                        [#if pageURL?contains("page=")]
                                            [#assign separate = ""]
                                            [#assign pageURL = pageURL?replace("page="+page, "")]
                                        [/#if]
                                    [/#if]
                                    [#setting number_format="0.##"]
                                    [#assign pageNums = (count/maxRecords)?string['0;;roundingMode=up']?number]
                                    [#if page > 1]
                                        <li class="page-item"><a class="page-link" href="${pageURL}${separate}page=${page-1}" aria-label="Previous">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="7.94" height="13.053" viewBox="0 0 7.94 13.053">
                                                    <path d="M12061.466,311.514l-5.112,5.112,5.112,5.112" transform="translate(-12054.939 -310.1)" fill="none" stroke="#919493" stroke-linecap="round" stroke-width="2"></path>
                                                </svg><span class="sr-only">Previous</span></a></li>
                                    [/#if]
                                    [#list 1..pageNums as x]
                                        <li class="page-item [#if (x==page)]active[/#if]"><a class="page-link" href="${pageURL}${separate}page=${x}">${x}</a></li>
                                    [/#list]
                                    [#if page < pageNums]
                                        <li class="page-item"><a class="page-link" href="${pageURL}${separate}page=${page+1}" aria-label="Next">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="7.94" height="13.053" viewBox="0 0 7.94 13.053">
                                                    <path d="M12061.466,311.514l-5.112,5.112,5.112,5.112" transform="translate(12062.88 323.153) rotate(180)" fill="none" stroke="#919493" stroke-linecap="round" stroke-width="2"></path>
                                                </svg><span class="sr-only">Next</span></a></li>
                                    [/#if]
                                [/#if]
                            </ul>
                        </nav>
                    </div>
                [#else]
                    <div style="text-align: left;">
                        <br>
                        <span>${i18n["no.results"]}.</span>
                        <br><br>
                    </div>
                [/#if]
            </div>
        </section>
    </div>
</div>
