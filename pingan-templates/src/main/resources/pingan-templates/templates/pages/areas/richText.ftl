[#assign pageContent = cmsfn.page(content)!content]

[#if pageContent.showTitle?has_content && pageContent.showTitle]
    <div class="container">
        <div class="border-bottom border-primary page-title-container">
            <h1>${pageContent.title!""}</h1>
        </div>
        <h5 class="page-desc font-weight-normal mb-8 mb-sm-10">${cmsfn.decode(pageContent).description!""}</h5>
    </div>
[/#if]

<div class="container">${cmsfn.decode(content).htmlMain!""}</div>
