[#include "/pingan-templates/templates/functions/link.ftl"/]
[#-------------- ASSIGNMENTS --------------]
[#-- Page's model & definition, based on the rendering hierarchy and not the node hierarchy --]
[#assign site = sitefn.site()!]
[#assign rootPage = cmsfn.root(content, "mgnl:page")! ]
[#assign theme = sitefn.theme(site)!]
[#assign pageDef = ctx.pageDef!]
[#assign pageContent = cmsfn.page(content)!content]
[#assign pageNode = cmsfn.asJCRNode(pageContent)]
[#assign desktopImage = getAssetLink(pageNode, "desktopImage")!""]
[#assign ogImage = pageContent.thumbImage!""]
[#assign host = ""]
[#if ctx.request?has_content]
    [#assign host = ctx.request.getScheme() + "://" + ctx.request.getServerName() + ":" + ctx.request.getServerPort()]
    [#assign host = host?replace("[，,\\s]", "", "r")]
    [#-------------- remove default port --------------]
    [#if ctx.request.getScheme()?contains("https")]
        [#assign host = host?replace(":443$", "", "r")]
    [#else]
        [#assign host = host?replace(":80$", "", "r")]
    [/#if]
[/#if]

[#-- set og:image to 'newsDetails' and 'perspectivesDetails' --]
[#if pageContent['mgnl:template']?has_content]
	[#if pageContent['mgnl:template'] == 'pingan-templates:pages/newsDetails']
		[#assign ogImage = ctx.contextPath + "/.resources/pingan-templates/webresources/assets/images/logo-red-bg.png"]
		[#if desktopImage?has_content]
			[#assign ogImage = desktopImage]
		[/#if]
	[#elseif pageContent['mgnl:template'] == 'pingan-templates:pages/perspectivesDetails']
		[#assign ogImage = ctx.contextPath + "/.resources/pingan-templates/webresources/assets/images/logo-red-bg.png"]
		[#list cmsfn.children(pageNode, 'mgnl:area') as item1]
			[#if item1?has_content && item1.name == "main"]
				[#list cmsfn.children(item1, 'mgnl:component') as item2]
					[#if item2?has_content && item2.hasProperty("mgnl:template") && item2.getProperty("mgnl:template").getString() == "pingan-templates:components/banner"]
						[#assign comDesktopImage = getAssetLink(item2, "desktopImage")!""]
						[#assign comMobileImage = getAssetLink(item2, "mobileImage")!""]
						[#if comDesktopImage?has_content]
							[#assign ogImage = comDesktopImage]
						[#elseif comMobileImage?has_content]
							[#assign ogImage = comMobileImage]
						[/#if]
					[/#if]
				[/#list]
			[/#if]
		[/#list]
	[/#if]
[/#if]
[#-------------- RENDERING --------------]
[#assign description = cmsfn.decode(pageContent).description!""]
[#assign description = description?replace('<[^>]+>','','r')?trim]
<meta charset="UTF-8">
<meta name="description" content="${description}">
<meta name="keywords" content="${pageContent.keywords!""}">
<meta name="viewport" content="width=device-width,height=device-height,viewport-fit=cover,user-scalable=no,initial-scale=1,minimum-scale=1,maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta property="og:site_name" content="${pageContent.name!"PingAn"}">
<meta property="og:title" content="${pageContent.title!""}">
<meta property="og:description" content="${description}">
<meta property="og:type" content="website">

[#if ogImage?has_content && !ogImage?contains("http://") && !ogImage?contains("https://")]
	[#assign ogImage = host + ogImage]
[/#if]
<meta name="image" property="og:image" content="${ogImage}"/>
<title>${pageContent.title!""}</title>
<link rel="shortcut icon" href="${ctx.contextPath}/.resources/pingan-templates/webresources/assets/images/favicon.ico" type="image/x-icon">
[#list theme.cssFiles as cssFile]
	[#if cssFile.conditionalComment?has_content]<!--[if ${cssFile.conditionalComment}]>[/#if]
	<link rel="stylesheet" type="text/css" href="${cssFile.link}" media="${cssFile.media}" />
	[#if cssFile.conditionalComment?has_content]<![endif]-->[/#if]
[/#list]

[#if pageDef.cssFiles??]
	[#list pageDef.cssFiles as cssFile]
		<link rel="stylesheet" type="text/css" href="${cssFile.link}" media="${cssFile.media}" />
	[/#list]
[/#if]