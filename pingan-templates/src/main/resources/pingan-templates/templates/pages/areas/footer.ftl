[#include "/pingan-templates/templates/functions/link.ftl"/]

[#assign rootPage = cmsfn.root(content, "mgnl:page")! ]
[#assign footerNode = cmsfn.asJCRNode(rootPage).getNode("footer")]
[#assign privacyUrl = getLink(footerNode, "privacyUrl")!""]
[#assign termsUrl = getLink(footerNode, "termsUrl")!""]
[#assign footer = cmsfn.asContentMap(footerNode)]
<footer>
	<div class="container">
		<h2 class="title text-primary mb-5">${footer.subscriptionTitle!}</h2>
		<input id="contextPath" type="hidden" value="${ctx.contextPath}">
		<form id="subscribeForm" class="subscribe">
			<div class="input-container d-flex mb-4">
				<div class="form-group mb-0 flex-grow-1">
					<input class="form-control h5" type="email" id="email" name="email" placeholder="Your email address" maxlength="100">
				</div>
				<button class="btn btn-plain" type="submit">
					<svg xmlns="http://www.w3.org/2000/svg" width="19.351" height="16.688" viewBox="0 0 19.351 16.688">
						<path id="Path_273" data-name="Path 273" d="M18.747,6.992H3.8L9.389,1.4,7.99,0,0,7.99l7.99,7.99,1.4-1.4L3.8,8.989H18.747Z" transform="translate(18.997 16.335) rotate(180)" fill="#fff" stroke="#fff" stroke-width="0.5" opacity="0.75"></path>
					</svg>
				</button>
			</div>
			<div class="form-group mb-0">
				<div class="form-control--checkbox">
					<input type="checkbox" name="pics" value="1" id="pics">
					<label class="small" for="pics">${footer.prefixTerm!} <a href="${privacyUrl!}">${footer.privacyText!}</a> ${footer.connector!} <a href="${termsUrl!}">${footer.termsText!}</a></label>
				</div>
			</div>
		</form>
		<div class="form-subscribe-success" style="display: none;">
			<h5 class="font-weight-normal text-white mb-8 mb-sm-10">Thank you for subscribing.</h5>
		</div>
		<div class="row mb-8 navs">
			[#assign infoUUID = footer.navigationLink!]
			[#assign info = cmsfn.contentById(infoUUID, "pa-navigation")!]
			[#if info?has_content]
				[#list cmsfn.children(info, "mgnl:paNavigation") as item]
					[#assign wrapForI18n = cmsfn.wrapForI18n(item)]
					[#if wrapForI18n.display?has_content && wrapForI18n.display]
						[#assign urlItem = getLink(cmsfn.asJCRNode(wrapForI18n), "url")!""]
						<div class="col-md-3">
							<a class="d-block h5 text-white text-uppercase mb-4" href="${urlItem}">${wrapForI18n.title!}</a>
							<ul class="list-unstyled d-desktop">
								[#list cmsfn.children(item, "mgnl:paNavigation") as childItem]
									[#assign wrapForI18nChild = cmsfn.wrapForI18n(childItem)]
									[#if wrapForI18nChild.display?has_content && wrapForI18nChild.display]
										[#assign urlChildItem = getLink(cmsfn.asJCRNode(wrapForI18nChild), "url")!""]
										<li><a class="text-capitalize" href="${urlChildItem}">${wrapForI18nChild.title!}</a></li>
									[/#if]
								[/#list]
							</ul>
						</div>
					[/#if]
				[/#list]
			[/#if]
		</div>
		<hr>
		<div class="row py-4 row-m-1 small bottom">
			<div class="col-md-auto">
				<div class="row mb-3 mb-md-0 justify-content-center justify-content-md-start">
					<div class="col-auto"><a href="https://www.facebook.com/pingangroup/" target="_blank"><img alt="Ping An Group" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAATCAYAAACp65zuAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAACqADAAQAAAABAAAAEwAAAAB1QG88AAAAh0lEQVQoFWNgQAL///83AOL1QAwHSNIQJlBGAYjfw1VAGTCFLDAGkC4AYgEk/kMg+wESH8IEGnAAybQL6AqY0AWg/A/o4rgUoqtjYARZCRT9D8QGQAxzI8jEC0AMAg8ZGRkTGJDchYvZAFaOSxZJXAGsEEYAJZB9fQAmDqOJ9syoQliQUUYDAIE+ml9dc7G0AAAAAElFTkSuQmCC"></a></div>
					<div class="col-auto"><a href="https://twitter.com/pingan_group" target="_blank"><img alt="Ping An Group" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAPCAYAAAAGRPQsAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAE6ADAAQAAAABAAAADwAAAACmtwgfAAABAklEQVQ4EZWScRWCMBjEwef/YgMiGGERiEADbWAFG2gDaYANxATQQBvg76bzDQZT771j377dHWMsSUDf95nGGNAYWEPhCrfSM+awsF6KDN7hZi5MYjgF+TqowEzJBgpaeL1hlEpfhjlolwe3M6X6ODMxfp6/OFGffK12V02IWnpHuIcxfL5miUpn1cIHzLy35NSlN58r5bNI9SRQjZXt/P9Yp2lqAxdv7+H/DOu4uaCBn93p98f+GssBykGImyC7BNJ4o3PeYMSny9vE/YNVE4SMG8h38NvnlmNfMCdE51bDGOaDcOXwl/OSJg924DXsPdMcYcFg4AY6dBQNrLgCqqN4AvjzGtvAhIxLAAAAAElFTkSuQmCC"></a></div>
					<div class="col-auto"><a href="https://www.linkedin.com/company/ping-an/" target="_blank"><img alt="Ping An Group" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAARCAYAAAA7bUf6AAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAEaADAAQAAAABAAAAEQAAAACtW7E7AAAAv0lEQVQ4Ed2Qbw0CMQzFNxQgYQ44CWcBB5OAA4ICkAAOzgGHhFMAKGAoOH4lGyn71Cx8oslL/6zvra2b5zmCR0Z0LQY5gY+1aCwgJUW8q9geMkIHjhmdnfnjTs8EBzRXWXfy3m9kOvK9+utEvATb7G/4AezoTw7CCIqNPEitL4Xsr1Ve0rP0y2EtFmh6gqlqls+iVUTIgdFlzXUlFKwiw3t32Hi5hbbeKqJJEl90oVVEa5gP+0Wqkz+bpF6vKX8BdPB/gGG9DjwAAAAASUVORK5CYII="></a></div>
				</div>
			</div>
			<div class="col-md text-center text-md-right"><a class="mb-1 mb-md-0 policy" href="${privacyUrl!}">${footer.privacyText!}</a> <a class="mb-1 mb-md-0 policy" href="${termsUrl!}">${footer.termsText!}</a></div>
			<div class="col-md-auto text-center text-md-right">
				<copyright>${footer.copyRight!}</copyright>
			</div>
		</div>
	</div>
</footer>
<div class="browser-notification">
	<div class="note-cont">
		<p>This website uses cookies to help us provide you with better experience and allow us to improve our service. By continuing to browse the site, you understand and agree to our <a href="${privacyUrl!}" target="_blank">${footer.privacyText!}</a> and <a href="${termsUrl!}" target="_blank">${footer.termsText!}</a> .</p>
		<p class="ie-browser">This website is not supported by IE. Get the latest version of Firefox or Chrome for better browsing experience.</p>
	</div>
	<div class="btn-close" href="javascript:void(0);">Noted</div>
</div>
