[#include "/pingan-templates/templates/functions/link.ftl"/]
[#assign jumpUrl = getLink(cmsfn.asJCRNode(content), "jumpUrl")!""]

<div class="container">
	<div class="content">
		<h1 class="text-primary mb-6">${content.lookingTitle}</h1>
		<h5 class="font-weight-normal mb-6">${content.lookingDesc}</h5>
		[#if jumpUrl?has_content]
			<a class="text-grey" href="${jumpUrl}">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="2" viewBox="0 0 24 2">
					<line x2="24" transform="translate(0 1)" fill="none" stroke="#F05A23" stroke-width="2"></line>
				</svg>
				<span class="ml-2">${content.jumpText!""}</span>
			</a>
		[/#if]
	</div>
</div>
