[#include "/pingan-templates/templates/macros/navigation.ftl"/]

[#assign siteRoot = cmsfn.root(content, "mgnl:page")! /]
[#assign homeLink = ctx.contextPath /]
[#assign chunkSize = 3 /]

[#if !homeLink?has_content]
	[#assign homeLink = "/" /]
[/#if]
<header class="fixed-top" id="header">
	<nav class="navbar navbar-expand-xl">
		<a class="navbar-brand active" href="${homeLink}"><img alt="Ping An Group" src="${ctx.contextPath}/.resources/pingan-templates/webresources/assets/images/logo-en.png"></a>
		<button class="navbar-toggler navbar-toggler--search ml-auto" type="button" data-toggle="collapse" data-target="#nav_search" aria-controls="nav_search" aria-expanded="false" aria-label="Toggle navigation"><img alt="search" src="${ctx.contextPath}/.resources/pingan-templates/webresources/assets/images/responsive/search.png"></button>
		<button class="navbar-toggler navbar-toggler--main" type="button" data-toggle="collapse" data-target="#nav_main" aria-controls="nav_main" aria-expanded="false" aria-label="Toggle navigation"><img alt="Ping An Group" src="${ctx.contextPath}/.resources/pingan-templates/webresources/assets/images/menu.png"></button>
		<div class="collapse navbar-collapse" id="nav_search">
			<form class="form-search form-search--responsive" action="${ctx.contextPath}/search.html" method="get">
				<input class="form-control h4 font-weight-normal" name="queryStr" type="search" placeholder="Search" aria-label="Search" autocomplete="off" maxlength="100">
			</form>
		</div>
		<div class="collapse navbar-collapse" id="nav_main">

			[@navigation siteRoot /]

			<ul class="navbar-nav align-items-center navbar-nav--right">
				<li class="nav-item nav-item--langs border border-secondary">
					<ul class="navbar-nav navbar-nav--lang flex-row">
						<li class="nav-item">
							<div class="nav-link text-grey font-weight-normal text-nowrap">中文版</div>
						</li>
						<li class="nav-item"><a class="nav-link font-weight-bold" href="//www.pingan.cn/tc/index.shtml">繁</a></li>
						<li class="nav-item">
							<div class="nav-link px-0">|</div>
						</li>
						<li class="nav-item"><a class="nav-link font-weight-bold" href="//www.pingan.cn/">简</a></li>
					</ul>
				</li>
				<li class="nav-item dropdown dropdown--mega d-desktop"><a class="nav-link nav-link--search dropdown-toggle" id="search" href="javascript:void(0);" ondragstart="return false"><img alt="Ping An Group" src="${ctx.contextPath}/.resources/pingan-templates/webresources/assets/images/search.png">
						<div class="dropdown-menu dropdown-menu--mega" aria-labelledby="search">
							<div class="container d-flex">
								<form class="m-auto form-search form-search--desktop" action="${ctx.contextPath}/search.html" method="get">
									<input class="form-control h4 font-weight-normal" name="queryStr" type="search" placeholder="Search" aria-label="Search" autocomplete="off" maxlength="100">
								</form>
							</div>
						</div></a></li>
			</ul>
		</div>
	</nav>
</header>