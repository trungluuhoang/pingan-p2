[#include "/pingan-templates/templates/pages/areas/breadcrumbs.ftl"/]
[#include "/pingan-templates/includes/ftls/functions.ftl"]
[#include "/pingan-templates/templates/macros/events.ftl"/]

[#assign eventBannerTemplate = 'pingan-templates:components/event/eventBanner']
[#assign eventRelatedDocsTemplate = 'pingan-templates:components/event/relatedDocs']

[#assign tipsUrl = getLink(cmsfn.asJCRNode(content), "tipsUrl")!""]
[#assign siteRoot = cmsfn.root(content, "mgnl:page")!content]
[#assign eventPath = cmsfn.parent(pageContent).@path]
[#assign yearsParam = ctx.getParameter("years")!]
[#assign searchYearsParam = ctx.getParameterValues("year")![]]
[#assign searchYearsQuery = '']
[#if !searchYearsParam?has_content && cmsfn.page(content).eventYear?has_content]
    [#assign searchYearsParam = [cmsfn.page(content).eventYear]]
[/#if]
[#if yearsParam?has_content && yearsParam == 'All']
    [#assign searchYearsParam = []]
[/#if]

[#assign now = .now]
[#assign nextYear = 2022]
[#assign yearToStart = 2015]

<section class="section--nav-tabs" style="">
    <div style="inset: auto; margin: 0px 0px 20px; display: block; position: relative; box-sizing: content-box; min-height: 60px; height: auto; padding-top: 0px; padding-bottom: 0px;" data-scrollmagic-pin-spacer="" class="scrollmagic-pin-spacer"><div class="bg-white mb-4 header-sticky-acc" style="position: relative; margin: auto; inset: 0px auto auto 0px; box-sizing: border-box; width: 100%;">
            <div class="container">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item mb-0 mr-2 d-flex align-items-center"><a class="nav-link py-3 px-1 active" data-toggle="tab" role="tab" id="tab_filter" href="#tab_content_filter" aria-controls="tab_content_filter" aria-selected="true">
                            <svg class="mr-2 img-icon" xmlns="http://www.w3.org/2000/svg" width="16.253" height="16.316" viewBox="0 0 16.253 16.316">
                                <path d="M15.619.105.642,0h0A.638.638,0,0,0,.161,1.06l5.4,6.1v8.523a.638.638,0,0,0,1.109.429l3.808-4.188a.638.638,0,0,0,.166-.429V7.16l5.445-5.989A.638.638,0,0,0,15.619.105ZM9.532,6.485a.637.637,0,0,0-.166.429v4.33L6.833,14.03V6.914a.637.637,0,0,0-.16-.422L2.063,1.285l12.12.085Zm0,0" transform="translate(0)" fill="#646565"></path>
                            </svg>Filter</a></li>
                    <li class="nav-item mb-0 mr-2 d-flex align-items-center"><a class="nav-link py-3 px-1" style="display: none" data-toggle="tab" role="tab" id="tab_search" href="#tab_content_search" aria-controls="tab_content_search" aria-selected="false">
                            <svg class="mr-2 img-icon" xmlns="http://www.w3.org/2000/svg" width="14.935" height="14.935" viewBox="0 0 14.935 14.935">
                                <g transform="translate(-373 -186)">
                                    <line x2="3.905" y2="3.905" transform="translate(383.5 196.5)" fill="none" stroke="#696a6a" stroke-width="1.5"></line>
                                    <g transform="translate(373 186)" fill="none" stroke="#696a6a" stroke-width="1.5">
                                        <circle cx="6.5" cy="6.5" r="5.75" fill="none"></circle>
                                    </g>
                                </g>
                            </svg>Search</a></li>
                </ul>
            </div>
        </div></div>
    <div class="tab-content container">
        <input type="hidden" id="filter_parentPath" value="${pageContent.@path}">
        <div class="tab-pane fade tab-pane--filter active show" role="tabpanel" id="tab_content_filter" aria-labelledby="tab_filter">
            <form class="form-filters" action="${cmsfn.link(cmsfn.page(content)!)!}" id="FilterForm">
                <input type="hidden" id="filter_endpoint" value="https://run.mocky.io/v3/5dec4850-c482-4c78-9f97-1b2c6c727a58">
                <input type="hidden" id="filter_loadMoreLimit" value="3">
                <div class="row row-filter mb-4 mb-sm-1">
                    <div class="col col-pills">
                        <div class="d-flex flex-wrap checkbox-pills year">
                            <div class="mr-1 mb-2 checkbox-pill checkbox-pill--filter">
                                <input type="checkbox" value="All" id="filter_undefined_All" name="years" [#if (yearsParam?has_content && yearsParam == 'All')]checked[/#if]>
                                <label class="badge badge-pill border py-2 px-4 mb-0 small font-weight-normal h6-responsive" for="filter_undefined_All">All</label>
                            </div>
                            [#list nextYear..yearToStart as year]
                                <div class="mr-1 mb-2 checkbox-pill checkbox-pill--filter">
                                    <input type="checkbox" value="${year?c}" id="filter_undefined_${year?c}" name="year" [#if (searchYearsParam![])?seq_contains(year?c)]checked[/#if]>
                                    <label class="badge badge-pill border py-2 px-4 mb-0 small font-weight-normal h6-responsive" for="filter_undefined_${year?c}">${year?c}</label>
                                </div>
                            [/#list]
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="tab-pane fade tab-pane--search" role="tabpanel" id="tab_content_search" aria-labelledby="tab_search">
            <form class="form-search" action="${cmsfn.link(cmsfn.page(content)!)!}">
                [#--<input type="hidden" id="keyword" value="">--]
                <input class="form-control h5 font-weight-normal" type="search" placeholder="Enter Keyword(s)" aria-label="Search">
            </form>
        </div>
    </div>
</section>
<div class="container">
    <div class="details-page-content">
        <section class="section--upcoming">
            [@cms.area name="upcommingEvents"/]
        </section>
        <section class="section--past">
            <h5 class="text-primary mb-5">Results and Presentations</h5>
            [#if searchYearsParam?size > 0]
                [#list searchYearsParam as year]
                    [@yearEvents year?number eventBannerTemplate eventPath eventRelatedDocsTemplate/]
                [/#list]
            [#else]
                [#list nextYear..yearToStart as year]
                    [@yearEvents year eventBannerTemplate eventPath eventRelatedDocsTemplate/]
                [/#list]
            [/#if]
        </section>
    </div>
</div>
<div class="container">
    For financial documents before 2015, please visit our <a href="${tipsUrl}">${content.tipsText!}</a>.
</div>
