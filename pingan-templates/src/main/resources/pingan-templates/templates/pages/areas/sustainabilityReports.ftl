[#include "/pingan-templates/templates/pages/areas/breadcrumbs.ftl"/]
[#include "/pingan-templates/templates/macros/filters.ftl"/]

[#assign pageContent = cmsfn.page(content)!content]
[#assign catUUID = pageContent.categories!]
[#assign categories = cmsfn.contentById(catUUID, "category")!]

[#if categories?has_content]
    [#assign categoriesList = cmsfn.children(categories, "mgnl:category")!]
[/#if]
<section class="section--nav-tabs">
    <div class="bg-white mb-4 header-sticky-acc">
        <div class="container">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item mb-0 mr-2 d-flex align-items-center">
                    <a class="nav-link py-3 px-1 active" data-toggle="tab" role="tab" id="tab_filter" href="#tab_content_filter" aria-controls="tab_content_filter" aria-selected="true">
                        <svg class="mr-2 img-icon" xmlns="http://www.w3.org/2000/svg" width="16.253"
                                height="16.316" viewBox="0 0 16.253 16.316">
                            <path d="M15.619.105.642,0h0A.638.638,0,0,0,.161,1.06l5.4,6.1v8.523a.638.638,0,0,0,1.109.429l3.808-4.188a.638.638,0,0,0,.166-.429V7.16l5.445-5.989A.638.638,0,0,0,15.619.105ZM9.532,6.485a.637.637,0,0,0-.166.429v4.33L6.833,14.03V6.914a.637.637,0,0,0-.16-.422L2.063,1.285l12.12.085Zm0,0"
                                    transform="translate(0)" fill="#646565"></path>
                        </svg>
                        Filter
                    </a>
                </li>
                <li class="nav-item mb-0 mr-2 d-flex align-items-center">
                    <a class="nav-link py-3 px-1" data-toggle="tab" style="display: none" role="tab" id="tab_search" href="#tab_content_search" aria-controls="tab_content_search">
                        <svg class="mr-2 img-icon" xmlns="http://www.w3.org/2000/svg" width="14.935"
                                height="14.935" viewBox="0 0 14.935 14.935">
                            <g transform="translate(-373 -186)">
                                <line x2="3.905" y2="3.905" transform="translate(383.5 196.5)" fill="none"
                                        stroke="#696a6a" stroke-width="1.5"></line>
                                <g transform="translate(373 186)" fill="none" stroke="#696a6a"
                                    stroke-width="1.5">
                                    <circle cx="6.5" cy="6.5" r="5.75" fill="none"></circle>
                                </g>
                            </g>
                        </svg>
                        Search
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="tab-content container">
        <div class="tab-pane fade tab-pane--filter show active" role="tabpanel" id="tab_content_filter"
                aria-labelledby="tab_filter">
            <form class="form-filters">
                <input type="hidden" id="filter_endpoint" value="${ctx.contextPath}/.rest/pingan/v1/query/sustainabilityReports">
                <input type="hidden" id="filter_initialLimit" value="10">
                <input type="hidden" id="filter_loadMoreLimit" value="10">
                <input type="hidden" name="limit" id="filter_limit" value="10">
                <input type="hidden" name="offset" id="filter_offset" value="0">
                <input type="hidden" name="categories" id="filter_categories" value="all">

                [@yearFilter [2022, 2021, 2020, 2019, 2018] /]

                [@categoryFilter categoriesList/]
            </form>
        </div>
        <div class="tab-pane fade tab-pane--search" role="tabpanel" id="tab_content_search"
                aria-labelledby="tab_search">
            <form class="form-search">
                <input class="form-control h5 font-weight-normal" type="search" placeholder="Enter Keyword(s)"
                        aria-label="Search">
            </form>
        </div>
    </div>
</section>
<div class="container">
    <div class="details-page-content">
        <script type="text/template" id="template_ir_library">
            <a class="row py-sm-4 pb-4 text-dark-grey item item--{item.type} {item.glightbox}" href="{item.link}" target="{item.target}">
                <div class="col-xl-2 col-md-3 col-sm-4 col-title">{item.date}</div>
                <div class="col">
                    <div class="d-flex d-sm-block">
                        <div class="row-hover-text-primary icon icon-{item.type}">
                            <p>{item.title}</p>
                        </div>
                    </div>
                </div>
            </a>

        </script>
        <section>
            <div class="grey-table grey-table--responsive items">
                <div class="container-fluid rounded grey-table-header d-desktop">
                    <div class="pt-3 pb-2">
                        <div class="row">
                            <div class="col-xl-2 col-md-3 col-4">Date</div>
                            <div class="col">Document</div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid grey-table-body">
                    <div class="cards"></div>
                </div>
                <div class="mt-5 mt-md-7 grey-table-footer">
                    <nav class="pagination" aria-label="pagination"><a class="page-link" href="javascript:void(0);" aria-label="Previous" data-page="prev">
                            <svg xmlns="http://www.w3.org/2000/svg" width="7.94" height="13.053" viewBox="0 0 7.94 13.053">
                                <path d="M12061.466,311.514l-5.112,5.112,5.112,5.112" transform="translate(-12054.939 -310.1)" fill="none" stroke="#919493" stroke-linecap="round" stroke-width="2"></path>
                            </svg><span class="sr-only">Previous</span></a><a class="page-link" href="javascript:void(0);" data-page="1">1</a><a class="page-link" href="javascript:void(0);" data-page="2">2</a><a class="page-link" href="javascript:void(0);" data-page="3">3</a><a class="page-link" href="javascript:void(0);" aria-label="Next" data-page="next">
                            <svg xmlns="http://www.w3.org/2000/svg" width="7.94" height="13.053" viewBox="0 0 7.94 13.053">
                                <path d="M12061.466,311.514l-5.112,5.112,5.112,5.112" transform="translate(12062.88 323.153) rotate(180)" fill="none" stroke="#919493" stroke-linecap="round" stroke-width="2"></path>
                            </svg><span class="sr-only">Next</span></a></nav>
                </div>
            </div>
        </section>
    </div>
</div>
