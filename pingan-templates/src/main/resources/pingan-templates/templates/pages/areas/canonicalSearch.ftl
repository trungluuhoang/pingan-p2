[#assign canonical = state.originalURL!]
[#assign queryString = state.queryString!]
[#assign pageStr = ctx.getParameter('page')!]
[#if pageStr?has_content]
	[#assign pageStr = '&amp;page='+pageStr]
	[#assign canonical = canonical?replace(pageStr,'')!]	
	[#assign canonical = canonical?trim!]
[/#if]

<script type="text/javascript">
	function createLink(url) {
		let doc = document;
		let link = doc.createElement("link");
		link.setAttribute("rel", "canonical");
		link.setAttribute("href", url);

		let heads = doc.getElementsByTagName("head");
		if (heads.length)
			heads[0].appendChild(link);
		else
			doc.documentElement.appendChild(link);
	}

	let isHttps = 'https:' == document.location.protocol ? true : false;
	if (!isHttps) {
		createLink('${canonical?replace("https://","http://")}')
	} else {
		createLink('${canonical?replace("http://","https://")}')
	}

</script>