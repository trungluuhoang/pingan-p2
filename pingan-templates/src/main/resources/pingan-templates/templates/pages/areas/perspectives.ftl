[#include "/pingan-templates/templates/pages/areas/breadcrumbs.ftl"/]
[#include "/pingan-templates/templates/macros/filters.ftl"/]

[#assign pageContent = cmsfn.page(content)!content]
[#assign catUUID = pageContent.categories!]
[#assign categories = cmsfn.contentById(catUUID, "category")!]
[#if categories?has_content]
    [#assign categoriesList = cmsfn.children(categories, "mgnl:category")!]
[/#if]

<div class="container">
    <div class="details-page-content">
        <script type="text/template" id="template_cardImage">
            <div class="col-lg-4 col-md-6 col-tile">
                <a class="d-block h-100 tile tile--img" href="{item.link}">
                    <div class="position-relative img-wrapper rounded hover-elevation">
                        <img class="w-100 h-100 rounded object-fit-cover" src="{item.image}" alt="">
                        <div class="position-absolute-0000 p-2 px-md-6 py-md-8 d-flex flex-column content">
                            <div class="rounded bg-primary text-white w-fit-content py-1 px-2 mt-auto mb-0 mb-md-2">{item.category}</div>
                            <h5 class="mb-0 font-weight-normal text-white d-desktop" style="{item.imageTextColor}">{item.title}</h5>
                        </div>
                    </div>
                    <h5 class="mt-2 mb-0 text-dark-grey d-responsive" style="{item.imageTextColor}">{item.title}</h5>
                </a>
            </div>

        </script>
        <script type="text/template" id="template_cardOrange">
            <div class="col-lg-4 col-md-6 col-tile">
                <a class="position-relative d-block h-100 rounded hover-elevation tile tile--color bg-orange-gradient tile--color-orange-gradient"
                   href="{item.link}">
                    <div class="px-5 pt-5 pb-5 px-sm-7 pt-sm-8 pb-sm-6 d-flex flex-column h-100 content">
                        <h5 class="mb-0 font-weight-normal text-white mb-auto title h-160">{item.title}</h5>
                        <div class="text-white-75">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="2" viewBox="0 0 24 2">
                                <line x2="24" transform="translate(0 1)" fill="none" stroke="#fff" stroke-width="2"
                                      opacity="0.75"></line>
                            </svg>
                            <span class="ml-2">{item.learnMoreText}</span>
                        </div>
                    </div>
                </a>
            </div>

        </script>
        <script type="text/template" id="template_cardGrey">
            <div class="col-lg-4 col-md-6 col-tile">
                <a class="position-relative d-block h-100 rounded hover-elevation tile tile--color bg-secondary tile--color-secondary"
                   href="{item.link}">
                    <div class="px-5 pt-5 pb-5 px-sm-7 pt-sm-8 pb-sm-6 d-flex flex-column h-100 content">
                        <h5 class="mb-0 font-weight-normal text-white mb-auto title h-160">{item.title}</h5>
                        <div class="text-white-75">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="2" viewBox="0 0 24 2">
                                <line x2="24" transform="translate(0 1)" fill="none" stroke="#fff" stroke-width="2"
                                      opacity="0.75"></line>
                            </svg>
                            <span class="ml-2">{item.learnMoreText}</span>
                        </div>
                    </div>
                </a>
            </div>

        </script>
        <section>
            <form class="form-filters d-desktop" id="form_filter_desktop">
                <input type="hidden" id="filter_parentPath" value="${pageContent.@path}">
                <input type="hidden" id="filter_initialLimit" value="9">
                <input type="hidden" id="filter_loadMoreLimit" value="6">
                <input type="hidden" name="limit" id="filter_limit" value="9">
                <input type="hidden" name="offset" id="filter_offset" value="0">
                <input type="hidden" name="categories" id="filter_categories" value="all">
                <input type="hidden" id="filter_endpoint" value="${ctx.contextPath}/.rest/pingan/v1/query/perspectivesDetails">

                [@yearFilter [2022, 2021, 2020, 2019] /]

                [@categoryFilter categoriesList/]
            </form>
        </section>
        <section>
            <div class="container-p-8 container-lg-p-0 grid">
                <div class="row row-m-4 row-md-m-5 row-lg-m-9 cards"></div>
                <div class="d-flex">
                    <div class="m-auto">
                        <div class="btn-loadmore">
                            <button class="btn btn-sm btn-outline-grey rounded-pill border-border-grey btn-icon btn-arrow"
                                    type="button" aria-expanded="false">
                                <svg class="img-icon" xmlns="http://www.w3.org/2000/svg" width="13.116" height="7.867"
                                     viewBox="0 0 13.116 7.867">
                                    <path d="M12325.452,862.175l5.143,5.052,5.145-5.052"
                                          transform="translate(-12324.037 -860.761)" fill="none" stroke="#f05a23"
                                          stroke-linecap="round" stroke-width="2"></path>
                                </svg>
                                <span class="title--close">Load More</span><span class="title--open">Load More</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
