[#assign pageContent = cmsfn.page(content)!content]
[#assign canonical = ""]
[#if pageContent.canonical?has_content && pageContent.canonical?trim!='']
    [#assign canonical = pageContent.canonical]
[#else]
    [#assign queryString = state.queryString!]
    [#if queryString?has_content]
        [#assign queryString = '?'+queryString]
        [#assign canonical = state.originalURL?replace(queryString,'')!]
    [#else]
        [#assign canonical = state.originalURL!]
    [/#if]
[/#if]

<script type="text/javascript">
  function createLink(url) {
    let doc = document;
    let link = doc.createElement("link");
    link.setAttribute("rel", "canonical");
    link.setAttribute("href", url);

    let heads = doc.getElementsByTagName("head");
    if (heads.length)
      heads[0].appendChild(link);
    else
      doc.documentElement.appendChild(link);
  }

  [#if pageContent.canonical?has_content && pageContent.canonical?trim!='']
  createLink('${canonical}')
  [#else]
  let isHttps = 'https:' == document.location.protocol ? true : false;
  if (!isHttps) {
    createLink('${canonical?replace("https://","http://")}')
  } else {
    createLink('${canonical?replace("http://","https://")}')
  }
  [/#if]

</script>









