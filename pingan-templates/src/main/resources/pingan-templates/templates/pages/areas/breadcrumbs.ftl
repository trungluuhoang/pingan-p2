[#assign pageContent = cmsfn.page(content)!content]

<div class="container">
    <nav class="d-desktop nav-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb mb-0 p-0 mb-4 bg-white">
            [#list cmsfn.ancestors(content, "mgnl:page") as page]
                [#if page?has_content && page.@depth > 0 && page != pageContent]
                    <li class="breadcrumb-item"><a class="small text-uppercase text-grey" href="${cmsfn.link(page)!"|"}">${page.title!page.@name!}</a></li>
                [/#if]
            [/#list]

        </ol>
    </nav>
    <div class="border-bottom border-primary page-title-container">
        [#if pageContent.showTitle?has_content && pageContent.showTitle]
            <h3 class="page-title">${pageContent.title!}</h3>
        [/#if]
    </div>
    <h5 class="page-desc font-weight-normal mb-8 mb-sm-10">${cmsfn.decode(pageContent).description!}</h5>
</div>