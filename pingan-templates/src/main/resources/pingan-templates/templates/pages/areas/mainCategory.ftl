[#include "/pingan-templates/templates/pages/areas/breadcrumbs.ftl"/]

<div class="container">
    <div class="details-page-content">
    [#list components as component ]
        [@cms.component content=component /]
    [/#list]
    </div>
</div>