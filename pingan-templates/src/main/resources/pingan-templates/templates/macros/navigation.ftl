[#include "/pingan-templates/templates/functions/link.ftl"/]

[#macro navigationSub navItem, chunkSize]
    [#assign childItems = cmsfn.children(navItem, "mgnl:paNavigation")![]]
    <div class="col-xl-6">
        <div class="row">
            [#list childItems?chunk(chunkSize!3) as childGroup]
                <div class="col-xl col-navs">
                    <ul class="navs">
                        [#list childGroup as childItem]
                            [#assign wrapForI18n = cmsfn.wrapForI18n(childItem)]
                            [#if wrapForI18n.display?has_content && wrapForI18n.display]
                                [#assign urlChildItem = getLink(cmsfn.asJCRNode(wrapForI18n), "url")!""]
                                [#assign pageUri = urlChildItem?split("/")?last?split(".")?first]
                                [#assign active = pageUri?has_content?then(ctx.aggregationState.originalURI?contains("/" + pageUri)?then("active", ""),"")]

                                [#if wrapForI18n.isGroup?has_content && wrapForI18n.isGroup]
                                    <li class="nav-item no-arrow font-weight-medium"><a class="nav-link text-capitalize" href="javascript:void(0);">${wrapForI18n.title!}</a></li>
                                [#else]
                                    <li class="nav-item"><a class="nav-link text-capitalize ${active}" href="${urlChildItem}">${wrapForI18n.title!}</a></li>
                                [/#if]
                            [/#if]
                        [/#list]
                    </ul>
                </div>
            [/#list]
        </div>
    </div>
[/#macro]

[#macro navigation rootNav]
    <ul class="navbar-nav ml-auto align-items-center navbar-nav--left">
        [#assign infoUUID = rootNav.navigationLink!]
        [#assign info = cmsfn.contentById(infoUUID, "pa-navigation")!]
        [#if info?has_content]
            [#list cmsfn.children(info, "mgnl:paNavigation") as item]
                [#assign wrapForI18n = cmsfn.wrapForI18n(item)]
                [#assign chunkSize = wrapForI18n.numOfItem?has_content?then(wrapForI18n.numOfItem?number, 3)]
                [#if wrapForI18n.display?has_content && wrapForI18n.display]
                    [#assign urlItem = getLink(cmsfn.asJCRNode(wrapForI18n), "url")!""]
                    [#assign pageUri = urlItem?split("/")?last?split(".")?first]
                    [#assign active = pageUri?has_content?then(ctx.aggregationState.originalURI?contains("/" + pageUri)?then("active", ""), "")]
                    <li class="nav-item dropdown dropdown--mega">
                        <div class="nav-link dropdown-toggle d-flex align-items-center ${active}" id="${wrapForI18n.@name}" href="${urlItem}">
                            <a class="nav-link font-weight-medium active" href="${urlItem}">${wrapForI18n.title!}</a>
                            <button class="btn btn-plain d-responsive ml-auto p-2 btn-toggle">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13.116" height="7.867" viewBox="0 0 13.116 7.867">
                                    <path d="M12325.452,862.175l5.143,5.052,5.145-5.052" transform="translate(-12324.037 -860.761)" fill="none" stroke="#919493" stroke-linecap="round" stroke-width="2"></path>
                                </svg>
                            </button>
                        </div>
                        <div class="dropdown-menu dropdown-menu--mega">
                            <div class="container">
                                <h4 class="font-weight-normal text-primary text-uppercase">${wrapForI18n.title!}</h4>
                                <div class="row">
                                    <div class="col-xl-3 d-desktop">
                                        <aisde>${wrapForI18n.description!}</aisde>
                                    </div>
                                    [@navigationSub item chunkSize/]
                                </div>
                            </div>
                        </div>
                    </li>
                [/#if]
            [/#list]
        [/#if]
    </ul>
[/#macro]