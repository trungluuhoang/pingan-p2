[#include "/pingan-templates/templates/functions/link.ftl"/]

[#macro yearEvents year eventTemplate parentPath eventRelatedDocsTemplate]
    [#assign searchYearsQuery = '([date] >= CAST("${year?c}-01-01T00:00:00.000Z" AS DATE) AND [date] <= CAST("${year?c}-12-31T23:59:59.999Z" AS DATE))']
    [#if year == now?string.yyyy?number]
        [#assign currentDate = now?string['yyyy-MM-dd']]
        [#assign searchYearsQuery = '([date] >= CAST("${year?c}-01-01T00:00:00.000Z" AS DATE) AND [date] <= CAST("${currentDate}T23:59:59.999Z" AS DATE))']
    [/#if]
    [#assign jcrQuery = "SELECT * from [nt:base] AS p WHERE [mgnl:template]='${eventTemplate}' AND ISDESCENDANTNODE('${parentPath}') AND ${searchYearsQuery} order by [date] desc"]
    [#assign relatedSearch = adsearchfn.search("website",jcrQuery!, "JCR-SQL2", "mgnl:component")!]
    [#assign events = []]
    [#if (relatedSearch![])?size > 0]
        [#list relatedSearch as node]
            [#assign event = cmsfn.asContentMap(node)!]
            [#if event?has_content]
                [#assign event = cmsfn.wrapForI18n(event)]
                [#assign events += [event]]
            [/#if]
        [/#list]
    [/#if]
    [#if events?has_content && events?size > 0 ]
        <div class="event-year" id="year_${year?c}">
            <div class="header py-3 px-4 py-sm-4 px-sm-6 rounded text-grey mb-6 mb-sm-8 h5-responsive">${year?c}</div>
            <div class="event-dates">
                [#list events as event]
                    <div class="row row-m-3 row-sm-m-8 mb-8 event-date">
                        <div class="col col-content">
                            <a class="d-block h5 font-weight-normal mb-4 h3-responsive text-dark-grey">${event.title!''}</a>
                            <div class="items">
                                [#assign relatedDocsComponents = adsearchfn.search("website", "SELECT * from [nt:base] AS p WHERE [mgnl:template]='${eventRelatedDocsTemplate}' AND ISDESCENDANTNODE('${cmsfn.page(event).@path}') ", "JCR-SQL2", "mgnl:component")!]
                                [#if relatedDocsComponents?size > 0]
                                    [@renderRelatedDoc cmsfn.asContentMap(relatedDocsComponents[0]).relatedDocs!/]
                                [/#if]
                            </div>
                        </div>
                    </div>
                [/#list]
            </div>
        </div>
    [/#if]
[/#macro]

[#macro renderRelatedDoc relatedDocs]
    [#if relatedDocs?has_content]
        [#list cmsfn.children(relatedDocs) as item]
            [#if item?has_content]
                [#assign url = item.url!""]
                [#if item.field?has_content && item.field == "internal"]
                    [#assign url = getAssetLink(cmsfn.asJCRNode(item), "dam")!""]
                [/#if]
                [#if item.type?has_content && (item.type == "video" || item.type == "videoOutside")]
                    [#assign specStr = " glightbox"]
                    [#if item.type == "videoOutside"]
                        [#assign specStr = "\" target=\"_blank"]
                    [/#if]
                    <a class="d-flex mb-3 text-dark-grey item item--video ${specStr}" href="${url}">
                        <svg class="mr-3 flex-shrink-0 img-icon" xmlns="http://www.w3.org/2000/svg" width="22" height="22.966"
                             viewBox="0 0 22.966 22.966">
                            <path d="M11.483,0A11.483,11.483,0,1,0,22.966,11.483,11.483,11.483,0,0,0,11.483,0Zm3.969,12.092L9.71,15.68a.718.718,0,0,1-1.1-.609V7.895a.718.718,0,0,1,1.1-.609l5.741,3.588a.718.718,0,0,1,0,1.217Z"
                                  fill="#f05a23"></path>
                        </svg>
                        ${item.title!""}
                    </a>
                [#elseif item.type?has_content && item.type == "xls"]
                    <a class="d-flex mb-3 text-dark-grey item item--xls" href="${url}" target="_blank">
                        <svg class="mr-3 flex-shrink-0 img-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <g transform="translate(-557 -681)">
                                <rect width="24" height="24" transform="translate(557 681)" fill="#fff" opacity="0"></rect>
                                <g transform="translate(561 682)">
                                    <path d="M155,271h2.227v1.328H155Zm0,0" transform="translate(-148.362 -259.395)" fill="#f05a23"></path>
                                    <path d="M237,332h2.141v1.285H237Zm0,0" transform="translate(-226.851 -317.783)" fill="#f05a23"></path>
                                    <path d="M155,332h2.227v1.285H155Zm0,0" transform="translate(-148.362 -317.783)" fill="#f05a23"></path>
                                    <path d="M237,271h2.141v1.328H237Zm0,0" transform="translate(-226.851 -259.395)" fill="#f05a23"></path>
                                    <path d="M272,21.215V24.16h2.946Zm0,0" transform="translate(-260.352 -20.306)" fill="#f05a23"></path>
                                    <path d="M155,392h2.227v1.285H155Zm0,0" transform="translate(-148.362 -375.213)" fill="#f05a23"></path>
                                    <path d="M237,392h2.141v1.285H237Zm0,0" transform="translate(-226.851 -375.213)" fill="#f05a23"></path>
                                    <path d="M75,392h2.141v1.285H75Zm0,0" transform="translate(-71.788 -375.213)" fill="#f05a23"></path>
                                    <path d="M10.363,5.139V0H0V21.925H15.5V5.139Zm3.212,14.217H1.927V7.708H13.575Zm0,0" fill="#f05a23"></path>
                                    <path d="M75,210h9.078v1.328H75Zm0,0" transform="translate(-71.788 -201.007)" fill="#f05a23"></path>
                                    <path d="M75,332h2.141v1.285H75Zm0,0" transform="translate(-71.788 -317.783)" fill="#f05a23"></path>
                                    <path d="M75,271h2.141v1.328H75Zm0,0" transform="translate(-71.788 -259.395)" fill="#f05a23"></path>
                                </g>
                            </g>
                        </svg>
                        ${item.title!""}
                    </a>
                [#elseif item.type?has_content && item.type == "mp3"]
                    <a class="d-flex mb-3 text-dark-grey item item--mp3" href="${url}" target="_blank">
                        <svg class="mr-3 flex-shrink-0 img-icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <g transform="translate(-587 -681)">
                                <rect width="24" height="24" transform="translate(587 681)" fill="#fff" opacity="0"></rect>
                                <g transform="translate(588 631.026)">
                                    <g transform="translate(17.853 55.818)">
                                        <path d="M424.88,97.763l-.885.909a8.03,8.03,0,0,1,.016,11.51l.887.906a9.3,9.3,0,0,0-.018-13.325Z" transform="translate(-423.995 -97.763)" fill="#f05a23"></path>
                                    </g>
                                    <g transform="translate(15.965 57.495)">
                                        <path d="M380.006,137.589l-.857.935a5.5,5.5,0,0,1,.016,8.1l.861.931a6.768,6.768,0,0,0-.019-9.971Z" transform="translate(-379.149 -137.589)" fill="#f05a23"></path>
                                    </g>
                                    <g transform="translate(14.087 59.165)">
                                        <path d="M335.337,177.244l-.788.994a2.967,2.967,0,0,1,.016,4.649l.794.989a4.235,4.235,0,0,0-.022-6.631Z" transform="translate(-334.549 -177.244)" fill="#f05a23"></path>
                                    </g>
                                    <g transform="translate(0 58.886)">
                                        <rect width="3.972" height="7.128" fill="#f05a23"></rect>
                                    </g>
                                    <g transform="translate(5.24 53.974)">
                                        <path d="M131.691,53.974l-7.241,4.417v8.126l7.248,4.471Z" transform="translate(-124.45 -53.974)" fill="#f05a23"></path>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        ${item.title!""}
                    </a>
                [#else]
                    <a class="d-flex mb-3 text-dark-grey item item--pdf" href="${url}" target="_blank">
                        <svg class="mr-3 flex-shrink-0 img-icon" xmlns="http://www.w3.org/2000/svg" width="22" height="21.703"
                             viewBox="0 0 15.502 21.703">
                            <g transform="translate(-805 -1201.687)">
                                <g transform="translate(-475.47 -29.371)">
                                    <path d="M275,15v3.617h3.617Z" transform="translate(1016.58 1216.833)"
                                          fill="#f05a23"></path>
                                    <g transform="translate(1280.47 1231.058)">
                                        <path d="M60,301.459H75.5V295H60Zm10.141-4.833h2.242v.67H70.9v.712h1.218v.619H70.9v1.3h-.763Zm-3.562,0h1.232a1.823,1.823,0,0,1,.716.13,1.441,1.441,0,0,1,.826.877,1.928,1.928,0,0,1,.1.639,1.9,1.9,0,0,1-.116.681,1.464,1.464,0,0,1-.333.521,1.492,1.492,0,0,1-.521.335,1.851,1.851,0,0,1-.677.119H66.579Zm-3.13,0h1.409a.968.968,0,0,1,.435.1,1.115,1.115,0,0,1,.339.256,1.222,1.222,0,0,1,.223.358,1.066,1.066,0,0,1,.081.4,1.158,1.158,0,0,1-.077.419,1.2,1.2,0,0,1-.214.358,1.008,1.008,0,0,1-.333.251.992.992,0,0,1-.433.093h-.67v1.065h-.763Z"
                                              transform="translate(-60 -279.756)" fill="#f05a23"></path>
                                        <path d="M142.367,340.221a.516.516,0,0,0,.1-.339.59.59,0,0,0-.03-.2.41.41,0,0,0-.081-.14.312.312,0,0,0-.116-.081.355.355,0,0,0-.13-.026h-.6v.9h.623A.3.3,0,0,0,142.367,340.221Z"
                                              transform="translate(-137.299 -321.895)" fill="#f05a23"></path>
                                        <path d="M69.56,5.942V0H60V13.693H75.5V5.942Z" transform="translate(-60)"
                                              fill="#f05a23"></path>
                                        <path d="M202.922,341.323a.752.752,0,0,0,.272-.207.942.942,0,0,0,.167-.312,1.266,1.266,0,0,0,.058-.393,1.282,1.282,0,0,0-.058-.4.893.893,0,0,0-.17-.309.739.739,0,0,0-.274-.2.925.925,0,0,0-.367-.07h-.47V341.4h.47A.888.888,0,0,0,202.922,341.323Z"
                                              transform="translate(-194.738 -321.895)" fill="#f05a23"></path>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        ${item.title!""}
                    </a>
                [/#if]
            [/#if]
        [/#list]
    [/#if]
[/#macro]
