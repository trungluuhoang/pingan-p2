[#macro yearFilter yearsArr=[2022, 2021, 2020, 2019, 2018, 2017, 2016, 2015, 2014, 2013, 2012, 2011]]
<div class="row row-filter mb-4 mb-sm-1">
    <input type="hidden" name="year" id="filter_year" value="all">
    <div class="col col-pills">
        <div class="d-flex flex-wrap checkbox-pills year">
            <div class="mr-1 mb-2 checkbox-pill checkbox-pill--filter">
                <input class="year" type="checkbox" value="All" id="filter_year_0_All">
                <label class="badge badge-pill border py-2 px-4 mb-0 small font-weight-normal h6-responsive"
                       for="filter_year_0_All">All</label>
            </div>
            [#list yearsArr as year]
                <div class="mr-1 mb-2 checkbox-pill checkbox-pill--filter">
                    <input class="year" type="checkbox" value="${year?c}" id="filter_year_1_${year?c}">
                    <label class="badge badge-pill border py-2 px-4 mb-0 small font-weight-normal h6-responsive"
                           for="filter_year_1_${year?c}">${year?c}</label>
                </div>
            [/#list]
        </div>
    </div>
</div>
[/#macro]


[#macro categoryFilter categoriesList]
    <div class="row row-filter mb-4 mb-sm-1">
        <div class="col col-pills">
            <div class="d-flex flex-wrap checkbox-pills categories">
                <div class="mr-1 mb-2 checkbox-pill checkbox-pill--filter">
                    <input class="categories" type="checkbox" value="All" id="filter_categories_0_All">
                    <label class="badge badge-pill border py-2 px-4 mb-0 small font-weight-normal h6-responsive"
                           for="filter_categories_0_All">All</label>
                </div>
                [#if categoriesList?has_content]
                    [#list categoriesList as category]
                        <div class="mr-1 mb-2 checkbox-pill checkbox-pill--filter">
                            <input class="categories" type="checkbox" value="${category.@uuid}"
                                   id="filter_categories_1_${category.name}">
                            <label class="badge badge-pill border py-2 px-4 mb-0 small font-weight-normal h6-responsive"
                                   for="filter_categories_1_${category.name}">${category.name}</label>
                        </div>
                    [/#list]
                [/#if]
            </div>
        </div>
    </div>
[/#macro]
