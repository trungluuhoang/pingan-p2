[#function getDate content={} customFormat='']
    [#assign date = '']
    [#assign format = 'dd MMM yyyy']

    [#if customFormat?has_content]
        [#assign format = customFormat]
    [/#if]

    [#if content.date?has_content]
        [#assign date = content.date?string[format]]
    [#elseif content['mgnl:created']?has_content]
        [#assign date = content['mgnl:created']?string[format]]
    [#elseif content['jcr:created']?has_content]
        [#assign date = content['jcr:created']?string[format]]
    [/#if]

    [#return date]
[/#function]