/**
 * This file Copyright (c) 2020 Magnolia International
 * Ltd. (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package com.pingan.brands.fields;

import com.vaadin.data.BinderValidationStatus;
import com.vaadin.data.BindingValidationStatus;
import com.vaadin.ui.Component;
import com.vaadin.ui.HasComponents;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.jcr.wrapper.DelegateNodeWrapper;
import info.magnolia.ui.CloseHandler;
import info.magnolia.ui.ValueContext;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.contentapp.Datasource;
import info.magnolia.ui.contentapp.action.CloseAction;
import info.magnolia.ui.datasource.jcr.JcrNodeWrapper;
import info.magnolia.ui.editor.EditorView;
import info.magnolia.ui.observation.DatasourceObservation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.jcr.RepositoryException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Custom commit action for the categories multi link field.
 */
public class CategoriesCommitAction<T> extends CloseAction<CategoriesCommitActionDefinition> {

    private static final Logger log = LoggerFactory.getLogger(CategoriesCommitAction.class);

    private String categoriesFieldName = "categories";
    private String categoriesChildFieldName = "field";
    private String categoriesPropertyName = "categoriesUUIDs";

    private final ValueContext<T> valueContext;
    private final EditorView<T> form;
    private final Datasource<T> datasource;
    private final DatasourceObservation.Manual datasourceObservation;

    @Inject
    public CategoriesCommitAction(CategoriesCommitActionDefinition definition, CloseHandler closeHandler, ValueContext<T> valueContext, EditorView<T> form, Datasource<T> datasource, DatasourceObservation.Manual datasourceObservation) {
        super(definition, closeHandler);
        this.valueContext = valueContext;
        this.form = form;
        this.datasource = datasource;
        this.datasourceObservation = datasourceObservation;
    }

    public void execute() throws ActionExecutionException {
        List<BinderValidationStatus<?>> validationStatuses = this.form.validate();
        boolean hasValidationErrors = validationStatuses.stream().anyMatch(BinderValidationStatus::hasErrors);
        if (!hasValidationErrors) {
            this.write();
            super.execute();
        } else {
            List<BindingValidationStatus<?>> bindingValidationStatuses = (List)validationStatuses.stream().map(BinderValidationStatus::getFieldValidationErrors).flatMap(Collection::stream).collect(Collectors.toList());
            Iterator var4 = bindingValidationStatuses.iterator();

            while(var4.hasNext()) {
                BindingValidationStatus<?> validationStatus = (BindingValidationStatus)var4.next();
                Component field = (Component)validationStatus.getField();

                HasComponents parent;
                for(parent = field.getParent(); parent != null && !(parent instanceof Component.Focusable); parent = parent.getParent()) {
                }

                if (parent != null) {
                    ((Component.Focusable)parent).focus();
                    break;
                }
            }

        }
    }

    protected void write() {
        this.valueContext.getSingle().ifPresent((item) -> {
            this.form.write(item);
            if (item instanceof JcrNodeWrapper || item instanceof DelegateNodeWrapper) {
                JcrNodeWrapper jcrNodeWrapper = item instanceof JcrNodeWrapper ? (JcrNodeWrapper) item : (JcrNodeWrapper) ((DelegateNodeWrapper) item).getWrappedNode();
                try {
                    if(jcrNodeWrapper.hasNode(categoriesFieldName)){
                        String uuidCategories = NodeUtil.getCollectionFromNodeIterator(jcrNodeWrapper.getNode(categoriesFieldName).getNodes()).stream().filter(n -> {
                            try {
                                return n.hasProperty(categoriesChildFieldName);
                            } catch (RepositoryException e) {
                                log.error("does not have the category property: {}", e.getMessage());
                            }
                            return false;
                        }).map(n -> PropertyUtil.getString(n, categoriesChildFieldName).replaceAll("-", "")).collect(Collectors.joining( "," ));
                        jcrNodeWrapper.setProperty(categoriesPropertyName, uuidCategories);
                    }
                } catch (RepositoryException e) {
                    log.error("can not set the categories property {}", e.getMessage());
                }
            }
            this.datasource.commit(item);
            this.datasourceObservation.trigger();
        });
    }

    protected ValueContext<T> getValueContext() {
        return this.valueContext;
    }

    protected EditorView<T> getForm() {
        return this.form;
    }

    protected Datasource<T> getDatasource() {
        return this.datasource;
    }

    protected DatasourceObservation.Manual getDatasourceObservation() {
        return this.datasourceObservation;
    }

    public String getCategoriesFieldName() {
        return categoriesFieldName;
    }

    public void setCategoriesFieldName(String categoriesFieldName) {
        this.categoriesFieldName = categoriesFieldName;
    }

    public String getCategoriesChildFieldName() {
        return categoriesChildFieldName;
    }

    public void setCategoriesChildFieldName(String categoriesChildFieldName) {
        this.categoriesChildFieldName = categoriesChildFieldName;
    }

    public String getCategoriesPropertyName() {
        return categoriesPropertyName;
    }

    public void setCategoriesPropertyName(String categoriesPropertyName) {
        this.categoriesPropertyName = categoriesPropertyName;
    }
}
