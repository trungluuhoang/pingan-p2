/**
 * This file Copyright (c) 2020 Magnolia International
 * Ltd. (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package com.pingan.brands.fields;

import info.magnolia.ui.api.action.ActionType;
import info.magnolia.ui.contentapp.action.CloseActionDefinition;

/**
 * Custom commit action for the categories multi link field.
 */
@ActionType("categoriesCommitAction")
public class CategoriesCommitActionDefinition extends CloseActionDefinition {

    public CategoriesCommitActionDefinition() {
        this.setImplementationClass(CategoriesCommitAction.class);
        this.setName("categoriesCommit");
    }

    public int getShortcut() {
        return 23;
    }
}

