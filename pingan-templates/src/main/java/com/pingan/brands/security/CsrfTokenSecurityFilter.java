/**
 * This file Copyright (c) 2018 Magnolia International
 * Ltd.  (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This file is dual-licensed under both the Magnolia
 * Network Agreement and the GNU General Public License.
 * You may elect to use one or the other of these licenses.
 *
 * This file is distributed in the hope that it will be
 * useful, but AS-IS and WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE, or NONINFRINGEMENT.
 * Redistribution, except as permitted by whichever of the GPL
 * or MNA you select, is prohibited.
 *
 * 1. For the GPL license (GPL), you can redistribute and/or
 * modify this file under the terms of the GNU General
 * Public License, Version 3, as published by the Free Software
 * Foundation.  You should have received a copy of the GNU
 * General Public License, Version 3 along with this program;
 * if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * 2. For the Magnolia Network Agreement (MNA), this file
 * and the accompanying materials are made available under the
 * terms of the MNA which accompanies this distribution, and
 * is available at http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package com.pingan.brands.security;

import info.magnolia.audit.AuditLoggingUtil;
import info.magnolia.cms.filters.AbstractMgnlFilter;
import info.magnolia.cms.security.CsrfSecurityFilter;
import info.magnolia.cms.security.SecurityUtil;
import info.magnolia.context.Context;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;
import java.util.Optional;
import java.util.Random;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Filter that handles setup and validation of tokens to prevent CSRF attacks.
 *
 * This provides additional layer of security in addition to Referrer-checking {@link CsrfSecurityFilter}.
 *
 * <p>This filter passes if:</p>
 * <ul>
 * <li>the method is not POST</li>
 * <li>when user is NOT logged in, CSRF token passed as request parameter matches the value of CSRF token temporarily stored in a cookie.</li>
 * <li>when user is logged in, CSRF token passed as request parameter matches the value of CSRF token stored in session.</li>
 * </ul>
 *
 * <p>To provide flexibility, check is performed with voter in the filters bypasses node.
 * The default bypass configured is:</p>
 * <ul>
 * <li>Bypass any request url that starts with '/.'.</li>
 * </ul>
 *
 * <p>To add more bypasses (i.e. to 'white-list' specific referrer domains or uris) use for example:</p>
 * <ul>
 * <li>{@link info.magnolia.voting.voters.RequestHeaderPatternSimpleVoter} or</li>
 * <li>{@link info.magnolia.voting.voters.RequestHeaderPatternRegexVoter}.</li>
 * </ul>
 *
 * @see <a href="https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)_Prevention_Cheat_Sheet">Cross-Site
 *      Request Forgery (CSRF) Prevention Cheat Sheet</a>
 */
public class CsrfTokenSecurityFilter extends AbstractMgnlFilter {

    private static final Logger log = LoggerFactory.getLogger(CsrfTokenSecurityFilter.class);

    static final String CSRF_ATTRIBUTE_NAME = "csrf";
    private static final String EVENT_TYPE = "Possible CSRF Attack";

    private Random random = new SecureRandom();

    private final Provider<Context> contextProvider;

    @Inject
    public CsrfTokenSecurityFilter(final Provider<Context> contextProvider) {
        this.contextProvider = contextProvider;
    }

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (csrfCheckPasses(request, response)) {
            chain.doFilter(request, response);
        }
    }

    /**
     * @return true if csrf checks for this request pass, false otherwise.
     */
    protected boolean csrfCheckPasses(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(false);
        return unloggedRequestCheckPasses(request, response, session) || loggedInRequestCheckPasses(request, response, session);
    }

    /**
     * Handles a request issued by a logged in user. Session has been created, token is stored in it and matched against
     * the token coming as a request parameter.
     *
     * @return true if csrf checks pass, false otherwise.
     */
    private boolean loggedInRequestCheckPasses(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException {
        if (session != null) {
            // user is logged in, check token
            if (request.getMethod().equals("POST") && request.getRequestURI().contains("/en/.rest/pingan/v1/")){
                return true;
            }
            if (request.getMethod().equals("POST") && !session.isNew()) {
                String token = request.getParameter(CSRF_ATTRIBUTE_NAME);
                if (StringUtils.isBlank(token)) {
                    csrfTokenMissing(request, response, request.getServletPath());
                    return false;
                }
                if (!token.equals(session.getAttribute(CSRF_ATTRIBUTE_NAME))) {
                    csrfTokenMismatch(request, response, request.getServletPath());
                    return false;
                }
                // session is new, user has just logged in, put token in session
            } else if (session.getAttribute(CSRF_ATTRIBUTE_NAME) == null) {
                session.setAttribute(CSRF_ATTRIBUTE_NAME, generateSafeToken());
            }
            return true;
        }
        return false;
    }

    /**
     * Handles a request issued at login page. Session has not been created yet, token is temporarily kept in a cookie on the client.
     * Cookies act here as a sort of pre-session.
     * <p>Token is generated anew at each GET request. Upon POSTing, when the login form is actually submitted to the server, cookie token
     * is matched against the value coming from request. To enhance security, a salted-hash is used for the cookie, so that an attacker won't
     * be able to recreate the cookie value from the plain token without knowledge of the server secrets.
     * <p>This method is using a stateless technique, the so-called "Double Submit Cookie". See more at
     * https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.html#double-submit-cookie
     *
     * @return true if csrf checks pass, false otherwise.
     */
    private boolean unloggedRequestCheckPasses(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws IOException {
        if (session == null) {
            // login page is requested, set cookie and token
            if (request.getMethod().equals("GET")) {
                String token = generateSafeToken();
                request.setAttribute(CSRF_ATTRIBUTE_NAME, token);

                Cookie cookie = new Cookie(CSRF_ATTRIBUTE_NAME, SecurityUtil.getBCrypt(token));
                cookie.setPath(request.getServletPath());
                // A negative value means that the cookie is not stored persistently
                cookie.setMaxAge(-1);
                response.addCookie(cookie);

                // login attempt, check token and cookie
            } else if (request.getMethod().equals("POST")) {
                String token = request.getParameter(CSRF_ATTRIBUTE_NAME);
                Optional<Cookie> cookie = Arrays.stream(request.getCookies())
                        .filter(c -> c.getName().equals(CSRF_ATTRIBUTE_NAME))
                        .findFirst();

                if (StringUtils.isNotBlank(token) && cookie.isPresent()) {
                    if (!SecurityUtil.matchBCrypted(token, cookie.get().getValue())) {
                        csrfTokenMismatch(request, response, request.getServletPath());
                        return false;
                    }
                } else {
                    csrfTokenMissing(request, response, request.getServletPath());
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private void csrfTokenMissing(HttpServletRequest request, HttpServletResponse response, String url) throws IOException {
        final String auditDetails = String.format("CSRF token not set while user '%s' attempted to access url '%s'.", contextProvider.get().getUser().getName(), url);
        handleError(request, response, auditDetails);
    }

    private void csrfTokenMismatch(HttpServletRequest request, HttpServletResponse response, String url) throws IOException {
        final String auditDetails = String.format("CSRF token mismatched while user '%s' attempted to access url '%s'.", contextProvider.get().getUser().getName(), url);
        handleError(request, response, auditDetails);
    }

    /**
     * Actions to take when a CSRF attack is detected.
     * Log a message and send {@link HttpServletResponse#SC_FORBIDDEN} error response.
     */
    protected void handleError(HttpServletRequest request, HttpServletResponse response, String message) throws IOException {
        auditLogging(request, response, message);
        response.sendError(HttpServletResponse.SC_FORBIDDEN, "CSRF token mismatch possibly caused by expired session. Please re-open the page and submit the form again.");
    }

    private void auditLogging(HttpServletRequest request, HttpServletResponse response, String auditDetails) throws IOException {
        log.warn("{}. {}", new Object[]{EVENT_TYPE, auditDetails});
        AuditLoggingUtil.logSecurity(request.getRemoteAddr(), EVENT_TYPE, auditDetails);
    }

    private String generateSafeToken() {
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);
        Base64.Encoder encoder = Base64.getUrlEncoder().withoutPadding();
        String token = encoder.encodeToString(bytes);
        return token;
    }

}
