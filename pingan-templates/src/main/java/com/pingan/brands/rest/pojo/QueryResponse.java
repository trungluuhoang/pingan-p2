package com.pingan.brands.rest.pojo;

import java.util.List;

public class QueryResponse {

    private List<Result> data;

    private Integer totalRecord;

    public List<Result> getData() {
        return data;
    }

    public void setData(List<Result> data) {
        this.data = data;
    }

    public Integer getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(Integer totalRecord) {
        this.totalRecord = totalRecord;
    }
}

