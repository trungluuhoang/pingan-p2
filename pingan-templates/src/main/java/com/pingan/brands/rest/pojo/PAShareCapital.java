package com.pingan.brands.rest.pojo;

import java.util.List;

public class PAShareCapital implements Result {

    private Integer year;

    private List<PAShareCapitalItem> items;

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public List<PAShareCapitalItem> getItems() {
        return items;
    }

    public void setItems(List<PAShareCapitalItem> items) {
        this.items = items;
    }

    public PAShareCapital(Integer year, List<PAShareCapitalItem> items) {
        this.year = year;
        this.items = items;
    }
}
