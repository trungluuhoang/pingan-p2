package com.pingan.brands.rest.pojo;

public class PAPerspectivesDetail implements Result {

    private String id;

    private String image;

    private String color;

    private String category;

    private String title;

    private String link;

    private String target;

    private String learnMoreText;

    private String imageTextColor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getLearnMoreText() {
        return learnMoreText;
    }

    public void setLearnMoreText(String learnMoreText) {
        this.learnMoreText = learnMoreText;
    }

    public String getImageTextColor() {
        return imageTextColor;
    }

    public void setImageTextColor(String imageTextColor) {
        this.imageTextColor = imageTextColor;
    }
}
