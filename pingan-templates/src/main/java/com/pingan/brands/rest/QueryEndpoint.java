package com.pingan.brands.rest;


import static java.util.stream.Collectors.toList;

import info.magnolia.context.MgnlContext;
import info.magnolia.dam.api.Asset;
import info.magnolia.dam.templating.functions.DamTemplatingFunctions;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.jcr.util.SessionUtil;
import info.magnolia.repository.RepositoryConstants;
import info.magnolia.rest.AbstractEndpoint;
import info.magnolia.rest.registry.ConfiguredEndpointDefinition;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pingan.brands.rest.pojo.PADocument;
import com.pingan.brands.rest.pojo.PAPerspectivesDetail;
import com.pingan.brands.rest.pojo.PAShareCapital;
import com.pingan.brands.rest.pojo.PAShareCapitalItem;
import com.pingan.brands.rest.pojo.QueryRequest;
import com.pingan.brands.rest.pojo.QueryResponse;
import com.pingan.brands.rest.pojo.Result;

/**
 * REST service for query.
 */
@Path("/pingan/v1/query")
@Produces(MediaType.APPLICATION_JSON)
public class QueryEndpoint extends AbstractEndpoint<ConfiguredEndpointDefinition> {

    private static Logger log = LoggerFactory.getLogger(QueryEndpoint.class);

    private static final String ROOT = "/";
    private static final String SHARE_CAPITAL_NODETYPE = "mgnl:paShareCapital";
    private static final String IR_LIBRARY_NODETYPE = "mgnl:paIrLibrary";
    private static final String SUSTAINABILITY_REPORT_NODETYPE = "mgnl:paSustainabilityReport";

    private static final String STATEMENT = "SELECT * FROM [%s] WHERE ";
    private static final String DESCENDANTNODE_QUERY_PATTERN = " ISDESCENDANTNODE('%s') ";
    private static final String TEMPLATE_QUERY_PATTERN = " [mgnl:template]='%s' ";
    private static final String YEAR_QUERY_PATTERN = " ([date] >= CAST('%s-01-01T00:00:00.000Z' AS DATE) AND [date] <= CAST('%s-12-31T23:59:59.999Z' AS DATE)) ";
    private static final String ORDER_QUERY_PATTERN = " ORDER BY [date] DESC ";
    private final DamTemplatingFunctions damfn;

    @Inject
    protected QueryEndpoint(ConfiguredEndpointDefinition endpointDefinition, DamTemplatingFunctions damfn) {
        super(endpointDefinition);
        this.damfn = damfn;
    }

    @POST
    @Path("/perspectivesDetails")
    public Response getPerspectivesDetails(QueryRequest queryRequest) {

        QueryResponse queryResponse = new QueryResponse();
        List<Result> perspectivesDetailList = new ArrayList<>();
        if (!queryRequest.getYear().isEmpty() && !queryRequest.getCategories().isEmpty()) {
            try {
                String statement = buildPerspectivesDetailsQuery(queryRequest.getYear(), queryRequest.getCategories(), queryRequest.getParentPath());
                Collection<Node> collectionFromNodeIterator = query(statement, RepositoryConstants.WEBSITE, queryRequest.getLimit(), queryRequest.getOffset(), NodeTypes.Page.NAME);
                collectionFromNodeIterator.forEach(node -> perspectivesDetailList.add(createPerspectivesDetail(node)));
                queryResponse.setData(perspectivesDetailList);
                queryResponse.setTotalRecord(getTotalRecord(statement, RepositoryConstants.WEBSITE, NodeTypes.Page.NAME));
                return buildResponse(Response.ok(queryResponse));
            } catch (RepositoryException e) {
                log.warn("Error when retrieving [{}] from [{}] workspace", "perspectivesDetails", RepositoryConstants.WEBSITE);
                return buildResponse(Response.status(Response.Status.NOT_FOUND));
            }
        } else {
            queryResponse.setData(new ArrayList<>());
            queryResponse.setTotalRecord(0);
            return buildResponse(Response.ok(queryResponse));
        }
    }

    private Integer getTotalRecord(String statement, String workspace, String filterNodeType) throws RepositoryException {
        return query(statement, workspace, -1L, 0L, filterNodeType).size();
    }

    @POST
    @Path("/shareCapitals")
    public Response getShareCapitals(QueryRequest queryRequest) {

        QueryResponse queryResponse = new QueryResponse();
        if (!queryRequest.getYear().isEmpty() && !queryRequest.getCategories().isEmpty()) {
            List<PAShareCapital> paShareCapitals = new ArrayList<>();
            List<PAShareCapitalItem> items = new ArrayList<>();
            try {
                String statement = buildShareCapitalsQuery(queryRequest.getYear(), ROOT);
                Collection<Node> collectionFromNodeIterator = query(statement, "pa-share-capital", -1L, queryRequest.getOffset(), SHARE_CAPITAL_NODETYPE);
                collectionFromNodeIterator.forEach(node -> {
                    if (PropertyUtil.getDate(node, "date") != null){
                        items.add(createShareCapitalItem(node));
                    }
                });

                items.stream().collect(Collectors.groupingBy(PAShareCapitalItem::getYear, Collectors.mapping((PAShareCapitalItem p) -> p, toList()))).forEach((k, v) -> paShareCapitals.add(new PAShareCapital(k, v)));
                paShareCapitals.stream().sorted(Comparator.comparingInt(PAShareCapital::getYear).reversed()).collect(toList());
                queryResponse.setData(paShareCapitals.stream().sorted(Comparator.comparingInt(PAShareCapital::getYear).reversed()).collect(toList()));
                queryResponse.setTotalRecord(getTotalRecord(statement, "pa-share-capital", SHARE_CAPITAL_NODETYPE));
                return buildResponse(Response.ok(queryResponse));
            } catch (RepositoryException e) {
                log.warn("Error when retrieving [{}] from [{}] workspace", "shareCapital", "pa-share-capital");
                return buildResponse(Response.status(Response.Status.NOT_FOUND));
            }
        } else {
            queryResponse.setData(new ArrayList<>());
            queryResponse.setTotalRecord(0);
            return buildResponse(Response.ok(queryResponse));
        }

    }

    private Collection<Node> query(String statement, String workspace, Long limit, Long offset, String filterNodeType) throws RepositoryException {
        final Session jcrSession = MgnlContext.getJCRSession(workspace);
        final QueryManager jcrQueryManager = jcrSession.getWorkspace().getQueryManager();
        final Query query = jcrQueryManager.createQuery(statement, Query.JCR_SQL2);
        if (limit > 0) {
            query.setLimit(limit);
        }
        query.setOffset(offset);
        QueryResult result = query.execute();
        return NodeUtil.getCollectionFromNodeIterator(NodeUtil.filterDuplicates(NodeUtil.filterParentNodeType(result.getNodes(), filterNodeType)));
    }

    private String buildPerspectivesDetailsQuery(List<String> years, List<String> categories, String parentPath) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(STATEMENT, NodeTypes.Page.NAME));
        sb.append(String.format(DESCENDANTNODE_QUERY_PATTERN, parentPath));
        sb.append(" AND ");
        sb.append(String.format(TEMPLATE_QUERY_PATTERN, "pingan-templates:pages/perspectivesDetails"));
        sb.append(buildYearsQuery(years));
        sb.append(buildCategoriesQuery(categories));
        sb.append(ORDER_QUERY_PATTERN);
        log.debug("QUERY: [{}]", sb);
        return sb.toString();
    }

    @POST
    @Path("/irLibrary")
    public Response getIrLibrary(QueryRequest queryRequest) {

        QueryResponse queryResponse = new QueryResponse();
        List<Result> documents = new ArrayList<>();
        if (!queryRequest.getYear().isEmpty() && !queryRequest.getCategories().isEmpty()) {
            try {
                String statement = buildDocumentsQuery(queryRequest.getYear(), queryRequest.getCategories(), ROOT, IR_LIBRARY_NODETYPE);
                Collection<Node> collectionFromNodeIterator = query(statement, "pa-ir-library", queryRequest.getLimit(), queryRequest.getOffset(), IR_LIBRARY_NODETYPE);
                collectionFromNodeIterator.forEach(node -> documents.add(createDocument(node)));
                queryResponse.setData(documents);
                queryResponse.setTotalRecord(getTotalRecord(statement, "pa-ir-library", IR_LIBRARY_NODETYPE));
                return buildResponse(Response.ok(queryResponse));
            } catch (RepositoryException e) {
                log.warn("Error when retrieving [{}] from [{}] workspace", IR_LIBRARY_NODETYPE, "pa-ir-library");
                return buildResponse(Response.status(Response.Status.NOT_FOUND));
            }
        } else {
            queryResponse.setData(new ArrayList<>());
            queryResponse.setTotalRecord(0);
            return buildResponse(Response.ok(queryResponse));
        }
    }

    @POST
    @Path("/sustainabilityReports")
    public Response getSustainabilityReports(QueryRequest queryRequest) {

        QueryResponse queryResponse = new QueryResponse();
        List<Result> documents = new ArrayList<>();
        if (!queryRequest.getYear().isEmpty() && !queryRequest.getCategories().isEmpty()) {
            try {
                String statement = buildDocumentsQuery(queryRequest.getYear(), queryRequest.getCategories(), ROOT, SUSTAINABILITY_REPORT_NODETYPE);
                Collection<Node> collectionFromNodeIterator = query(statement, "pa-sustainability-reports", queryRequest.getLimit(), queryRequest.getOffset(), SUSTAINABILITY_REPORT_NODETYPE);
                collectionFromNodeIterator.forEach(node -> documents.add(createDocument(node)));
                queryResponse.setData(documents);
                queryResponse.setTotalRecord(getTotalRecord(statement, "pa-sustainability-reports", SUSTAINABILITY_REPORT_NODETYPE));
                return buildResponse(Response.ok(queryResponse));
            } catch (RepositoryException e) {
                log.warn("Error when retrieving [{}] from [{}] workspace", SUSTAINABILITY_REPORT_NODETYPE, "pa-sustainability-reports");
                return buildResponse(Response.status(Response.Status.NOT_FOUND));
            }
        } else {
            queryResponse.setData(new ArrayList<>());
            queryResponse.setTotalRecord(0);
            return buildResponse(Response.ok(queryResponse));
        }
    }

    private String buildDocumentsQuery(List<String> years, List<String> categories, String parentPath, String filterNodeType) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(STATEMENT, filterNodeType));
        sb.append(String.format(DESCENDANTNODE_QUERY_PATTERN, parentPath));
        sb.append(buildYearsQuery(years));
        sb.append(buildCategoriesQuery(categories));
        sb.append(ORDER_QUERY_PATTERN);
        log.debug("QUERY: [{}]", sb);
        return sb.toString();
    }

    private String buildShareCapitalsQuery(List<String> years, String parentPath) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(STATEMENT, SHARE_CAPITAL_NODETYPE));
        sb.append(String.format(DESCENDANTNODE_QUERY_PATTERN, parentPath));
        sb.append(buildYearsQuery(years));
        sb.append(ORDER_QUERY_PATTERN);
        log.debug("QUERY: [{}]", sb);
        return sb.toString();
    }

    private String buildYearsQuery(List<String> years) {
        if (years.size() == 1 && years.contains("all")) {
            return StringUtils.EMPTY;
        }

        StringBuilder sb = new StringBuilder();
        sb.append(" AND ");
        sb.append("(");
        for (Iterator<?> i = years.iterator(); i.hasNext(); ) {
            String next = i.next().toString();
            sb.append(String.format(YEAR_QUERY_PATTERN, next, next));
            if (i.hasNext()) {
                sb.append(" OR ");
            }
        }
        sb.append(") ");
        return sb.toString();
    }

    private String buildCategoriesQuery(List<String> categories) {
        if (categories.size() == 1 && categories.contains("all")) {
            return StringUtils.EMPTY;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(" AND ");
        sb.append("(");
        for (Iterator<?> i = categories.iterator(); i.hasNext(); ) {
            String next = i.next().toString();
            sb.append("[categoriesUUIDs] LIKE '%");
            sb.append(next.replace("-", ""));
            sb.append("%'");
            if (i.hasNext()) {
                sb.append(" OR ");
            }
        }
        sb.append(")");
        return sb.toString();
    }

    private Response buildResponse(Response.ResponseBuilder sb) {
        return sb.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
    }

    private String getCategoryName(Node node) {

        AtomicReference<String> categoryName = new AtomicReference<>();
        try {
            if (node.hasNode("categories")) {
                NodeUtil.asList(NodeUtil.getNodes(node.getNode("categories"), NodeTypes.ContentNode.NAME))
                        .stream().findFirst().ifPresent(
                        n -> categoryName.set(PropertyUtil.getString(SessionUtil.getNodeByIdentifier("category", PropertyUtil.getString(n, "field", "")), "name", "")));
            }
        } catch (RepositoryException e) {
            log.warn("Error when retrieving [{}] of [{}] workspace", "categories", NodeUtil.getPathIfPossible(node));
        }
        return categoryName.get();
    }

    private PADocument createDocument(Node node) {
        PADocument paDocument = new PADocument();
        Calendar date = PropertyUtil.getDate(node, "releaseDate");
        if (date != null) {
            paDocument.setDate(new SimpleDateFormat("MMM yyyy", Locale.ENGLISH).format(date.getTime()));
        }
        paDocument.setType(PropertyUtil.getString(node, "type", StringUtils.EMPTY));
        paDocument.setTitle(PropertyUtil.getString(node, "title", StringUtils.EMPTY));
        paDocument.setLink(MgnlContext.getContextPath() + NodeUtil.getPathIfPossible(node));
        paDocument.setTarget("_blank");
        try {
            if (node.hasNode("link")) {
                Node link = node.getNode("link");
                if (StringUtils.equalsIgnoreCase(PropertyUtil.getString(link, "field", StringUtils.EMPTY), "dam")) {
                    Asset asset = damfn.getAsset("jcr:" + PropertyUtil.getString(link, "dam", ""));
                    if (asset != null) {
                        final String contextPath = MgnlContext.getContextPath();
                        paDocument.setLink(String.format("%s%s%s", contextPath, "/resource", asset.getPath()));
                    }
                } else {
                    paDocument.setLink(PropertyUtil.getString(link, "external", StringUtils.EMPTY));
                }
            }
        } catch (RepositoryException e) {
            log.warn("Error when retrieving link property of [{}]", NodeUtil.getPathIfPossible(node));
        }
        return paDocument;
    }

    private PAPerspectivesDetail createPerspectivesDetail(Node node) {
        PAPerspectivesDetail paPerspectivesDetail = new PAPerspectivesDetail();
        paPerspectivesDetail.setColor(PropertyUtil.getString(node, "color", ""));
        if (StringUtils.isNotEmpty(PropertyUtil.getString(node, "image"))) {
            paPerspectivesDetail.setImage(damfn.getAssetLink("jcr:" + PropertyUtil.getString(node, "image", ""), "original"));
        }
        paPerspectivesDetail.setCategory(getCategoryName(node));
        paPerspectivesDetail.setTitle(PropertyUtil.getString(node, "title", ""));
        paPerspectivesDetail.setLink(MgnlContext.getContextPath() + NodeUtil.getPathIfPossible(node).replaceFirst("^/en", "") + ".html");
        paPerspectivesDetail.setTarget("_blank");
        paPerspectivesDetail.setLearnMoreText("Learn More");
        paPerspectivesDetail.setImageTextColor(PropertyUtil.getString(node, "imageTextColor", ""));
        return paPerspectivesDetail;
    }

    private PAShareCapitalItem createShareCapitalItem(Node node) {
        PAShareCapitalItem item = new PAShareCapitalItem();
        Calendar date = PropertyUtil.getDate(node, "date");
        if (date != null) {
            item.setYear(date.get(Calendar.YEAR));
            item.setDay(date.get(Calendar.DAY_OF_MONTH));
            item.setMonth(new SimpleDateFormat("MMM", Locale.ENGLISH).format(date.getTime()));
        }
        item.setTitle(PropertyUtil.getString(node, "title", StringUtils.EMPTY));
        item.setDescription(PropertyUtil.getString(node, "description", StringUtils.EMPTY));
        item.setChange(PropertyUtil.getString(node, "change", StringUtils.EMPTY));
        item.setTotal_equity(PropertyUtil.getString(node, "totalEquity", StringUtils.EMPTY));
        return item;
    }
}
