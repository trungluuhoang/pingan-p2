package com.pingan.brands.rest.pojo;

public class PAShareCapitalItem implements Result {

    private Integer day;
    private String month;
    private Integer year;
    private String title;
    private String description;
    private String change;
    private String total_equity;

    public void setDay(int day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public String getTotal_equity() {
        return total_equity;
    }

    public void setTotal_equity(String total_equity) {
        this.total_equity = total_equity;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
