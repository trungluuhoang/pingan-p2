package com.pingan.brands.rest;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeNameHelper;
import info.magnolia.jcr.util.NodeTypes;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.rest.AbstractEndpoint;
import info.magnolia.rest.registry.ConfiguredEndpointDefinition;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * REST service for form.
 */
@Path("/pingan/v1/form")
@Produces(MediaType.APPLICATION_JSON)
public class FormEndpoint extends AbstractEndpoint<ConfiguredEndpointDefinition> {

	private static Logger log = LoggerFactory.getLogger(FormEndpoint.class);
	private static final String SUBSCRIPTION_WORKSPACE = "pa-subscription";
	private static final String SUBSCRIPTION_NODETYPE = "mgnl:paSubscription";
	private static final String DATE_FORMAT = "yyyy-MM-dd";

	private final NodeNameHelper nodeNameHelper;

	@Inject
	protected FormEndpoint(ConfiguredEndpointDefinition endpointDefinition, NodeNameHelper nodeNameHelper) {
		super(endpointDefinition);
		this.nodeNameHelper = nodeNameHelper;
	}

	@POST
	@Path("/subscribe")
	public Response subscribe(@FormParam("email") String email) throws RepositoryException {

		if (StringUtils.isEmpty(email)) {
			return buildResponse(Response.ok("{\"success\":false}"));
		} else {
			final Session session = MgnlContext.getJCRSession(SUBSCRIPTION_WORKSPACE);
			String nodeName = nodeNameHelper.getValidatedName(email);
			final Node parentNode = NodeUtil.createPath(session.getRootNode(), getParentPath(), NodeTypes.Folder.NAME, false);
			if (!parentNode.hasNode(nodeName)) {
				final Node node = parentNode.addNode(nodeName, SUBSCRIPTION_NODETYPE);
				PropertyUtil.setProperty(node, "email", email);
				log.debug("subscribe for email [{}] under path [{}]", email, node.getPath());
				session.save();
			}
			return buildResponse(Response.ok("{\"success\":true}"));
		}
	}

	private Response buildResponse(Response.ResponseBuilder sb) {
		return sb.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
	}

	private String getParentPath() {
		return new SimpleDateFormat(DATE_FORMAT).format(new Date());
	}
}
