/**
 * This file Copyright (c) 2020 Magnolia International
 * Ltd. (http://www.magnolia-cms.com). All rights reserved.
 *
 *
 * This program and the accompanying materials are made
 * available under the terms of the Magnolia Network Agreement
 * which accompanies this distribution, and is available at
 * http://www.magnolia-cms.com/mna.html
 *
 * Any modifications to this file must keep this entire header
 * intact.
 *
 */
package com.pingan.brands.templates;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.*;
import java.util.Collection;

/**
 * Advanced search templating functions.
 */
@Singleton
public class AdvancedSearchFunctions {

    private static final Logger log = LoggerFactory.getLogger(AdvancedSearchFunctions.class);

    public static Collection<Node> search(String workspace, String statement, String language, String returnItemType, long limit, long offset) throws RepositoryException {
        Session session = MgnlContext.getJCRSession(workspace);
        QueryManager manager = session.getWorkspace().getQueryManager();
        Query query = manager.createQuery(statement, language);
        query.setLimit(limit);
        query.setOffset(offset);
        QueryResult result = query.execute();
        return NodeUtil.getCollectionFromNodeIterator(NodeUtil.filterDuplicates(NodeUtil.filterParentNodeType(result.getNodes(), returnItemType)));
    }

    public static Collection<Node> search(String workspace, String statement, String language, String returnItemType) throws RepositoryException {
        Session session = MgnlContext.getJCRSession(workspace);
        QueryManager manager = session.getWorkspace().getQueryManager();
        Query query = manager.createQuery(statement, language);
        QueryResult result = query.execute();
        return NodeUtil.getCollectionFromNodeIterator(NodeUtil.filterDuplicates(NodeUtil.filterParentNodeType(result.getNodes(), returnItemType)));
    }

    public static int count(String workspace, String statement, String language, String returnItemType) throws RepositoryException {
        return search(workspace, statement, language, returnItemType).size();
    }

}
