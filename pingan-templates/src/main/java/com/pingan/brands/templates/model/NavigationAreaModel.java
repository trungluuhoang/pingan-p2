package com.pingan.brands.templates.model;

import info.magnolia.rendering.model.RenderingModel;
import info.magnolia.rendering.model.RenderingModelImpl;
import info.magnolia.rendering.template.AreaDefinition;

import java.util.Locale;

import javax.inject.Inject;
import javax.jcr.Node;

import org.apache.commons.lang3.LocaleUtils;

public class NavigationAreaModel extends RenderingModelImpl<AreaDefinition> {

    @Inject
    public NavigationAreaModel(Node content, AreaDefinition definition, RenderingModel<?> parent) {
        super(content, definition, parent);
    }

    public Locale getLocale(String language) {
        return LocaleUtils.toLocale(language);
    }
}
