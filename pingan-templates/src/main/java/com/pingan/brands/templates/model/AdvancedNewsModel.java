package com.pingan.brands.templates.model;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeUtil;
import info.magnolia.rendering.model.RenderingModel;
import info.magnolia.rendering.model.RenderingModelImpl;
import info.magnolia.rendering.template.configured.ConfiguredTemplateDefinition;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AdvancedNewsModel extends RenderingModelImpl<ConfiguredTemplateDefinition> {

    private static Logger log = LoggerFactory.getLogger(AdvancedNewsModel.class);
    private static final String statement = "SELECT * FROM [%s] AS n WHERE ISDESCENDANTNODE('/%s') ORDER BY n.date DESC";

    @Inject
    public AdvancedNewsModel(Node content, ConfiguredTemplateDefinition definition, RenderingModel<?> parent) {
        super(content, definition, parent);
    }

    public List<Node> getLatestData(Integer limit, String workspace, String contentType, String root) {
        try {
            final Session jcrSession = MgnlContext.getJCRSession(workspace);
            final QueryManager jcrQueryManager = jcrSession.getWorkspace().getQueryManager();
            final Query query = jcrQueryManager.createQuery(String.format(statement, contentType, root), Query.JCR_SQL2);
            query.setLimit(limit);
            return NodeUtil.asList(NodeUtil.asIterable(query.execute().getNodes()));
        } catch (RepositoryException e) {
            log.warn("Error when retrieving [{}] from [{}] workspace", content, workspace);
            return Collections.emptyList();
        }
    }
}
