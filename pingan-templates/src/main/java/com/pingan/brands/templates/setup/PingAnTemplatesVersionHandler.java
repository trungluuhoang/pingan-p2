package com.pingan.brands.templates.setup;

import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.BootstrapSingleResource;
import info.magnolia.module.delta.DeltaBuilder;
import info.magnolia.module.delta.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is optional and lets you manage the versions of your module,
 * by registering "deltas" to maintain the module's configuration, or other type of content.
 * If you don't need this, simply remove the reference to this class in the module descriptor xml.
 *
 * @see info.magnolia.module.DefaultModuleVersionHandler
 * @see info.magnolia.module.ModuleVersionHandler
 * @see info.magnolia.module.delta.Task
 */
public class PingAnTemplatesVersionHandler extends DefaultModuleVersionHandler {

    public PingAnTemplatesVersionHandler() {
        register(DeltaBuilder.update("1.0.0", "").addTasks(updateTo_1_0_0()));
    }

    @Override
    protected List<Task> getExtraInstallTasks(InstallContext installContext) {
        List<Task> tasks = new ArrayList<>();
        tasks.addAll(updateTo_1_0_0());
        return tasks;
    }

    private List<Task> updateTo_1_0_0() {
        List<Task> tasks = new ArrayList<>();

//        tasks.add(new BootstrapSingleResource("Add site configuration.", "", "/mgnl-bootstrap/pingan-templates/sites/config.modules.multisite.config.sites.en.xml"));
        return tasks;
    }
}
