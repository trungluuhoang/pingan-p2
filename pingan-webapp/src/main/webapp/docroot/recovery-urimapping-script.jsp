<%--Follow same instruction but using this script instead--%>
<%--https://wiki.magnolia-cms.com/display/WIKI/Reset+superuser+account--%>

<%@ page contentType="text/plain" %>
<%@ page import="info.magnolia.context.MgnlContext"%>
<%@ page import="javax.jcr.Session"%>
<%
info.magnolia.context.MgnlContext.setInstance(new info.magnolia.context.SingleJCRSessionSystemContext());
try{
    Session s = MgnlContext.getJCRSession("config");
    System.out.println("removing /modules/ui-admincentral/virtualUriMappings");
    s.getNode("/modules/ui-admincentral/virtualUriMappings").remove();
    s.save();
} catch (Exception e) {
    System.out.println(e);
}
System.out.println("finished");
%>