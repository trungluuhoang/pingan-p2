===========================================================

                                       _ _
     _ __ ___   __ _  __ _ _ __   ___ | (_) __ _  (R)
    | '_ ` _ \ / _` |/ _` | '_ \ / _ \| | |/ _` |
    | | | | | | (_| | (_| | | | | (_) | | | (_| |
    |_| |_| |_|\__,_|\__, |_| |_|\___/|_|_|\__,_|
                     |___/


        19. May 2020
        v 6.2.1
        DX Core Edition

        http://www.magnolia-cms.com/

===========================================================

Magnolia is an open-source enterprise class Content Management System
developed by Magnolia International Ltd., based on the standard API
for Java Content Repositories (JCR).

This package contains:

   Magnolia Core 6.2.1
   magnolia-empty-webapp 6.2.1
   magnolia-empty-webapp 6.2.1
   Magnolia Internationalization Framework 6.2.1
   Magnolia JAAS 6.2.1
   magnolia-ldap 1.10.3
   magnolia-license 1.7.1
   Magnolia Link Unfurl Module 1.3.3
   Magnolia Backup Module Project 2.4.1
   Magnolia Mail module 5.5.4
   magnolia-module-rssaggregator 2.6.3
   magnolia-module-soft-locking 3.0
   Magnolia Rendering 6.2.1
   Magnolia Templating 6.2.1


===========================================================
Installation
===========================================================

Installation instructions can be found at:
    http://documentation.magnolia-cms.com/DOCS/Installing+Magnolia


===========================================================
Documentation, Licensing & Support
===========================================================

You can find documentation at:
    http://documentation.magnolia-cms.com/

More information about this release can be found at:
    http://documentation.magnolia-cms.com/releases/6.2.1.html

Feel free to join the community and contribute through
bug reports, documentation, discussions, and more to help
improve Magnolia!
    http://www.magnolia-cms.com/developers.html

For details on commercial services and support regarding
Magnolia, please visit:
    http://www.magnolia-cms.com/services.html

THIS SOFTWARE IS PROVIDED "AS-IS" AND FREE OF CHARGE, WITH
ABSOLUTELY NO WARRANTY OR SUPPORT OF ANY KIND, EXPRESSED OR
IMPLIED, UNDER THE TERMS OF THE INCLUDED LICENSE AGREEMENT.

Thank you for using Magnolia.

Magnolia International Ltd.
info@magnolia-cms.com


===========================================================

Copyright 2003-2020 Magnolia International Ltd.

Magnolia is a registered trademark of
Magnolia International Ltd.

http://www.magnolia-cms.com
All rights reserved.
